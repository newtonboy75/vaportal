<div class="col-md-4">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">System</h3> 
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-6 text-center">
					<h1>
						<span class="fa fa-cogs" aria-hidden="true"></span> {{ count($online_va) }}
					</h1>
					<a href="#" id="dashboard-online-va"class="label label-primary">Working VA</a>
				</div>
				<div class="col-md-6 text-center">
					<h1>
						<span class="fa fa-user-circle" aria-hidden="true"></span> {{ $served_clients }}
					</h1>
					<a href="#" class="label label-success" data-toggle="modal" data-target="#myModalWorkingVA">Served Clients</a>
				</div>
				<div class="col-md-6 text-center">
					<h1>
						<span class="fa fa-users" aria-hidden="true"></span>  {{ $teams_count }}
					</h1>
					<a href="/dashboard/teams" class="label label-primary">Teams</a>
				</div>
				<div class="col-md-6 text-center">
					<h1>
						<span class="fa fa-envelope-open-o" aria-hidden="true"></span> {{ $requests_count }}
					</h1>
					<a href="/dashboard/requests" class="label label-danger">Requests</a>
				</div>
			</div>
		</div><!-- box-body -->
		<div class="box-footer">
		</div>
	</div>
</div>