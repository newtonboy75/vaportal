@extends('layouts.adminlte-master')
@section('page-title', "Profile")

@section('content')
<div class="" style="width: 100%;">

  @if ($errors->any())
  <div class="box box-warning box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">Errors</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
      <!-- /.box-tools -->
    </div>

    <!-- /.box-header -->
    <div class="box-body">
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </div>
    <!-- /.box-body -->
  </div>
  @endif

  @if (session()->has('notification_message'))
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-info"></i> Success!</h4>
    {{ session()->get('notification_message') }}
  </div>
  @endif

  <div class="" style="width: 100% !important; background: #fff; padding-left: 40px; padding-right: 40px;">

    <div class="panel-heading">
      <i class="fa fa-user-circle-o fa-minus"></i> Edit Profile
    </div>

    @if(\Auth::user()->hasRole('va'))
    <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/users/profile')}}" id="profile-form">
      {{ csrf_field() }}
      <div class="row" style="padding: 20px;">
        <div class="col-md-2">
          <div class="form-group">
            <img id='user-avatar-profile' src="{{$avatar}}" class="img-circle img-responsive center-block" alt="User Image">
          </div>
          <div class="form-group">
            <label class="btn btn-default center-block">
              <span id="filename">
                Choose an image...
              </span><span style="color:red">*</span>
              <input type="file" style='display:none' name="avatar" id="file">
              <input required type="hidden" id="avatar_"  name="avatar_" value="{{$avatar}}">
            </label>
          </div>
          
            <!-- Button trigger modal -->
            <br><br>
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#changePasswordModal">
            Change Password
            </button>
        
        </div>
        <div class="col-md-10">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>First Name  <span style="color:red">*</span></label>
                <input required type="text" class="form-control input-lg" name="first_name" value="{{ $q->first_name }}" placeholder="First Name">
              </div>
              <div class="form-group">
                <label>Middle Name  <span style="color:red">*</span></label>
                <input required type="text" class="form-control input-lg" name="middle_name" value="{{ $q->middle_name }}" placeholder="Middle Name">
              </div>
              <div class="form-group">
                <label>Last Name  <span style="color:red">*</span></label>
                <input required type="text" class="form-control input-lg" name="last_name" value="{{ $q->last_name }}" placeholder="Last Name">
              </div>
              <div class="form-group">
                <label>Suffix</label>
                <input type="text" class="form-control input-lg" name="name_suffix" value="{{ $q->name_suffix ?? 'N/A' }}" placeholder="Sr/Jr/etc">
              </div>
              <div class="form-group">
                <label>Work Email  <span style="color:red">*</span></label>
                <input required type="text" class="form-control input-lg" name="_email" value="{{ $q->email }}" placeholder="Work Email" disabled>
                
              </div>
              <!---
              <div class="form-group">
                <label>Current Password</label>
                <input type="password" class="form-control input-lg" name="current_password" value="" placeholder="Current Password">
              </div>
              <div class="form-group">
                <label>New Password</label>
                <input type="password" class="form-control input-lg" name="new_password" value="" placeholder="New Password">
              </div>
              <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" class="form-control input-lg" name="confirm_password" value="" placeholder="Confirm Password">
              </div>
              -->
              <div class="form-group">
                <label>Personal Email  <span style="color:red">*</span></label>
                <input required type="text" class="form-control input-lg" name="personal_email" value="{{ $q->personal_email }}" placeholder="Personal Email">
              </div>
              
              <div class="form-group">
                <label>Civil Status  <span style="color:red">*</span></label>
                <select required name="civil_status" class="form-control">
                  <option disabled selected value="Please select">Please select</option>
                  <option value="single" {{ $q->civil_status == 'single' ? 'selected' : ''}}>Single</option>
                  <option value="married" {{ $q->civil_status == 'married' ? 'selected' : ''}}>Married</option>
                  <option value="separated" {{ $q->civil_status == 'separated' ? 'selected' : ''}}>Separated</option>
                  <option value="divorced" {{ $q->civil_status == 'divorced' ? 'selected' : ''}}>Divorced</option>
                  <option value="widowed" {{ $q->civil_status == 'widowed' ? 'selected' : ''}}>Widowed</option>
                  <option value="cohabitation" {{ $q->civil_status == 'cohabitation' ? 'selected' : ''}}>Co-Habitation</option>
                </select>
              </div>
              
              <div class="form-group">
                <label>Gender  <span style="color:red">*</span></label>
                <select required name="gender" class="form-control">
                  <option disabled selected value="Please select">Please select</option>
                  <option value="male" {{ $q->gender == 'male' ? 'selected' : ''}}>Male</option>
                  <option value="female" {{ $q->gender == 'female' ? 'selected' : ''}}>Female</option>
                </select>
              </div>
              
              <div class="form-group">
                <label>Mobile Number  <span style="color:red">*</span></label>
                <input required type="tel" class="form-control input-lg" name="mobile_number" value="{{ $q->mobile_number }}" placeholder="Mobile Number">
              </div>
              <div class="form-group">
                <label>Alternate Contact Number  <span style="color:red">*</span></label>
                <input required type="tel" class="form-control input-lg" name="alternate_contact_number" value="{{ $q->alternate_contact_number }}" placeholder="Alternate Number">
              </div>
              
              <div class="form-group">
                <label>Landline  <span style="color:red">*</span></label>
                <input required type="tel" class="form-control input-lg" name="landline" value="{{ $q->landline }}" placeholder="Landline Number">
              </div>
              

            </div>
            <div class="col-sm-6">
            
            <div class="form-group">
                <label>Date of Birth  <span style="color:red">*</span></label>
                <div class="input-group date" id="datepicker">
                  <input required type="text" name='birthdate' class="form-control input-lg" value="{{$q->birthdate}}">
                  <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                  </div>
                </div>
              </div>
              

              <div class="form-group">
                <label>Complete Address  <span style="color:red">*</span></label>
                <input required type="text" class="form-control input-lg" name="address1" value="{{ $q->address1 }}" placeholder="House # Street, Municipality, City">
              </div>  
              <div class="form-group">
                <label>Zip Code  <span style="color:red">*</span></label>
                <input required type="text" class="form-control input-lg" name="zip" value="{{ $q->zip }}" placeholder="Zip Code">
              </div> 
              
                <div class="form-group">
                <label>Location  <span style="color:red">*</span></label>
               <select required name="location" class="form-control">
                  <option disabled selected value="Please select">Please select</option>
                  <option value="luzon" {{ $q->location == 'luzon' ? 'selected' : ''}}>Luzon</option>
                  <option value="visayas" {{ $q->location == 'visayas' ? 'selected' : ''}}>Visayas</option>
                  <option value="mindanao" {{ $q->location == 'mindanao' ? 'selected' : ''}}>Mindanao</option>
                </select>
              </div>

              <div class="form-group">
                <label>Skype ID  <span style="color:red">*</span></label>
                <input required type="text" class="form-control input-lg" name="skype_id" value="{{ $q->skype_id }}" placeholder="Skype ID">
              </div>
              <div class="form-group">
                <label>Facebook Account  <span style="color:red">*</span></label>
                <input required type="text" class="form-control input-lg" name="facebook" value="{{ $q->facebook }}" placeholder="Facebook">
              </div>
              
              <div class="form-group">
                <label>Timezone:</label>
                {{ $q->timezone }}
              </div>
              <div class="form-group">
                <label>Contact Person (In case of emegency)  <span style="color:red">*</span></label>
                <input required type="text" class="form-control input-lg" name="contact_person_name" value="{{ $q->contact_person_name }}" placeholder="Contact Person Name">
              </div>
              <div class="form-group">
                <label>Contact Person Mobile Number  <span style="color:red">*</span></label>
                <input required type="text" class="form-control input-lg" name="contact_person_number" value="{{ $q->contact_person_number }}" placeholder="Contact Person Number">
              </div>
              <div class="form-group">
                <label>Contact Person Relationship  <span style="color:red">*</span></label>
                <input required type="tel" class="form-control input-lg" name="contact_person_relationship" value="{{ $q->contact_person_relationship }}" placeholder="Contact Person Relationship">
              </div>
              
  
              <div class="form-group">
                <label>Contract Date  <span style="color:red">*</span></label>
                <div class="input-group date" id="datepicker2">
                  <input required type="text" name='contract_date' class="form-control input-lg" value="{{$q->contract_date}}">
                  
                  <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                  </div>
                  
                </div>
                <small>Encoded Date: {{date('F j, Y', strtotime($q->created_at))}}</small>
            </div>
            
            </div>
          </div>
          
            

        </div>
      </div>

      <div class="box">
        <div class="box-footer clearfix">
          <button type="submit" class="pull-right btn btn-success btn-lg">Update</button>
        </div>
      </div>
    </form>

    @else

    <div class="panel-body">
      <div class="col-md-12">

        <form enctype="multipart/form-data" method="POST" action="{{url('/dashboard/users/profile')}}">

          {{ csrf_field() }}

          <div class="row">

            <div class="box box-info">

              <div class="box-body">

                <div class="form-group">
                  <img id='user-avatar-profile' src="{{$avatar}}" class="img-circle img-responsive center-block" alt="User Image">
                </div>

                <div class="form-group">
                  <label class="btn btn-default center-block">
                    <span id="filename">
                      Choose an image...
                    </span>
                    <input type="file" style='display:none' name="avatar" id="file">
                    
                  </label>
                </div>

                <div class="form-group">
                  <label>Work Email</label>
                  <input type="text" class="form-control input-lg" name="email" value="{{ $q->email }}" placeholder="Work Email">
                </div>

                <div class="form-group">
                  <label>Current Password</label>
                  <input type="password" class="form-control input-lg" name="current_password" value="" placeholder="Current Password">
                </div>

                <div class="form-group">
                  <label>New Password</label>
                  <input type="password" class="form-control input-lg" name="new_password" value="" placeholder="New Password">
                </div>

                <div class="form-group">
                  <label>Confirm Password</label>
                  <input type="password" class="form-control input-lg" name="confirm_password" value="" placeholder="Confirm Password">
                </div>

                <div class="form-group">
                  <label>First Name</label>
                  <input type="text" class="form-control input-lg" name="first_name" value="{{ $q->first_name }}" placeholder="First Name">
                </div>

                <div class="form-group">
                  <label>Last Name</label>
                  <input type="text" class="form-control input-lg" name="last_name" value="{{ $q->last_name }}" placeholder="Last Name">
                </div>

                <div class="form-group">
                  <label>Personal Email</label>
                  <input type="text" class="form-control input-lg" name="personal_email" value="{{ $q->personal_email }}" placeholder="Personal Email">
                </div>

                <div class="form-group">
                  <label>Birthday</label>
                  <div class="input-group date" id="datepicker">
                    <input type="text" name='birthdate' class="form-control input-lg" value="{{$q->birthdate}}">
                    <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label>Address 1</label>
                  <input type="text" class="form-control input-lg" name="address1" value="{{ $q->address1 }}" placeholder="Address 1">
                </div>

                <div class="form-group">
                  <label>Address 2</label>
                  <input type="text" class="form-control input-lg" name="address2" value="{{ $q->address2 }}" placeholder="Address 2">
                </div>

                <div class="form-group">
                  <label>City</label>
                  <input type="text" class="form-control input-lg" name="city" value="{{ $q->city }}" placeholder="City">
                </div>

                <div class="form-group">
                  <label>Skype ID</label>
                  <input type="text" class="form-control input-lg" name="skype_id" value="{{ $q->skype_id }}" placeholder="Skype ID">
                </div>

                <div class="form-group">
                  <label>Facebook Account</label>
                  <input type="text" class="form-control input-lg" name="facebook" value="{{ $q->facebook }}" placeholder="Facebook">
                </div>

                <div class="form-group">
                  <label>Mobile Number</label>
                  <input type="text" class="form-control input-lg" name="mobile_number" value="{{ $q->mobile_number }}" placeholder="Mobile Number">
                </div>

                <div class="form-group">
                  <label>Alternate Contact Number</label>
                  <input type="text" class="form-control input-lg" name="alternate_contact_number" value="{{ $q->alternate_contact_number }}" placeholder="Alternate Number">
                </div>

                <div class="form-group">
                  <label>Timezone:</label>
                  {{ $q->timezone }}
                </div>


              </div>
            </div>

            <div class="box">
              <div class="box-footer clearfix">
                <button type="submit" class="pull-right btn btn-success btn-lg">Update</button>
              </div>
            </div>

          </div>
        </form>

      </div>
    </div>
    @endif

  </div>
</div>
<div class='clearfix'></div>



<!-- Modal  Password -->
<div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="changePasswordModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="changePasswordModalLabel">Change Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="form-group">
                <label>Current Password&nbsp;&nbsp;<small><span class="text-danger curpass"></span></small></label>
                <input type="password" class="form-control input-lg" name="current_password"  id="current_password" value="" placeholder="Current Password">
              </div>
              <div class="form-group">
                <label>New Password&nbsp;&nbsp;<small><span class="text-danger newpass"></span></small></label>
                <input type="password" class="form-control input-lg" name="new_password" id="new_password" value="" placeholder="New Password">
              </div>
              <div class="form-group">
                <label>Confirm Password&nbsp;&nbsp;<small><span class="text-danger confirmpass"></span></small></label>
                <input type="password" class="form-control input-lg" name="confirm_password" id="confirm_password" value="" placeholder="Confirm Password">
              </div>
              <small><span class="text-success successpass"></span></small>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="change_password">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endsection




@push('view-styles')
<style>
img#user-avatar-profile {
  object-fit: cover;
  width: 230px;
  height: 230px;
}
</style>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
@endpush

@push('view-scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

<script>
$('#change_password').click(function(){

var current_password = $('#current_password').val();
var new_password = $('#new_password').val();
var confirm_password = $('#confirm_password').val();

$('.curpass').text('');
$('.newpass').text('');
$('.confirmpass').text('');

if(current_password == ''){
    $('.curpass').text('Current password is empty');
}else if(new_password == ''){
    $('.newpass').text('New Password is empty');
}else if(new_password != confirm_password){
    $('.confirmpass').text('Password confirmation do not match with New Password');
}else{
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$.ajax({

                    url: '/dashboard/users/change-user-password',
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, current_password: current_password, new_password: new_password,  confirm_password: confirm_password},
                    success: function (data) { 
                        console.log(data);
                        if(data == 'Password incorrect'){
                            $('.curpass').text('Password do not match with current password');
                        }else{
                           $('.successpass').text('Password changed successfully');
                        }
                    }
                });
}
});
</script>

<script>
$(document).on('submit', '#profile-form', function(e){
if($('#user-avatar-profile').attr('src').indexOf('gravatar.com') >= 1){
alert('profile image is required.');
e.preventDefault();
return false;
}else{
    return true;
}
});

var file = document.getElementById("file");
file.onchange = function() {
  if (file.files.length > 0) {
    readURL(this);
  }
};

function readURL(input) {
  if (input.files && input.files[0]) {
      
    if(input.files[0].size > 1048576){
        alert('Profile image should not be more than 1mb');
    }else{
        var reader = new FileReader();

    reader.onload = function(e) {
      $('#user-avatar-profile').attr('src', e.target.result);
      $('#avatar_').val(e.target.result);
    };

    reader.readAsDataURL(input.files[0]);
    }
    
  }
}

$(function() {
  $('#datepicker').datepicker({
    format: 'yyyy-mm-dd'
  });
  
  $('#datepicker2').datepicker({
    format: 'yyyy-mm-dd'
  });
});
</script>
@endpush
