<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\ActivityLog;

class ActivityLogController extends Controller
{
    public function index()
    {
        return view('admin.activity-log.index');
    }

    public function datatables(Request $request)
    {

        $target_date = date("Y-m-d", strtotime(now()));
        if (isset($request->target_date)) {
            $target_date = $request->target_date;
        }

        $activity_logs = ActivityLog::whereDate('created_at', $target_date)
        ->orderBy('created_at', 'DESC')
        ->select(['id','first_name', 'last_name','role','log', 'created_at']);

        return DataTables::of($activity_logs)
            ->removeColumn('first_name')
            ->removeColumn('last_name')
            ->addColumn('user', function($al){
                return $al->first_name . " " . $al->last_name;
            })
            ->addColumn('timestamp', function($al){
                return date('Y-m-d H:i:s a', strtotime($al->created_at));
            })
        ->make(true);

    }
}