<!-- Footer -->
<footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
        <div class="col-lg-6">
            <div class="copyright text-center text-lg-left text-muted">
                &copy; 2019 <a href="#" class="font-weight-bold ml-1" target="_blank">Virtudesk</a>
            </div>
        </div>
        <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                <li class="nav-item">
                    <a href="#" class="nav-link" target="_blank">About Us</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link" target="_blank">Contact</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link" target="_blank">Support</a>
                </li>
            </ul>
        </div>
    </div>
</footer>
</div>
</div>
@yield('pageScripts')
<script src="{{asset('js/timedly.js')}}"></script>
@yield('specialScripts')
</body>

</html>
