<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
use App\Models\Project;
use App\Models\ClientTask;
use Illuminate\Validation\Rule;
use App\Models\ActivityLog;
use App\Models\ManagerClient;

class TaskboardController extends Controller
{
    public function index($project_id = null)
    {
        $auth_user = \Auth::user();

        //if ($auth_user->is('client') || $auth_user->is('manager') ) {
        if ($auth_user->is('client')) {
            
            if ($project_id == null) {
                $todos = ClientTask::where('client_id', $auth_user->id)->where('status','todo')->get();
                $in_progress = ClientTask::where('client_id', $auth_user->id)->where('status','in-progress')->get();
                $done = ClientTask::where('client_id', $auth_user->id)->where('status','done')->get();
            } else {

                //check if user is owner of the project_id
                $project = Project::where('user_id', $auth_user->id)->where('id', $project_id)->get();
                if (count($project)==0) {
                    return redirect('/dashboard/taskboard');
                }

                $todos = ClientTask::where('project_id', $project_id)->where('client_id', $auth_user->id)->where('status','todo')->get();
                $in_progress = ClientTask::where('project_id', $project_id)->where('client_id', $auth_user->id)->where('status','in-progress')->get();
                $done = ClientTask::where('project_id', $project_id)->where('client_id', $auth_user->id)->where('status','done')->get();
            }
            
            $projects = Project::where('user_id', $auth_user->id)->get();

            if ($auth_user->is('client')) {
                return va_view('admin.taskboard.client-index',compact('todos', 'in_progress', 'done', 'projects', 'project_id'));
            } else {
                return view('admin.taskboard.index',compact('todos', 'in_progress', 'done', 'projects', 'project_id'));
            }
        }elseif ($auth_user->is('manager')) {
            
            $clients = ManagerClient::where('user_id', $auth_user->id)->pluck('client_id');
            
            if ($project_id == null) {
                $todos = ClientTask::where('client_id', $auth_user->id)->where('status','todo')->get();
                $in_progress = ClientTask::where('client_id', $auth_user->id)->where('status','in-progress')->get();
                $done = ClientTask::where('client_id', $auth_user->id)->where('status','done')->get();
            } else {

                //check if user is owner of the project_id
                $project = Project::whereIn('user_id', $clients)->where('id', $project_id)->get();
                if (count($project)==0) {
                    return redirect('/dashboard/taskboard');
                }

                $todos = ClientTask::where('project_id', $project_id)->whereIn('client_id', $clients)->where('status','todo')->get();
                $in_progress = ClientTask::where('project_id', $project_id)->whereIn('client_id', $clients)->where('status','in-progress')->get();
                $done = ClientTask::where('project_id', $project_id)->whereIn('client_id', $clients)->where('status','done')->get();
            }
            
            $projects = Project::whereIn('user_id', $clients)->get();
            return view('admin.taskboard.index',compact('todos', 'in_progress', 'done', 'projects', 'project_id'));

        } else {
            $projects = Project::get();

            if ($project_id == null) {
                $todos = ClientTask::where('status','todo')->get();
                $in_progress = ClientTask::where('status','in-progress')->get();
                $done = ClientTask::where('status','done')->get();
            } else {

                //check project exists
                $project = Project::where('id', $project_id)->get();
                if (count($project)==0) {
                    return redirect('/dashboard/taskboard');
                }

                $todos = ClientTask::where('project_id', $project_id)->where('status','todo')->get();
                $in_progress = ClientTask::where('project_id', $project_id)->where('status','in-progress')->get();
                $done = ClientTask::where('project_id', $project_id)->where('status','done')->get();
            }

            return view('admin.taskboard.index',compact('todos', 'in_progress', 'done', 'projects', 'project_id'));

        }

    }

    public function changeStatus(Request $request)
    {
        $task_id = $request->task_id;
        $status = $request->status;

        $q = ClientTask::find($task_id);

        if (!empty($q)) {
            $q->status = $status;

            if ($status == 'done') {
                $q->completed_at =  date("Y-m-d H:i:s", strtotime(now()));
            } else {
                $q->completed_at = null;
            }
            $q->save();
        }
    }
}