@extends('layouts.adminlte-master')
@section('page-title', "VA Screeshots")

@section('content')
@include('admin.delete-modal')
@include('admin.preview-modal')

@push('view-styles')
<style>
    #get-next, #get-prev{
      background: #f0f0f0;
      font-size: 20px;
      font-weight: bold;
      border-radius: 50%;
      padding: 4px 10px;
      border: 1px solid #ccc;
    }
</style>
@endpush

<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Screenshots of {{$user->first_name}} {{$user->last_name}}</h3>
                </div>
                <div class='box-body'>
                    <div class='row'>
                        <div class='col-md-3'>
                            <label>Select Date</label>
                            <div class="input-group date" id="datepicker">
                            <input type="text" id='target_date' name='target_date' class="form-control input-lg" value="{{date('Y-m-d', strtotime(now()))}}">
                            <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                            </div>
                            </div>
                        </div>

                        <div class='col-md-3'>
                            <label>Sort by Activity</label>
                            <div class="btn-toolbar">
                            <button class='btn btn-info btn-sort' id='mouse-sort' data-sort='asc'>Sort by <img src='/images/custom/mouse.png' width='32px'/></button>
                            <button class='btn btn-info btn-sort' id='keyboard-sort' data-sort='asc'>Sort by <img src='/images/custom/keyboard.png' width='32px'/></button>
                            </div>
                        </div>

                        <div class='col-md-3'>
                            <label>Start Time</label>
                            <div class="input-group clockpicker">
                            <input id="start_time" name="start_time" type="text" class="form-control input-lg" value="12:00AM">
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                            </span>
                            </div>
                        </div>

                        <div class='col-md-3'>
                            <label>End Time</label>
                            <div class="input-group clockpicker">
                            <input id="end_time" name="end_time" type="text" class="form-control input-lg" value="11:59PM">
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class='row'><hr></div>


                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="va-screenshots-table table table-bordered table-hover dataTable" id="va-screenshots-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>Thumb</th>
                                        <th>Time Taken</th>
                                        <th>Mouse Activity</th>
                                        <th>Keyboard Activity</th>
                                        <th>Created</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<link rel="stylesheet" href="{{ asset('css/bootstrap-clockpicker.min.css') }}" />
@endpush

@push('view-styles')
<style>
    .va-screenshots-table tr {
        float:left;
        width:220px;
        height:350px;
        border:1px solid #dbdbdb;
        margin:5px;
    }
    .va-screenshots-table thead {
        display:none;
    }
    .va-screenshots-table tr td:nth-child(3) {
        width:100px;
        float:left;
        padding:0;
        padding-left:8px;
    }
    .va-screenshots-table tr td:nth-child(4) {
        width:100px;
        float:left;
        padding:0;
        padding-left:8px;
    }

    .va-screenshots-table tr td {
        float:left;
        width:210px;
        border: none !important;
    }

    .btn-sort {
        width:100px;
    }

    #prev-slide{
        position: absolute;
        top: 50%;
        left: 0;
        cursor: pointer;
        margin-left:30px;
    }

    #next-slide{
        position: absolute;
        top: 50%;
        right: 0;
        cursor: pointer;
        margin-right: 30px;
    }
</style>
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="{{ asset('js/bootstrap-clockpicker.min.js') }}"></script>

<script>
var _table;

function fetchData(target_date, start_time, end_time) {
    $('#va-screenshots-table').DataTable().destroy();

    _table = $('#va-screenshots-table').DataTable({
        processing: true,
        serverSide: true,
        searching: false,
        ajax: {
            url:'/dashboard/screenshots/{{$user->id}}/datatables',
            data: {
                target_date : target_date,
                start_time: start_time,
                end_time: end_time,
                type : 'GET'
            }
        },
        "columns": [
                { "data": "path" },
                { "data": "time_taken" },
                { "data": "mouse_activity" },
                { "data": "keyboard_activity" },
                { "data": "created_at", visible: false },
                { "data": "actions", width: "150px", orderable: false, searchable: false},
        ],
        "order": [[ 4, "desc" ]],
        'drawCallback': function(settings) {
            if ($('.va-screenshots-table tr').length==2 && $('.va-screenshots-table tr').find('td').hasClass('dataTables_empty')) {
                $('.va-screenshots-table tr').css("height", "auto");
                $('.va-screenshots-table tr').css("width", "100%");
                $('.va-screenshots-table tr').css("margin", "0");
            } else {
                $('.va-screenshots-table tr').css("height", "350px");
                $('.va-screenshots-table tr').css("width", "220px");
                $('.va-screenshots-table tr').css("margin", "5px");
            }
        }
    });

}


$(function() {
    fetchData();

    $('#datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    }).on('changeDate', function(e) {
        start_time = $("#start_time").val();
        end_time = $("#end_time").val();
        target_date = $("#target_date").val();
        fetchData(target_date, start_time, end_time);
    });

    $('.clockpicker').clockpicker({
        placement: 'bottom',
        align: 'left',
        donetext: 'Set',
        twelvehour:true,
        afterDone: function() {
            start_time = $("#start_time").val();
            end_time = $("#end_time").val();
            target_date = $("#target_date").val();
            fetchData(target_date, start_time, end_time);
        }
    });



    $("#mouse-sort").click(function(){
        _sort = $(this).data('sort');
        _table
            .order( [ 2, _sort ] )
            .draw();
        if (_sort=='asc') {
            $(this).data('sort', 'desc');
        } else {
            $(this).data('sort', 'asc');
        }
    });

    $("#keyboard-sort").click(function(){
        _sort = $(this).data('sort');
        _table
            .order( [ 3, _sort ] )
            .draw();
        if (_sort=='asc') {
            $(this).data('sort', 'desc');
        } else {
            $(this).data('sort', 'asc');
        }
    });
});

     var temp_delete_id = 0;

    //this shows the preview modal
    $(document).on("click", ".button-preview", function(){
        var id = $(this).attr('data-id');
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/screenshots/preview/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('.modal-body .content').append('<div id="next-slide"><a href="#" class="prev-next" rel="next" id="get-next"> > </a></div>');
            $('.modal-body .content').append('<div id="prev-slide"><a href="#" class="prev-next" rel="prev" id="get-prev"> < </div>');
            $('#modal-preview').modal('show');
        });
    });

    //this shows the modal
    $(document).on("click", ".button-delete", function(){
        var id = $(this).attr('data-id');
        temp_delete_id = id;
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/screenshots/show/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('.modal-body .content').append('<div id="next-slide"><a href="#" class="prev-next" rel="next" id="get-next"> > </a></div>');
            $('.modal-body .content').append('<div id="prev-slide"><a href="#" class="prev-next" rel="prev" id="get-prev"> < </div>');
            $('#modal-warning').modal('show');
        });

    });

    $(document).on("click", ".prev-next", function(){

           var imgsrc = $('.modal-body .content').find('div .img-responsive').attr('src').split('/');
           var position = $(this).attr('rel');
            $('.modal-body .content').html("");

            var target_img = $("img[src$='"+imgsrc[6]+"']");

            if(position == 'next'){
                new_target = target_img.closest('tr').next().find('img');
            }else{
                new_target = target_img.closest('tr').prev().find('img');
            }

            new_name = new_target.parent('td').next('td');
            new_date = new_name.next('td');

            var new_img = new_target.attr('src').replace('thumb_', '');
            $('.modal-body .content').html('<div><img width="838" height="475" class="img-responsive img-thumbnail" src="'+new_img+'"></div><br><strong>'+new_name.text()+'</strong><br><small>'+new_date.text()+'</small></div>');

            $('.modal-body .content').append('<div id="next-slide"><a href="#" class="prev-next" rel="next" id="get-next"> > </a></div>');
            $('.modal-body .content').append('<div id="prev-slide"><a href="#" class="prev-next" rel="prev" id="get-prev"> < </div>');
    });

    //this does the actual delete
    $("#button-delete").click(function(){
        $(this).html('<i class="fa fa-spin fa-refresh"></i> Delete');
        $.ajax({
            url: "/dashboard/screenshots/delete/"+temp_delete_id,
            dataType: "json"
        }).done(function(data) {
            $('#modal-warning').modal('hide');
            $("#button-delete").html('Delete');
            _table.ajax.reload( null, false );
        });
    });

</script>

@endpush
