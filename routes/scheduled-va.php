<?php

Route::group(array(
    'middleware' => ['web', 'admin.auth'],
    ), function() {	

        Route::get('/dashboard/scheduled-va', [
            'uses' => 'Admin\ScheduledVAController@index'
        ]);

});