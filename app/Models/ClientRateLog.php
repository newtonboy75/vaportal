<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientRateLog extends Model
{
    protected $table = "client_rates_log";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'va_id', 'old_rate_per_hour', 'new_rate_per_hour', 'updated_by'];
}
