<table class="table table-bordered table-hover" id="bills-table" role='grid' width='100%'>
    <thead>
        <tr>
            <th style='text-align:center'>Date</th>
            <th style='text-align:center'>Service</th>
            <th style='text-align:center'>Activity</th>
            <th style='text-align:center'>Time</th>
            <th style='text-align:center'>Rate</th>
            <th style='text-align:center'>Amount</th>
        </tr>
    </thead>
    <tbody>

        @if (count($timesheet)==0)
            <tr>
            <td colspan="6">
                <center>No data available in table</center>
            </td>
            </tr>
        @else
        @php
            $overall_total = 0;
        @endphp
            @foreach ($timesheet as $ts)
            @php
                $rate = $ts->client->getRateBilling($ts->user_id);
                $total = $ts->total_billable_hours * $rate;
                $overall_total = $overall_total + $total;
            @endphp
            <tr>
                <td>{{$ts->schedule_date}}</td>
                <td>{{$ts->user->first_name}} {{$ts->user->last_name}}</td>
                <td>Regular Hours</td>
                <td style='text-align:right'>{{$ts->total_billable_hours}}</td>
                <td style='text-align:center'>{{number_format($rate,2)}}</td>
                <td style='text-align:center'>{{number_format($total,2)}}</td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>

@if (count($timesheet)!=0)
    @php
        $overall_total = $overall_total + $setup_fee;
        $total_tax = $overall_total * ($tax/100);
        $amount_due = ( $overall_total + $total_tax ) - $advance_payments - $credits;
        $amount_due_text = "AMOUNT DUE:";
    @endphp
    @if ($amount_due<0)
        @php
            $amount_due = number_format(abs($amount_due),2);
            $amount_due = "(" .$amount_due . ")";
            $amount_due_text = "AMOUNT DUE: (ADVANCES BALANCE)";
        @endphp
    @else
        @php
        $amount_due = number_format($amount_due,2);
        @endphp
    @endif
    <table class="table table-bordered table-hover" role='grid' width='100%'>
        <tbody>
            @if ($setup_fee!=0)
            <tr>
                <td class='text-left'>SETUP FEE:</td>
                <td class='text-right'>{{number_format($setup_fee,2)}}</td>
            </tr>
            @endif
            <tr>
                <td class='text-left'>INVOICE TOTAL:</td>
                <td class='text-right'>{{number_format($overall_total,2)}}</td>
            </tr>

            <tr>
                <td class='text-left'>TAXES:</td>
                <td class='text-right'>{{number_format($total_tax, 2)}}</td>
            </tr>
            @if ($advance_payments!=0)
            <tr>
                <td class='text-left'>LESS ADVANCES:</td>
                <td class='text-right'>{{number_format($advance_payments, 2)}}</td>
            </tr>
            @endif
            @if ($credits!=0)
            <tr>
                <td class='text-left'>LESS CREDITS: {{$credit_text}}</td>
                <td class='text-right'>{{number_format($credits, 2)}}</td>
            </tr>
            @endif
            <tr>
                <td class='text-left'>{{$amount_due_text}}</td>
                <td class='text-right'>{{$amount_due}}</td>
            </tr>
        </tbody>
    </table>
@endif