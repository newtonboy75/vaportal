@extends('layouts.timedly.login')

@section('pageTitle')
    VA Time Tracker | Login
@endsection

@section('pageScripts')
<script>
    var updated = false;
    $(document).ready(function () {
        @php
            if(session()->has('error_msg')):
                $message = session()->get('error_msg');
                $type = 'error';
                $title = 'Something went wrong.';
        @endphp

        swal({
            title: '{{$title}}',
            html: '{!!$message!!}',
            type: '{{$type}}',
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary'
        });

        @php
            endif;
        @endphp
    });
</script>
@endsection

@section('content')
    <!-- Page content -->
    <div class="container">
        <div class="row justify-content-center">
        <div style="position: absolute; width: 100%; text-align:center; z-index: 9999; margin-top: -180px"><img alt="Virtudesk" width="130" src="https://virtudeskpro.com/img/backend/brand/virtudesk_logo_final.png">&nbsp;&nbsp;<h6 style="color: #f0f0f0; font-size: 24px; font-weight: 600; margin-top: 8px;">timedly</h6><img src="{{ asset('images/timedly/brand/blue.png') }}" class="navbar-brand-img" alt="..." style="display: none">
        </div>
            <div class="col-lg-5 col-md-7">
            
                <div class="card bg-secondary shadow border-0 mt--5">
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>Sign in with credentials</small>
                        </div>
                        <form method="POST" action="{{url('/auth/login')}}">
                            {{ csrf_field() }}
                            <div class="form-group mb-3">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Email" name="email" type="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Password" name="password" type="password">
                                </div>
                            </div>
                            <div class="custom-control custom-control-alternative custom-checkbox">
                                <input class="custom-control-input" id=" customCheckLogin" type="checkbox">
                                <label class="custom-control-label" for=" customCheckLogin">
                                    <span class="text-muted">Remember me</span>
                                </label>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary my-4">Sign in</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-12">
                        <a href="/auth/reset" class="text-primary"><small>Forgot password?</small></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
