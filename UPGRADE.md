Run these commands after upgrade

```bash
php artisan key:generate
mkdir -p storage/framework/cache/data
php artisan view:clear
```
