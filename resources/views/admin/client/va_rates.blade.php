@extends('layouts.adminlte-master')
@section('page-title', "Rate per VA ($q->full_name)")

@section('content')

@include('admin.client.va_rates_modal')
@include('admin.client.va_rates_add_modal')

<input type="hidden" id="client-id" value="{{ $q->id }}">
@if (isset($q->client_rates->rate_per_hour_formatted))
<input type="hidden" id="client-rate-per-hour" value="{{ $q->client_rates->rate_per_hour_formatted }}">
@else
<input type="hidden" id="client-rate-per-hour" value="0">
@endif
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Rate per VA ({{ $q->full_name }})</h3>
                    <span class="pull-right"><a href="#" class="btn btn-primary btn-add-rate-to-va"><i class="fa fa-plus"></i> Add VA Rate to Client</a></span>
                </div>
                <div class='box-body'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover dataTable" id="vas-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>VA</th>
                                        <th>Rate per Hour</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script>
    var _table;
    $(function() {
        _table = $('#vas-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/dashboard/client-va-rates/{{$q->id}}/datatables',
            "columns": [
                    { "data": "va_name" },
                    { "data": "rate_per_hour" },
                    { "data": "action", "orderable": false, 'width': '40' },
            ]
        });
    });

    $(document).on("click", ".btn-va-rate-update", function() {
        var _this = $(this),
            rate_id = _this.data('id'),
            modal_title = _this.data('title'),
            rate_per_hour = _this.data('rate-per-hour'),
            _modal = $("#modal-client-va-rate-update");

    
        $('.modal-title', _modal).text(modal_title);
        $('.client-rate-id', _modal).val(rate_id);
        $('.client-rate-per-hour', _modal).val(rate_per_hour);
        $('.modal-notification', _modal).hide();
        $('.modal-notification', _modal).removeClass('alert-error');
        $('.modal-notification', _modal).removeClass('alert-success');
        $("#modal-client-va-rate-update").modal("show");
    });

    $(document).on("click", "#button-update-va-client-rate", function() {
        var _modal = $("#modal-client-va-rate-update"),
            rate_id = $('.client-rate-id', _modal).val(),
            rate_per_hour = $('.client-rate-per-hour').val(),
            modal_title = $('.modal-title', _modal).text();

        $('.modal-title', _modal).html('<i class="fa fa-spin fa-refresh"></i> Saving');

        $.ajax({
            url: "/dashboard/client-va-rate-update/"+rate_id,
            dataType: "json",
            data: {
               "rate_per_hour": rate_per_hour
            },
        }).done(function(response) {
            $('.alert', _modal).removeClass('alert-success, alert-error');
            if(response.status === "success") {
                $('.alert', _modal).addClass('alert-success');
                $('.alert', _modal).html( response.message );
                
                _table.ajax.reload( null, false );

                $('.alert', _modal).show();
            } else {
                $('.alert', _modal).addClass('alert-error');
                $('.alert', _modal).show();
            }

            $('.modal-title', _modal).html( modal_title );
            $('.alert', _modal).html( response.message );
            $('.alert', _modal).show();

        });
    });

    $(document).on("click", ".btn-add-rate-to-va", function(e) {
        e.preventDefault();

        var _modal = $('#modal-client-va-rate-add'),
            _modal_title = "Add VA Rate for {{ $q->full_name }}";

        $.ajax({
            url: "/dashboard/client-va-rates/{{ $q->id }}/available-vas",
            dataType: "json",
        }).done(function(response) {

            if( response.status == "success") {
                
                
                $('.vas-html', _modal).html(response.vas_html);

                $('.modal-notification', _modal).hide();
                $('.modal-title', _modal).html(_modal_title); 
                _modal.modal("show");
            }
        });
    });

    $(document).on("click", "#button-add-va-client-rate", function() {
        var _modal = $("#modal-client-va-rate-add"),
            va_id = $("#va-id", _modal).val(),
            client_id = $("#client-id").val(),
            rate_per_hour = $(".client-rate-per-hour", _modal).val(),
            old_rate_per_hour = $("#client-rate-per-hour").val()

        $.ajax({
            url: "/dashboard/client-va-rate-add/"+client_id, 
            dataType: "json",
            data: {
                "rate_per_hour": rate_per_hour,
                "va_id": va_id,
                "old_rate_per_hour": old_rate_per_hour
            }
        }).done(function(response) {
            if(response.status == "success") {
                $(".modal-notification", _modal).hide();
                _modal.modal("hide");
                _table.ajax.reload( null, false );
            } else {
                $(".modal-notification", _modal).addClass("alert-error");
                $(".modal-notification", _modal).html(response.message);
                $(".modal-notification", _modal).show();
            
            }
        })
    });

    $(document).on("submit", 'form[name="client-va-add-rate"]', function(e) {
        e.preventDefault();
        $("#button-add-va-client-rate").trigger("click");  
    })

    $(document).on("submit", 'form[name="client-va-update-rate"]', function(e) {
        e.preventDefault();
        $("#button-update-va-client-rate").trigger("click");  
    })
    

</script>

@endpush