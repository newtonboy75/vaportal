<?php
//TODO: set permissions
Route::group(array(
    'middleware' => ['web', 'admin.auth'],
    ), function() {	

        Route::get('/dashboard/daily-reports', [
            'uses' => 'Admin\DailyReportsController@index'
        ]);

        Route::get('/dashboard/daily-reports/generate', [
            'uses' => 'Admin\DailyReportsController@generateDailyReports'
        ]);

        Route::get('/dashboard/daily-reports/generate/pdf', [
            'uses' => 'Admin\DailyReportsController@generatePDF'
        ]);
});