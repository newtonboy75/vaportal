<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
use App\Models\ActivityLog;
use App\Models\ClientRate;
use App\Models\ClientRateLog;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    public function index()
    {
        return view('admin.client.index');
    }

    public function create()
    {
        $timezone = config('vatimetracker.timezone');
        $managers = User::HasRole('manager')->get();
        return view('admin.client.create_edit', compact('timezone', 'managers'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:users',
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
            'alternate_contact_number' => 'max:100',
            'mobile_number' => 'max:32',
            'address1' => 'max:255',
            'address2' => 'max:255',
            'city' => 'max:50',
            'rate_per_hour' => 'numeric',
        ]);

        $user = User::create([
            'email' => $request->email,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'alternate_contact_number' => $request->alternate_contact_number,
            'mobile_number' => $request->mobile_number,
            'address1' => $request->address1,
            'address2' => $request->address2,
            'city' => $request->city,
            'timezone' => $request->timezone,
            'is_verified' => 1,
            'is_active' => 1,
            //'screenshot_interval' => $request->screenshot_interval,
            //'idle_interval' => $request->idle_interval,
            'password' => bcrypt($request->password),
        ]);

        $user->addRole("client");

        $rate = ClientRate::create([
            'user_id' => $user->id,
            'va_id' => 0,
            'rate_per_hour' => $request->rate_per_hour,
        ]);

            //add to manager client table
            foreach ($request->manager_id as $manager_id) {
                DB::table('manager_client')->insert(
                    [
                        'user_id' => $manager_id, 
                        'client_id' => $user->id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    ]
                );
            }

        ClientRateLog::create(array(
            'user_id' => $user->id,
            'va_id' => 0,
            'old_rate_per_hour' => 0,
            'new_rate_per_hour' => $request->rate_per_hour,
            'updated_by' => \Auth::user()->id,
        ));

        ActivityLog::addLog("Created client [{$user->first_name} {$user->last_name}] with ID: " . $user->id);
        return redirect('/dashboard/clients/edit/'. $user->id)->with('notification_message', 'Client has been created.');
    } 

    public function edit($id)
    {
        $q = User::find($id);
        $timezone = config('vatimetracker.timezone');
        if (empty($q)) {
            return redirect('/dashboard');            
        }
        $managers = User::HasRole('manager')->get();
        $current_managers = DB::table('manager_client')->where('client_id', $id)->get();

        if (count($current_managers)<=0) {
            $current_manager_id = array();
        } else {
            foreach ($current_managers as $manager)
                $current_manager_id[] = $manager->user_id;
        }

        return view('admin.client.create_edit', compact('q','timezone', 'managers', 'current_manager_id'));
    }

    public function update(Request $request) 
    {
        $q = User::find($request->id);

        $request->validate([
            'email' => [
                'required',
                Rule::unique('users')->ignore($q->id),
            ],
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'confirm_password' => 'same:password',
            'alternate_contact_number' => 'max:100',
            'mobile_number' => 'max:32',
            'address1' => 'max:255',
            'address2' => 'max:255',
            'city' => 'max:50',
            'rate_per_hour' => 'numeric',
        ]);

        if (!empty($q)) {
            $q->first_name = $request->first_name;
            $q->last_name = $request->last_name;
            $q->email = $request->email;
            $q->alternate_contact_number = $request->alternate_contact_number;
            $q->mobile_number = $request->mobile_number;
            $q->address1 = $request->address1;
            $q->address2 = $request->address2;
            $q->city = $request->city;
            $q->timezone = $request->timezone;
            $q->is_active = $request->is_active;
            //$q->screenshot_interval = $request->screenshot_interval;
            //$q->idle_interval = $request->idle_interval;
            $q->save();

            if($q->client_rates) {
                if($q->client_rates->rate_per_hour !== $request->rate_per_hour) {

                    $q->client_rates->addLog(array(
                        'user_id' => $q->id,
                        'va_id' => 0,
                        'old_rate_per_hour' => $q->client_rates->rate_per_hour,
                        'new_rate_per_hour' => $request->rate_per_hour,
                        'updated_by' => \Auth::user()->id,
                    ));

                    $q->client_rates->rate_per_hour = $request->rate_per_hour;
                    $q->client_rates->save();
                }
            } else {
                $rate = ClientRate::create([
                    'user_id' => $q->id,
                    'va_id' => 0,
                    'rate_per_hour' => $request->rate_per_hour,
                ]);

                ClientRateLog::create(array(
                    'user_id' => $q->id,
                    'va_id' => 0,
                    'old_rate_per_hour' => 0,
                    'new_rate_per_hour' => $request->rate_per_hour,
                    'updated_by' => \Auth::user()->id,
                ));

            }

            //remove previous
            DB::table('manager_client')->where('client_id', $q->id)->delete();

            //add to manager client table
            foreach ($request->manager_id as $manager_id) {
                DB::table('manager_client')->insert(
                    [
                        'user_id' => $manager_id, 
                        'client_id' => $q->id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    ]
                );
            }

            ActivityLog::addLog("Updated client [{$request->first_name} {$request->last_name}] with ID: " . $q->id);
        }

        return redirect()->intended('/dashboard/clients/edit/'. $q->id)->with('notification_message', 'Client has been updated.'); 

    }  

    public function show($id)
    {
        $q = User::find($id);
        $html = view('admin.client.show', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }

    public function delete($id)
    {
        $q = User::find($id);
        if (!empty($q)) {
            ActivityLog::addLog("Deleted client [{$q->first_name} {$q->last_name}] with ID: " . $q->id);
            $q->delete();

            DB::table('attendance')->where('user_id',$id)->delete();
            DB::table('client_tasks')->where('va_id',$id)->delete();
            DB::table('client_tasks')->where('client_id',$id)->delete();
            DB::table('dispute')->where('user_id',$id)->delete();
            DB::table('idles')->where('user_id',$id)->delete();
            DB::table('process_lists')->where('user_id',$id)->delete();
            DB::table('rates')->where('user_id',$id)->delete();
            DB::table('rates_log')->where('user_id',$id)->delete();
            DB::table('reports')->where('user_id',$id)->delete();
            DB::table('requests')->where('user_id',$id)->delete();
            DB::table('schedules')->where('user_id',$id)->delete();
            DB::table('screenshots')->where('user_id',$id)->delete();
            DB::table('team_user')->where('user_id',$id)->delete();
            DB::table('teams')->where('lead_user_id',$id)->delete();
            DB::table('time_in_out')->where('user_id',$id)->delete();
            DB::table('timesheets')->where('user_id',$id)->delete();
            DB::table('timesheets')->where('client_id',$id)->delete();
            DB::table('user_tasks')->where('user_id',$id)->delete();
            DB::table('user_tasks')->where('client_id',$id)->delete();
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table('role_user')->where('user_id',$id)->delete();
            DB::table('manager_client')->where('client_id',$id)->delete();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }        
        $response['status'] = "ok";
        return json_encode($response);
    }

    public function datatables()
    {
        $auth_user = \Auth::user();
        $rq = request()->active;
        $users = User::HasRole('client')->select(['id','first_name', 'last_name','email','mobile_number'])->where('is_active', $rq); 

        return DataTables::of($users)
            ->addColumn('actions', function($user) use ($auth_user){

                $delete_btn = '';
                if ($auth_user->allowed('delete_clients')) {
                    $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$user->id.'"><i class="fa fa-trash"></i></a>';
                }
                $edit_btn = '';
                if ($auth_user->allowed('edit_clients')) {
                    $edit_btn = '<a class="btn btn-info" href="/dashboard/clients/edit/'.$user->id.'"><i class="fa fa-edit"></i></a>';    
                }

                $rate_btn = "";
                if ($auth_user->allowed('edit_clients') || ($auth_user->is('billing')) ) {
                    $rate_btn = '<a class="btn btn-info" href="/dashboard/client-va-rates/'.$user->id.'" title="Edit VA Rate per Hour"><i class="fa fa-money"></i></a>';
                }
                if ($user->id!=1) {
                    return '<div class="btn-toolbar">'. $edit_btn . $rate_btn .  $delete_btn .'</div>';
                } else {
                    return '<div class="btn-toolbar">'. $edit_btn . $rate_btn . '</div>';
                }

        })->rawColumns(['actions'])
        ->make(true);        

    }


    public function client_va_rate($id) {
        $q = User::find($id);
        if (empty($q)) {
            return redirect('/dashboard');
        }

        return view('admin.client.va_rates', compact('q'));
    }

    public function client_va_rate_update($id, Request $request) {
        $validator = \Validator::make($request->all(), [
            'rate_per_hour' => 'numeric'
        ]);
        
        if ($validator->fails()) {
            $response["status"] = "error";
            $response["message"] = "Invalid Rate per hour";
            return json_encode($response);
        }

        $rate = ClientRate::find($id);

        if( $rate ) {
            $old_rate_per_hour = $rate->rate_per_hour;
            $rate->rate_per_hour = $request->rate_per_hour;
            $rate->save();

            $rate->addLog(array(
                'user_id' => $rate->user_id,
                'va_id' => $rate->va_id,
                'old_rate_per_hour' => $old_rate_per_hour,
                'new_rate_per_hour' => $request->rate_per_hour,
                'updated_by' => \Auth::user()->id,
            ));

            $response['message'] = 'Rate has been successfully updated!';
            $response['status'] = 'success';
            return json_encode($response);
        } else {
            $response['message'] = 'Failed to update rate!';
            $response['status'] = 'error';
            return json_encode($response);
        }
    }

    public function client_va_rate_datatables($id) {
        $rates = ClientRate::select(['client_rates.id', 'first_name', 'va_id', 'user_id', 'rate_per_hour'])
                       ->leftJoin('users', 'users.id', '=', 'client_rates.va_id')
                       ->where('user_id', $id)->where('va_id', '!=', 0);
        return DataTables::of($rates)
            ->addColumn('va_name', function ($rate) {
                return $rate->va->full_name;
            })

            ->addColumn('action', function($rate) {
                $title = 'Update Rate of ' . $rate->client->full_name .' for ' . $rate->va->full_name;
                return '<a class="btn btn-info btn-va-rate-update" href="#" data-id="' . $rate->id . '" data-title="' . $title . '" data-rate-per-hour="' . $rate->rate_per_hour_formatted . '"><i class="fa fa-edit"></i></a>';   
            })
            ->editColumn('rate_per_hour', function($rate) {
                return number_format( $rate->rate_per_hour, 2, '.', '' );
            })
            ->orderColumn('client_name', 'first_name $1')
            ->rawColumns(['action'])
            ->make(true);
    }

    public function client_va_rate_available_va($id) {
        $vas_html = '';
        $current_vas = ClientRate::select(['va_id'])->where('user_id', $id)->where('va_id', '!=', '0')->get();

        $vas = User::HasRole('va')
                   ->select(['id', 'first_name', 'last_name'])
                   ->whereNotIn('id', $current_vas)
                   ->orderBy('first_name', 'ASC')
                   ->get();


        if( !empty($vas) ) {
            $vas_html .= '<select class="form-control" id="va-id">';
            foreach($vas as $va) {
                $vas_html .= '<option value="' . $va->id . '">' . $va->full_name . '</option>'; 
            }
            $vas_html .= '</select>';
        } else {
            $vas_html = 'All VAs has been assigned to this Client or no VA available';
        }

        $response["status"] = "success";
        $response["vas_html"] = $vas_html;
        return json_encode($response);
    }

    public function client_va_rate_add($id, Request $request) {

        $validator = \Validator::make($request->all(), [
            'va_id' => 'required',
            'rate_per_hour' => 'numeric'
        ]);
        
        if ($validator->fails()) {
            $response["status"] = "error";
            $response["message"] = "Invalid Rate per hour";
            return json_encode($response);
        } else {

            $rate = ClientRate::where('user_id', $id)->where('va_id', $request->va_id)->first();
            if($rate) {
                $response["status"] = "error";
                $response["message"] = "rate is already existing";
                return json_encode($response);
            }

            $rate = ClientRate::create([
                'user_id' => $id,
                'va_id' => $request->va_id,
                'rate_per_hour' => $request->rate_per_hour,
            ]);

            $rate->addLog(array(
                'user_id' => $rate->user_id,
                'va_id' => $rate->va_id,
                'old_rate_per_hour' => $request->old_rate_per_hour,
                'new_rate_per_hour' => $request->rate_per_hour,
                'updated_by' => \Auth::user()->id,
            ));

            $response["status"] = "success";
            $response["message"] = "Rate has been added";
            return json_encode($response);
        }
        
    }

}