<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientTask extends Model {

    protected $table = "client_tasks";
    public $timestamps = false;

    const STATUS_TODO = 'todo';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_DONE = 'done';

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['id', 'priority','client_id', 'va_id', 'name', 'description', 'date_due'];

    public function va()
    {
        return $this->belongsTo("App\Models\User", "va_id");
    }

    public function client()
    {
        return $this->belongsTo("App\Models\User", "client_id");
    }

    public function project()
    {
        return $this->belongsTo("App\Models\Project", "project_id");
    }
}