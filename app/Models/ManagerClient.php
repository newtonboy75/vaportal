<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ManagerClient extends Model {

    protected $table = "manager_client";
    
    protected $fillable = ['user_id', 'client_id'];

    /**
     * Fillable property.
     *
     * @var array
     */

}