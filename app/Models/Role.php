<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

	protected $table = "roles";

	/**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'description'];

    /**
     * Relation to "User".
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('users')->withTimestamps();
    }    


    /**
     * Relation to "Permission".
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany("App\Models\Permission")->withTimestamps();
    }

    /**
     * Attach permission to role.
     *
     * @param mixed $id
     */
    public function addPermission($id)
    {
        $this->permissions()->attach($id);
    }    


   /**
     * Detach permission from role.
     *
     * @param mixed $ids
     */
    public function removePermission($ids)
    {
        $ids = is_array($ids) ? $ids : func_get_args();
        $this->permissions()->detach($ids);
    }    


    /**
     * Remove all permissions from role.
     */
    public function clearPermissions()
    {
        $this->removePermission($this->permissions->lists('id'));
    }    

    /**
     * Determine wether the current role has permission that given by name parameter.
     *
     * @param string $name
     *
     * @return bool
     */
    public function can($name)
    {
        foreach ($this->permissions as $permission) {
            if ($permission->name == $name || $permission->id == $name) {
                return true;
            }
        }
        return false;
    }    
}