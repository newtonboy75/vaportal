@extends(Auth::user()->roles[0]['slug'] == 'manager' ? 'layouts.adminlte-master' : 'layouts.adminlte-master2')
@section('page-title', "Virtual Assistants")

@section('content')
@include('admin.remove-user-modal')
@include('admin.delete-modal')
 @if (session()->has('notification_message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Success!</h4>
        {{ session()->get('notification_message') }}
    </div>
    @endif  
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Virtual Assistants</h3>
                    @if (Auth::user()->allowed('add_va'))
                    <span class="pull-right"><a href="/dashboard/va/create" class="btn btn-primary"><i class="fa fa-plus"></i> Create Virtual Assistant</a></span>
                    @endif
                </div>
                <div class='box-body'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-sm table-striped table-hover nowrap"  cellspacing="0" id="vas-table" width='100%'>
                                <thead style="background-color: #FFF !important;">
                                    <tr>
                                        <th>ID</th>
                                        <th>First Name</th>
                                        <th>Middle Name</th>
                                        <th>Last Name</th>
                                        <th>Name Suffix</th>
                                        <th>Email</th>
                                        <th>Personal Email</th>
                                        <th>Civil Status</th>
                                        <th>Gender</th>
                                        <th>Mobile #</th>
                                        <th>Alt #</th>
                                        <th>Landline #</th>
                                        <th>Date of Birth</th>
                                        <th>Address</th>
                                        <th>Zipcode</th>
                                        <th>Location</th>
                                        <th>Facebook</th>
                                        <th>Skype</th>
                                        <th>Contact Person</th>
                                        <th>Contact Person #</th>
                                        <th>Contact Person Relationship</th>
                                        <th>Contract Date</th>
                                        <th>Encoded Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('view-styles')

<link href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet">

<style>
th, td { white-space: nowrap;}
div.dataTables_wrapper {
  width: 100%;
  margin: 0 auto;
}
.dt-buttons{
margin-bottom: 20px !important;
}
</style>
@endpush

@push('view-scripts')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.3.3/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
<script>

var _table;
$(function() {
    _table = $('#vas-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/dashboard/va/datatables',
        scrollY:        "496px",
        scrollX:        true,
        scrollCollapse: false,
        fixedColumns:   {
            leftColumns: 1,
            rightColumns: 1,
        },
       dom: 'Blfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
        ],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "columns": [
                { "data": "id" },
                { "data": "first_name" },
                { "data": "middle_name" },
                { "data": "last_name" },
                { "data": "name_suffix" },
                { "data": "email"},
                { "data": "personal_email"},
                { "data": "civil_status"},
                { "data": "gender"},
                { "data": "mobile_number"},
                { "data": "alternate_contact_number"},
                { "data": "landline"},
                { "data": "birthdate"},
                { "data": "address1"},
                { "data": "zip"},
                { "data": "location"},
                { "data": "facebook"},
                { "data": "skype_id"},
                { "data": "contact_person_name"},
                { "data": "contact_person_number"},
                { "data": "contact_person_relationship"},
                { "data": "contract_date"},
                { "data": "created_at"},
                { "data": "actions", width: "300px", orderable: false, searchable: false},
        ]
    });
});

     var temp_delete_id = 0;

    //this shows the modal
    $(document).on("click", ".button-delete", function(){
        var id = $(this).attr('data-id');
        temp_delete_id = id;
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
        $.ajax({
            url: "/dashboard/va/show/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('#modal-warning').modal('show');
        }); 
    });

    //this does the actual delete
    $("#button-delete").click(function(){
        $(this).html('<i class="fa fa-spin fa-refresh"></i> Delete');
        $.ajax({
            url: "/dashboard/va/delete/"+temp_delete_id,
            dataType: "json"
        }).done(function(data) {
            $('#modal-warning').modal('hide');
            $("#button-delete").html('Delete');
            _table.ajax.reload( null, false );
        }); 
    });
    
    
        var redirect_to = '';
    
   $(document).on('click', '#logout_user', function(e){
            redirect_to = $(this).attr('href');
            e.preventDefault();
            $('.modal-body .modal-title-remove').html('Logout User');
            $('.modal-body .content-remove').html('Are you sure you want to log out this user?<p><small>User will be logged out from tracker.</small></p>');
            $('#modal-warning-remove').modal('show');
    });
    
    $(document).on('click', '#remove_from_team', function(e){
            redirect_to = $(this).attr('href');
            e.preventDefault();
            $('.modal-body .modal-title').html('Remove User from Team');
            $('.modal-body .content-remove').html('Are you sure you want to remove this user from your team?');
            $('#modal-warning-remove').modal('show');
    });
    
    $("#button-oks").click(function(){
        $('#modal-warning-remove').modal('hide');
        window.location.href = redirect_to;
    });

</script>

@endpush