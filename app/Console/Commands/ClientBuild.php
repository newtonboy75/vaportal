<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;

class ClientBuild extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'client:build {host} {port} {platform} {debug}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update and build the app. client:build {host} {port} {platform} {debug}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        define('APPLICATION_NAME', 'VA Builder');
        define('CREDENTIALS_PATH', base_path() . '/drive-va-builder.json');
        define('CLIENT_SECRET_PATH', base_path() . '/client_secret.json');
        define('SCOPES', implode(' ', array(Google_Service_Drive::DRIVE)));

        $args = $this->arguments();
        $file = base_path() . '/_client_app/src/config.json';
        $json = json_decode(file_get_contents($file));
        $json->HOST = $args['host'];
        $json->PORT = $args['port'];
        $json->DEBUG = isset($args['debug']) ? $args['debug'] : 0;
        $release = ($json->DEBUG) ? "DEBUG" : "RELEASE";
        $platform = $args['platform'];
        $clientName = 'VA Client';
        file_put_contents($file, json_encode($json, JSON_PRETTY_PRINT));

        $platforms = ['darwin', 'win32'];
        if(in_array($platform, $platforms)) {
            echo "Packaging App...\n";

            $process = '';
            if($platform == 'darwin') {
                $process = 'electron-packager ' . base_path() . '/_client_app "' . $clientName . '" --platform=' . $platform . ' --arch=all --icon=' . base_path() . '/_client_app/src/icon.icns --out=' . base_path() . '/_client_app/builds --overwrite';
            }
            if($platform == 'win32') {
                $process = 'electron-packager ' . base_path() . '/_client_app "' . $clientName . '" --platform=' . $platform . ' --arch=x64 --out=' . base_path() . '/_client_app/builds --overwrite';
            }

            // $process = 'electron-packager ' . base_path() . '/_client_app VAclient --platform=' . $platform . ' --arch=all --icon=' . base_path() . '/_client_app/src/icon.icns --out=' . base_path() . '/_client_app/builds --overwrite';
            // $process = 'electron-packager ' . base_path() . '/_client_app VAclient --platform=' . $platform . ' --arch=all --out=' . base_path() . '/_client_app/builds --overwrite';
            $this->runProcess($process);
    
            $filename = '';
            $filePath = '';
            
            if($platform == 'darwin') {
                echo "Compressing Package...\n";
                $filename = 'VAclient-darwin-x64_' . $release . '_' . date("Ymd_His") . '.tar.gz';
                $filePath = base_path() . '/_client_app/compressed/VAclient-darwin-x64_' . $release . '_' . date("Ymd_His") . '.tar.gz';
                $process = 'tar -zcvf ' . $filePath . ' -C ' . base_path() . '/_client_app/builds/' . str_replace(" ", "\ ", $clientName) . '-darwin-x64 .';
                $this->runProcess($process);

                echo "Remove Package Folder...\n";
                $process = 'rm -rf ' . base_path() . '/_client_app/builds/' . str_replace(" ", "\ ", $clientName) . '-darwin-x64/';
                $this->runProcess($process);

                echo "Compressed file " . $filename . "\n";
                echo "Uploading Compressed Package...\n";
                $this->uploadToGoogle($filename, $filePath);

                echo "Remove Compressed Package...\n";
                $process = 'rm -rf ' . base_path() . '/_client_app/compressed/' . $filename;
                $this->runProcess($process);

                echo "Done\n";
            }

            if($platform == 'win32') {

                // x64
                echo "Compressing Package...\n";
                $filename = 'VAclient-win32-x64_' . $release . '_' . date("Ymd_His") . '.tar.gz';
                $filePath = base_path() . '/_client_app/compressed/VAclient-win32-x64_' . $release . '_' . date("Ymd_His") . '.tar.gz';
                $process = 'tar -zcvf ' . $filePath . ' -C ' . base_path() . '/_client_app/builds/' . str_replace(" ", "\ ", $clientName) . '-win32-x64 .';
                $this->runProcess($process);

                echo "Remove Package Folder...\n";
                $process = 'rm -rf ' . base_path() . '/_client_app/builds/' . str_replace(" ", "\ ", $clientName) . '-win32-x64/';
                $this->runProcess($process);

                echo "Compressed file " . $filename . "\n";
                echo "Uploading Compressed Package...\n";
                $this->uploadToGoogle($filename, $filePath);

                echo "Remove Compressed Package...\n";
                $process = 'rm -rf ' . base_path() . '/_client_app/compressed/' . $filename;
                $this->runProcess($process);

                // ia32
                // echo "Compressing Package...\n";
                // $filename = 'VAclient-win32-ia32_' . $release . '_' . date("Ymd_His") . '.tar.gz';
                // $filePath = base_path() . '/_client_app/compressed/VAclient-win32-ia32_' . $release . '_' . date("Ymd_His") . '.tar.gz';
                // $process = 'tar -zcvf ' . $filePath . ' -C ' . base_path() . '/_client_app/builds/' . str_replace(" ", "\ ", $clientName) . '-win32-ia32 .';
                // $this->runProcess($process);

                // echo "Remove Package Folder...\n";
                // $process = 'rm -rf ' . base_path() . '/_client_app/builds/' . str_replace(" ", "\ ", $clientName) . '-win32-ia32/';
                // $this->runProcess($process);

                // echo "Uploading Compressed Package...\n";
                // $this->uploadToGoogle($filename, $filePath);

                // echo "Remove Compressed Package...\n";
                // $process = 'rm -rf ' . base_path() . '/_client_app/compressed/' . $filename;
                // $this->runProcess($process);

                // echo "Done\n";
            }
        } else {
            echo "Unknown Platform " . $platform;
        }
    }

    private function runProcess($process) {
        $process = new Process($process);
        $process->setTimeout(3600);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        foreach ($process as $type => $data) {
            echo "\n " . $data;
        }

        echo $process->getOutput();
    }

    private function uploadToGoogle($filename, $filePath) {
        $client = $this->getGoogleClient();
        $service = new Google_Service_Drive($client);

        $folderId = '1aJb4xOErr2WbBckoQ0Xvyx-wwlag6iHa';
        $fileMetadata = new Google_Service_Drive_DriveFile(array(
            'name' => $filename,
            'parents' => array($folderId)
        ));
        ini_set('memory_limit', '2G');
        $content = file_get_contents($filePath);
        $file = $service->files->create($fileMetadata, array(
            'data' => $content,
            'mimeType' => 'application/gzip',
            'uploadType' => 'multipart',
            'fields' => 'id'));
        printf("File ID: %s\n", $file->id);
        printf("Download Link: https://drive.google.com/open?id=%s\n", $file->id);
    }

    private function getGoogleClient() {
        $client = new Google_Client();
        $client->setApplicationName(APPLICATION_NAME);
        $client->setScopes(SCOPES);
        $client->setAuthConfig(CLIENT_SECRET_PATH);
        $client->setAccessType('offline');
      
        // Load previously authorized credentials from a file.
        // $credentialsPath = expandHomeDirectory(CREDENTIALS_PATH);
        $credentialsPath = CREDENTIALS_PATH;
        if (file_exists($credentialsPath)) {
          $accessToken = json_decode(file_get_contents($credentialsPath), true);
        } else {
          // Request authorization from the user.
          $authUrl = $client->createAuthUrl();
          printf("Open the following link in your browser:\n%s\n", $authUrl);
          print 'Enter verification code: ';
          $authCode = trim(fgets(STDIN));
      
          // Exchange authorization code for an access token.
          $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
      
          // Store the credentials to disk.
          if(!file_exists(dirname($credentialsPath))) {
            mkdir(dirname($credentialsPath), 0700, true);
          }
          file_put_contents($credentialsPath, json_encode($accessToken));
          printf("Credentials saved to %s\n", $credentialsPath);
        }
        $client->setAccessToken($accessToken);
      
        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
          $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
          file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
        }
        return $client;
      }
}
