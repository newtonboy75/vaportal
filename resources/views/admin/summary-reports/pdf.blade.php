<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        {{-- <meta charset="utf-8"> --}}
        <title>Summary Report</title>
        <link rel="stylesheet" href="{{ public_path() }}/css/pdf.css">
    </head>
<body>
<h2>Summary Reports</h2>
<div>{{$start_date}} - {{$end_date}}</div>
<br>

    <div class="section">
        <table class="table table-bordered table-hover" role='grid' width='100%'>
            <thead>
                <tr>
                    <th width="20%" style='text-align:center'>Date</th>
                    <th width="25%" style='text-align:center'>VA</th>
                    <th width="25%" style='text-align:center'>Client</th>
                    <th width="15%" style='text-align:center'>Total Time</th>
                    <th width="15%" style='text-align:center'>Total Billable</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($timesheets as $t)
                <tr>
                    <td>{{$t->target_date}}</td>
                    <td>{{$t->user->fullname}}</td>
                    <td>{{$t->client->fullname}}</td>
                    <td>{{$t->total_time}}</td>
                    <td>{{$t->billable_hours}}</td>
                </tr>
            @endforeach    
            </tbody>
        </table>

   </div>
    <br><br>

</body>
</html>