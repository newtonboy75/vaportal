@extends('layouts.master')
@section('page-title', "Error 404")

@section('content')
    <div class="container">

    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="col-xs-12">
                <a href="#" class="active" id="login-form-link">
                <img src="/images/bg/virtudesklogo.png" class="img-responsive">
                </a>
            </div>
            <h1 style='color:#ffffff;text-align:center'>Error 404 <br/></h1>
            <h3 style='color:#ffffff;text-align:center'>The page you are looking for cannot be found.</h3>
        </div>
    </div>
    </div>
@endsection

@push('view-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("/images/bg/laptop.jpg");
    </script>
@endpush

@push('view-styles')
    <style>
    body {
        padding-top: 90px;
    }
    </style>
@endpush