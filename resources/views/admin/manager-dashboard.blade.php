@extends('layouts.adminlte-master')
@section('page-title', "Dashboard")
@include('admin.preview-modal')
@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<style>

body, table{
    font-size: 14px;
}
.products-list li{
    float: left !important;
    margin-right: 20px;
    margin-bottom: 20px;
    width: 200px !important;
}

.product-img{
    float:left;
    width: 200px !important;
}

.product-img img{
    width: 200px !important;
    height: 200px !important;
}

.product-info{
    float:left;
    width: 200px !important;
    margin-left: 0px !important;
}

.product-description{
    font-size: 12px;
}
.item{
    padding: 12px !important;
}

#prev-slide{
        position: absolute;
        top: 50%;
        left: 0;
        cursor: pointer;
        margin-left:30px;
    }

    #next-slide{
        position: absolute;
        top: 50%;
        right: 0;
        cursor: pointer;
        margin-right: 30px;
    }

    .button-preview{
        cursor: pointer;
    }


</style>
@endpush
@section('content')

<div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{$va_count}}</h3>

              <p>My Virtual Assistants</p>
               @if (session()->has('notification_message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Success!</h4>
        {{ session()->get('notification_message') }}
    </div>
    @endif
            </div>
            <div class="icon">
              <i class="ion ion-person-stalker"></i>
            </div>
            <a href="/dashboard/va" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{$total_time}}</h3>

              <p>Total Hours Today</p>
            </div>
            <div class="icon">
              <i class="ion ion-clock"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3 id="sc_count">{{$screenshot_count}}</h3>

              <p>Screenshots Today</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-desktop"></i>
            </div>
            <a href="/dashboard/screenshots" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
</div>

<div class="row">
    <div class="col-md-8">

        <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Virtual Assistant Attendance <small class="text-primary" id="attendance_online"></small></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div style="height: 500px !important; overflow-x:auto;">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Time In</th>
                    <th>Status</th>
                  </tr>
                  </thead>
                  <tbody id="attendance_latest">
                    <span id="data-loading">loading please wait...</span>

                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="/dashboard/attendance" class="btn btn-sm btn-default btn-flat pull-right">View All</a>
            </div>
            <!-- /.box-footer -->
        </div>

        <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Virtual Assistant Task Logs</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Date</th>
                    <th>VA</th>
                    <th>Task Name</th>
                    <th>Time In</th>
                    <th>Time End</th>
                  </tr>
                  </thead>
                  <tbody id="vatasks">
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
        </div>
    </div>

    <div class="col-md-4">
        <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Screenshots</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box" id="all-sc">
                 <span id="data-loading">loading please wait...</span>
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <a href="/dashboard/screenshots" class="uppercase">View All Screenshots</a>
            </div>
            <!-- /.box-footer -->
          </div>
    </div>
</div>
@endsection


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="//momentjs.com/downloads/moment.min.js"></script>
<script src="{{ asset('js/notify.min.js') }}"></script>

<script>


$(document).ready(function() {
getManagerData();

window.setInterval(function(){
    getManagerData();
},150000);


        //this shows the preview modal
        $(document).on("click", ".button-preview", function(){
            var id = $(this).attr('data-id');
            $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
            $.ajax({
                url: "/dashboard/screenshots/preview/"+id,
                dataType: "json"
            }).done(function(data) {
                $('.modal-body .content').html(data.html);
                $('.modal-body .content').append('<div id="next-slide"><a href="#" class="prev-next" rel="next" id="get-next"><span style="font-size: 30px;" class="glyphicon">&#xe131;</span></a></div>');
                $('.modal-body .content').append('<div id="prev-slide"><a href="#" class="prev-next" rel="prev" id="get-prev"><span style="font-size: 30px;" class="glyphicon">&#xe132;</span></div>');
                //$('#modal-preview').modal('show');
            });
        });
    });


    $(document).on("click", ".prev-next", function(){

           var imgsrc = $('.modal-body .content').find('div .img-responsive').attr('src');
           //var imgsrc = dimg.replace('thumb_', '');
           var position = $(this).attr('rel');
            $('.modal-body .content').html("");
           //console.log(imgsrc);

            $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');

            var new_pos = 0;
            var new_target = '';
            var new_name = '';
            var new_date = '';
            var new_img = '';

            $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');

            var target_img = $("img[src$='"+imgsrc+"']");
            var pos_id = target_img.parent('div').attr('id');

            console.log(target_img.attr('src'));

            if(position == 'next'){
                if(new_pos <= 19){
                    new_pos = Number(pos_id) + 1;
                    new_target = $('#'+ new_pos).find('img');
                }

            }else{
                if(new_pos >= 0){
                    new_pos = Number(pos_id) - 1;
                    new_target = $('#'+ new_pos).find('img');
                }

            }

            new_name = $('#'+new_pos).next('div').find('.user').text();
            new_date = $('#'+new_pos).next('div').find('.timestamp').text();

            new_img = new_target.attr('src');
            var img_spl = new_img.split('/');

            $('.modal-body .content').html('<div><img width="838" height="475" class="img-responsive img-thumbnail" src="'+new_img+'"></div><br><strong>'+new_name+'</strong>&nbsp;&nbsp;<a href="/dashboard/screenshots/'+img_spl[2]+'/s/1"><small>view all screenshots</small></a><br><small>'+new_date+'</small></div>');

            if(new_pos < 19){
                 $('.modal-body .content').append('<div id="next-slide"><a href="#" class="prev-next" rel="next" id="get-next"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-right-circle-fill" viewBox="0 0 16 16"><path style="width: 30px !important;" d="M8 0a8 8 0 1 1 0 16A8 8 0 0 1 8 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z"/></svg></a></div>');
            }

            if(new_pos > 0){
                 $('.modal-body .content').append('<div id="prev-slide"><a href="#" class="prev-next" rel="prev" id="get-prev"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16"><path style="width: 30px !important;" d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"/></svg></div>');
            }

        });


    function getManagerData(){
         $.ajax({
                url: "/dashboard/get-manager-data",
                dataType: "json"
            }).done(function(data) {

                $('#data-loading').hide();

                var screenshots_latest = '';
                var attendance_latest = '';
                var vatask_latest = '';
                var bucket = '{{ env('AWS_BUCKET') }}';
                var thumb_url = '';

                if(data.screenshot_latest.length >= 1){
                    $('#all-sc').html('');
                    $.each(data.screenshot_latest, function(i, item) {
                      //console.log(item);
                      thumb_url =  item.filename;

                        screenshots_latest += '<li class="item"><div id="'+i+'"><img data-toggle="modal" data-target="#modal-preview"  class="button-preview" width="220px" data-id="'+ item.id +'" src="'+ thumb_url +'" alt="Screenshot Image"></div><div class="product-info"><strong><span class="user">'+ item.user.first_name + '  ' + item.user.last_name +'</span></strong><br><span class="timestamp"> '+ moment(item.created_at, "YYYY-MM-DD HH:mm:ss").format("MMM DD, YYYY h:mm A")  +'</span></div></li>';
                    });
                }

                if(data.attendance_latest.length >= 1){
                     $('#attendance_latest').html('');

                     $.each(data.attendance_latest, function(i, item) {
                         //moment(moment().format('YYYY-MM-DD h:mm:ss A'), v.user_tz);
                         var status = '';
                         if(item.status == 'in'){
                             status = '<span class="status"><span style="color: green">&#9679;</span> in</span>';
                        }else if(item.status == 'out'){
                            status = '<span class="status"><span style="color: red">&#9679;</span> out</span>';
                        }else if(item.status == 'break'){
                            status = '<span class="status"><span style="color: orange">&#9679;</span> break</span>';
                        }else if(item.status == 'idle'){
                            status = '<span class="status"><span style="color: gray">&#9679;</span> idle</span>';
                        }

                        attendance_latest += '<tr><td>'+ item.schedule_date +'</td><td>'+ item.user.first_name + ' ' + item.user.last_name  +'</td><td>' + moment(item.time_in, "HH:mm:ss").format("h:mm A") + '</td><td>'  + status +  '</td></tr>';
                    });
                }else{
                    $('#attendance_latest').html('<span>no latest attendance found</span>');
                }

                if(data.vatask_latest.length >= 1){
                    $('#vatasks').html('');
                    var date_start = '';
                    var date_end = '';

                     $.each(data.vatask_latest, function(i, item) {
                        date_start = moment(item.time_start, "hh:mm:ss").format("h:mm A");
                        date_end = moment(item.time_end, "hh:mm:ss").format("h:mm A");
                        vatask_latest += '<tr><td>'+ item.date_start +'</td><td>'+ item.user.first_name + ' ' + item.user.last_name +'</td><td>'+ item.name +'</td><td>'+ date_start +'</td><td>'+ date_end +'</td></tr>';
                    });
                }

                $('#all-sc').html(screenshots_latest);
                $('#attendance_latest').html(attendance_latest);
                $('#vatasks').html(vatask_latest);
                $('#attendance_online').html('  (' +data.attendance_count + ' online)');
                $('#sc_count').html(data.screenshot_count);
            });
    }
</script>
