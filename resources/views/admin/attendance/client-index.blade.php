@extends('layouts.vatheme-master')
@section('page-title', "Attendance")
@php
    $currentPage = 'attendance'
@endphp
@section('content')
<div class="content">
    <div class="container-fluid">
	<div class="row">
	    <div class="col-md-12">
		<div class="card">
		    <div class="card-header card-header-icon" data-background-color="purple">
			<i class="fa fa-user"></i>
		    </div>
		    <div class="card-content">
			<h4 class="card-title">Attendance</h4>


                    <div class='row'>
                        <div class='col-md-6'>
                            <label>Select Date</label>
                            <div class="input-group date" id="datepicker">
                            <input type="text" id='target_date' name='target_date' class="form-control input-lg" value="{{date('Y-m-d', strtotime(now()))}}">
                            <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                            </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            @if (\Auth::user()->is('client'))
                            <label>Type</label>
                            <select class="form-control input-lg" id="typepick" name="typepick">
                                <option value="present">Present</option>
                                <option value="absent">Absent</option>
                                <option value="break">On Break</option>
                            </select>
                            @endif

                            @if (\Auth::user()->is('administrator') || Auth::user()->is('super_administrator'))
                            <label>Type</label>
                            <select class="form-control input-lg" id="typepick" name="typepick">
                                <option value="present">Present</option>
                                <option value="absent">Absent</option>
                                <option value="break">On Break</option>
                                <option value="late">Late</option>
                                <option value="overbreak">Over Break</option>
                            </select>
                            @endif
                        </div>
                    </div>
                    <div class='row'><hr></div>

			<div class="table-responsive">
                <table class="table table-hover dataTable" id="attendance-table" role='grid' width='100%'>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Schedule</th>
                            <th>Date In</th>
                            <th>Time In</th>
                            <th>Remark</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div> <!-- end of .card-content -->
    </div>
</div>

</div>
</div>
</div>

@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
@endpush

@push('view-scripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

<script>
$(function() {
    fetchData();
});

$(function () {
    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    }).on('changeDate', function(e) {
        target_date = $("#target_date").val();
        type_pick = $('#typepick').find(":selected").val();
        fetchData(target_date, type_pick);
    });

    $("#typepick").change(function(){
        target_date = $("#target_date").val();
        type_pick = $('#typepick').find(":selected").val();
        fetchData(target_date, type_pick);
    });
    
});

function fetchData(target_date, type_pick) {
    $('#attendance-table').DataTable().destroy();

    $('#attendance-table').DataTable({
        processing: true,
        serverSide: false,
        ajax: { 
            url:'/dashboard/attendance/datatables',
            data: {
                target_date : target_date,
                type_pick: type_pick,
                type : 'GET'
            }
        },
        "columns": [
                { "data": "user_id" },
                { "data": "schedule"},
                { "data": "date_in"},
                { "data": "time_in"},
                { "data": "remark", width: "150px"},
        ]
    });

}
</script>
@endpush