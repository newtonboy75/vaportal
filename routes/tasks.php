<?php

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {	

        Route::get('/dashboard/tasks', [
            'middleware' => 'check.permission:list_tasks',
            'uses' => 'Admin\TaskController@index'
        ]);

        Route::get('/dashboard/tasks/datatables', [
            'middleware' => 'check.permission:list_tasks',
            'uses' => 'Admin\TaskController@datatables'
        ]);

        Route::get('/dashboard/tasks/show/{id}', [
            'middleware' => 'check.permission:list_tasks',
            'uses' => 'Admin\TaskController@show'
        ])->where('id', '[0-9]+'); 

        Route::get('/dashboard/tasks/create', [
            'middleware' => 'check.permission:add_tasks',
            'uses' => 'Admin\TaskController@create'
        ]);        

        Route::post('/dashboard/tasks/create', [
            'middleware' => 'check.permission:add_tasks',
            'uses' => 'Admin\TaskController@store'
        ]);  

        Route::post('/dashboard/tasks/update', [
            'middleware' => 'check.permission:edit_tasks',
            'uses' => 'Admin\TaskController@update'
        ]);

        Route::get('/dashboard/tasks/edit/{id}', [
            'middleware' => 'check.permission:edit_tasks',
            'uses' => 'Admin\TaskController@edit',
            'selected_nav_path' => 'dashboard/tasks/edit',
            'selected_parent_path' => 'dashboard/tasks'
        ])->where('id', '[0-9]+');         

        Route::get('/dashboard/tasks/delete/{id}', [
            'middleware' => 'check.permission:delete_tasks',
            'uses' => 'Admin\TaskController@delete'
        ])->where('id', '[0-9]+');         

});	