<?php

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {	

        Route::get('/dashboard/schedules', 'Admin\ScheduleController@index');
        Route::get('/dashboard/schedules/datatables', [
            'uses' => 'Admin\ScheduleController@datatables'
        ]); 

        Route::get('/dashboard/schedules/datatables_all', [
            'uses' => 'Admin\ScheduleController@datatablesAll'
        ]); 

        Route::get('/dashboard/schedules/today', [
            'uses' => 'Admin\ScheduleController@datatablesToday'
        ]);


        Route::get('/dashboard/va-schedules/{id}', 'Admin\ScheduleController@vaScheduleIndex');
        
        Route::get('/dashboard/va-schedules/{id}/datatables', [
            'uses' => 'Admin\ScheduleController@vaScheduleDataTables'
        ]);
        Route::get('/dashboard/va-schedules/{id}/create', [
            'uses' => 'Admin\ScheduleController@vaScheduleCreate'
        ]);
        Route::post('/dashboard/va-schedules/{id}/create', [
            'uses' => 'Admin\ScheduleController@vaScheduleStore'
        ]); 
        Route::get('/dashboard/va-schedules/show/{id}', [
            'uses' => 'Admin\ScheduleController@vaScheduleShow'
        ])->where('id', '[0-9]+');
        Route::get('/dashboard/va-schedules/delete/{id}', [
            'uses' => 'Admin\ScheduleController@vaScheduleDelete'
        ])->where('id', '[0-9]+'); 

        Route::get('/dashboard/va-schedules/edit/{id}', [
            'uses' => 'Admin\ScheduleController@vaScheduleEdit'
        ])->where('id', '[0-9]+');   

        Route::post('/dashboard/va-schedules/update', [
            'uses' => 'Admin\ScheduleController@vaScheduleUpdate'
        ]);   

});