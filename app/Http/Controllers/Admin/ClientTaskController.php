<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\ClientTask;
use App\Models\Schedule;
use App\Models\User;
use App\Models\Project;
use App\Models\Team;
use App\Models\TeamUser;
use App\Models\TaskNote;
use Illuminate\Validation\Rule;
use App\Models\ActivityLog;

class ClientTaskController extends Controller
{
    public function index()
    {
        $auth_user = \Auth::user();
        if ($auth_user->is('client')) {
            return va_view('admin.client-task.client-index');
        }
        return view('admin.client-task.index');
    }

    public function indexArchive()
    {
        $auth_user = \Auth::user();
        if ($auth_user->is('client')) {
            return va_view('admin.client-task.client-archive');
        }
        return view('admin.client-task.archive');
    }

    public function indexDeleted()
    {
        $auth_user = \Auth::user();
        if ($auth_user->is('client')) {
            return va_view('admin.client-task.client-deleted');
        }
        return view('admin.client-task.deleted');
    }

    public function create()
    {
        $auth_user = \Auth::user();


        $projects = Project::where('user_id', $auth_user->id)->get();

        if ($auth_user->is('client')) {

            //get all VAs under this client
            $vas = Schedule::where('client_id', $auth_user->id)
            ->pluck('user_id')->all();

            $va_users = User::HasRole('va')->select(['id','first_name', 'last_name'])
            ->whereIn('id', $vas)->get();

            if (count($va_users)==0) {
                $info_message = "You have not been assigned a VA yet. Please contact admin for more details.";
                return view('admin.info', compact('info_message'));
            }

            return va_view('admin.client-task.client-create_edit', compact('va_users', 'projects'));
        }

        if ($auth_user->is('manager')) {
            //get all VAs under this manager
            $team = Team::where("lead_user_id", $auth_user->id)->first();
            $vas = null;
            if ($team->id) {
                $vas = TeamUser::where("team_id", $team->id)->pluck('user_id')->all();
            }

            $va_users = User::HasRole('va')->select(['id','first_name', 'last_name'])
            ->whereIn('id', $vas)->get();

            return view('admin.client-task.create_edit', compact('va_users', 'projects'));

        }


        $va_users = User::HasRole('va')->select(['id','first_name', 'last_name'])
        ->get();

        return view('admin.client-task.create_edit', compact('va_users', 'projects'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:128',
            'description' => 'required|max:512',
            'priority' => 'required'
        ]);
        $auth_user = \Auth::user();

        $ctask = ClientTask::create([
            'client_id' => $auth_user->id,
            'project_id' => $request->project_id,
            'va_id' => $request->va_id,
            'name' => $request->name,
            'description' => $request->description,
            'priority' => $request->priority,
            'status' => 'todo',
            'date_due' => $request->date_due
        ]);

        $ctask->project_id = $request->project_id;
        $ctask->save();

        ActivityLog::addLog("User [{$auth_user->first_name} {$auth_user->last_name}] created a new task [{$request->name}].");

        return redirect('/dashboard/client-tasks/edit/'. $ctask->id)->with('notification_message', 'Task has been created.');
    } 

    public function edit($id)
    {

        $auth_user = \Auth::user();
        $projects = Project::where('user_id', $auth_user->id)->get();
        $q = ClientTask::where('client_id', $auth_user->id)->where('id', $id)->first();

        if (empty($q)) {
            return redirect('/dashboard');
        }

        if ($auth_user->is('client')) {
            //get all VAs under this client
            $vas = Schedule::where('client_id', $auth_user->id)
            ->pluck('user_id')->all();

            $va_users = User::HasRole('va')->select(['id','first_name', 'last_name'])
            ->whereIn('id', $vas)->get();

            return va_view('admin.client-task.client-create_edit', compact('q', 'va_users', 'projects'));
        }

        if ($auth_user->is('manager')) {
            //get all VAs under this manager
            $team = Team::where("lead_user_id", $auth_user->id)->first();
            $vas = null;
            if ($team->id) {
                $vas = TeamUser::where("team_id", $team->id)->pluck('user_id')->all();
            }

            $va_users = User::HasRole('va')->select(['id','first_name', 'last_name'])
            ->whereIn('id', $vas)->get();

            return view('admin.client-task.create_edit', compact('q', 'va_users', 'projects'));

        }

        $va_users = User::HasRole('va')->select(['id','first_name', 'last_name'])
        ->get();
        return view('admin.client-task.create_edit', compact('q', 'va_users', 'projects'));
    }

    public function update(Request $request) 
    {
        $auth_user = \Auth::user();
        $q = ClientTask::where('client_id', $auth_user->id)->where('id', $request->id)->first();

        $request->validate([
            'name' => 'required|max:128',
            'description' => 'required|max:512',
            'priority' => 'required'
        ]);

        if (!empty($q)) {
            $q->va_id = $request->va_id;
            $q->name = $request->name;
            $q->description = $request->description;
            $q->priority =  $request->priority;
            $q->project_id = $request->project_id;
            $q->date_due = $request->date_due;
            $q->save();
            ActivityLog::addLog("User [{$auth_user->first_name} {$auth_user->last_name}] updated the task [{$request->name}].");
        }

        return redirect()->intended('/dashboard/client-tasks/edit/'. $q->id)->with('notification_message', 'Task has been updated.'); 

    }

    public function viewTask($id)
    {
        $q = ClientTask::find($id);
        $notes = TaskNote::where('task_id', $id)->get();

        return va_view('admin.client-task.client-view', compact('q', 'notes'));
    }

    public function saveNotes(Request $request) 
    {
        
        $request->validate([
            'notes' => 'required|max:4096',
        ]);
        $auth_user = \Auth::user();

        $notes = TaskNote::create([
            'user_id' => $auth_user->id,
            'task_id' => $request->task_id,
            'notes' => $request->notes
        ]);

        return redirect('/dashboard/client-tasks/view/'. $request->task_id)->with('notification_message', 'Note has been created.');

    }

    //this function is being shared by all users to show client task
    public function show($id)
    {
        $q = ClientTask::find($id);
        $html = view('admin.client-task.show', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }

    //this function is being shared by all users to delete client tasks
    public function delete($id)
    {
        $auth_user = \Auth::user();

        if ($auth_user->is('client')) {

            $q = ClientTask::where('client_id', $auth_user->id)->where('id', $id)->first();

        } else {

            $q = ClientTask::where('id', $id)->first();

        }
        
        if (!empty($q)) {
            ActivityLog::addLog("User [{$auth_user->first_name} {$auth_user->last_name}] deleted the task [{$q->name}].");
            //$q->delete();
            $q->status = 'deleted';
            $q->save();
            $response['status'] = "ok";
            return json_encode($response);
        } else {
            $response['status'] = "error";
            return json_encode($response);
        }

    }

    public function deletePermanent($id) {
        $auth_user = \Auth::user();

        if ($auth_user->is('client')) {

            $q = ClientTask::where('client_id', $auth_user->id)->where('id', $id)->first();

        } else {

            $q = ClientTask::where('id', $id)->first();

        }
        
        if (!empty($q)) {
            ActivityLog::addLog("User [{$auth_user->first_name} {$auth_user->last_name}] permanently deleted the task [{$q->name}].");
            $q->delete();
            $response['status'] = "ok";
            return json_encode($response);
        } else {
            $response['status'] = "error";
            return json_encode($response);
        }
    }

    public function datatables()
    {
        $user = \Auth::user();
        $tasks = ClientTask::where('client_id', $user->id)
        ->where('status','<>','deleted')
        ->where('status','<>','done')
        ->select(['id','priority', 'va_id','name', 'description', 'status', 'project_id']);

        return DataTables::of($tasks)
            ->editColumn('project_id', function($task){
                if ($task->project_id == 0) {
                    return "No Project";
                } else {
                    return $task->project->name;
                }
            })
            ->editColumn('priority', function($task){
                if ($task->priority==1) return "High";
                if ($task->priority==2) return "Normal";
                if ($task->priority==3) return "Low";
            })
            ->editColumn('va_id', function($task){
                return $task->va->first_name . " " . $task->va->last_name;
            })
            ->addColumn('actions', function($task){
                $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class=" btn-sm btn btn-danger button-delete" data-id="'.$task->id.'"><i class="fa fa-trash"></i></a>';
                $edit_btn = '<a class="btn btn-info  btn-sm" href="/dashboard/client-tasks/edit/'.$task->id.'"><i class="fa fa-edit"></i></a>';
                $view_btn = '<a class="btn btn-primary btn-sm" href="/dashboard/client-tasks/view/'.$task->id.'"><i class="fa fa-eye"></i></a>';
                return '<div class="btn-toolbar">'. $view_btn . $edit_btn .  $delete_btn .'</div>';
            
        })->rawColumns(['actions'])
        ->make(true);

    }

    //archived means done status
    public function archivedDatatables()
    {
        $user = \Auth::user();
        $tasks = ClientTask::where('client_id', $user->id)
        ->where('status','done')
        ->select(['id','priority', 'va_id','name', 'description']);

        return DataTables::of($tasks)
            ->editColumn('priority', function($task){
                if ($task->priority==1) return "High";
                if ($task->priority==2) return "Normal";
                if ($task->priority==3) return "Low";
            })
            ->editColumn('va_id', function($task){
                return $task->va->first_name . " " . $task->va->last_name;
            })
            ->addColumn('actions', function($task){
                $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$task->id.'"><i class="fa fa-trash"></i></a>';
                return '<div class="btn-toolbar">'.  $delete_btn .'</div>';
            
        })->rawColumns(['actions'])
        ->make(true);
    }

    //deleted means deleted status
    public function deletedDatatables()
    {
        $user = \Auth::user();
        $tasks = ClientTask::where('client_id', $user->id)
        ->where('status','deleted')
        ->select(['id','priority', 'va_id','name', 'description']);

        return DataTables::of($tasks)
            ->editColumn('priority', function($task){
                if ($task->priority==1) return "High";
                if ($task->priority==2) return "Normal";
                if ($task->priority==3) return "Low";
            })
            ->editColumn('va_id', function($task){
                return $task->va->first_name . " " . $task->va->last_name;
            })
            ->addColumn('actions', function($task){
                $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$task->id.'"><i class="fa fa-trash"></i></a>';
                return '<div class="btn-toolbar">'.  $delete_btn .'</div>';
            
        })->rawColumns(['actions'])
        ->make(true);
    }
}