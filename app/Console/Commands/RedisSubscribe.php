<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use App\Models\User;
use App\Models\Attendance;
use App\Models\Idle;
use App\Http\Controllers\API\UserAttendanceController;
use DateTime;

class RedisSubscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:subscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe to a Redis channel';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Redis::psubscribe(['*'], function ($message, $channel) {
            $method = str_replace(' ',  '', ucwords(str_replace('-', ' ', $channel)));
            if(method_exists($this, $method)) {
                $this->$method(json_decode($message));
            } else {
                error_log("Unknown method invoked on pub/sub '" . $method . "'");
            }
        });
    }

    private function UserConnect($data) {
        $user = User::where('access_token', $data->access_token)->first();
        if($user) {
            $user->socket_id = $data->socket_id;
            $user->save();
        }
    }

    private function UserDisconnect($data) {
        $user = User::where('socket_id', $data->socket_id)->first();
        if($user) {
            $user->socket_id = null;
            $user->is_online = 0;
            $user->save();

            // Force idle
            //$user->startIdle();
            // Stop tasks
            $user->stopCurrentTask();
            $attendance = $user->attendanceList()->where('status', '!=', Attendance::STATUS_OUT)->first();
            if($attendance) {
                UserAttendanceController::doTimeout($attendance);

                $notif['content'] = $user->full_name . " has lost internet connection.";
                $notif['user_id'] = $user->id;
                $notif['type'] = "time-out";
                $notif['created_at'] = date("Y-m-d H:i:s");
                $notif['updated_at'] = date("Y-m-d H:i:s");
                \DB::table('notifications')->insert($notif);
            }
        }
    }

    private function UserAutoTimeout($data) {
        $user = User::where('socket_id', $data->socket_id)->first();
        if($user) {
            $user->socket_id = null;
            $user->is_online = 0;
            $user->save();

            $attendance = $user->attendanceList()->where('status', '!=', Attendance::STATUS_OUT)->first();
            if($attendance) {
                UserAttendanceController::doTimeout($attendance);

                $notif['content'] = $user->full_name . " has auto-timed out.";
                $notif['user_id'] = $user->id;
                $notif['type'] = "time-out";
                $notif['created_at'] = date("Y-m-d H:i:s");
                $notif['updated_at'] = date("Y-m-d H:i:s");
                \DB::table('notifications')->insert($notif);
            }
        }
    }
}