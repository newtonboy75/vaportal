<?php

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {	

        Route::get('/dashboard/settings', [
            'middleware' => 'check.permission:list_settings',
            'uses' => 'Admin\SettingsController@index'
        ]);

        Route::get('/dashboard/settings/datatables', [
            'middleware' => 'check.permission:list_settings',
            'uses' => 'Admin\SettingsController@datatables'
        ]);

        Route::get('/dashboard/settings/show/{id}', [
            'middleware' => 'check.permission:list_settings',
            'uses' => 'Admin\SettingsController@show'
        ])->where('id', '[0-9]+'); 

        Route::get('/dashboard/settings/create', [
            'middleware' => 'check.permission:add_settings',
            'uses' => 'Admin\SettingsController@create'
        ]);

        Route::post('/dashboard/settings/create', [
            'middleware' => 'check.permission:add_settings',
            'uses' => 'Admin\SettingsController@store'
        ]);  

        Route::post('/dashboard/settings/update', [
            'middleware' => 'check.permission:edit_settings',
            'uses' => 'Admin\SettingsController@update'
        ]);

        Route::post('/dashboard/settings/save', [
            'middleware' => 'check.permission:edit_settings',
            'uses' => 'Admin\SettingsController@save'
        ]);

        Route::get('/dashboard/settings/edit/{id}', [
            'middleware' => 'check.permission:edit_settings',
            'uses' => 'Admin\SettingsController@edit',
            'selected_nav_path' => 'dashboard/settings/edit',
            'selected_parent_path' => 'dashboard/settings'
        ])->where('id', '[0-9]+');

        Route::get('/dashboard/settings/delete/{id}', [
            'middleware' => 'check.permission:delete_settings',
            'uses' => 'Admin\SettingsController@delete'
        ])->where('id', '[0-9]+');

});