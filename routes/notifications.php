<?php

Route::group(array(
    'middleware' => ['web', 'admin.auth'],
    ), function() {	

        Route::get('/dashboard/notifications', [
            'uses' => 'Admin\NotificationController@index'
        ]);

        Route::get('/dashboard/notifications/datatables', [
            'uses' => 'Admin\NotificationController@datatables'
        ]);

        Route::get('/dashboard/notifications/check_notif', [
            'uses' => 'Admin\NotificationController@checkNotif'
        ]);
});	