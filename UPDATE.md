Set website to maintenance mode
Make database backup

Add to .env file to AWS section 
for production:
`AWS_UPLOAD_DIR=56bd47ff1ec5364c78abbc5caa4eaf557d309c5c1e013c0017cd01deb80ede04`
for QA:
`AWS_UPLOAD_DIR=1fea2c61721947cd6f48905eef54ed8f0db99552`

Go to AWS s3 and move screenshots under new dir

run
```
php artisan config:cache
php artisan migrate
```

then run
```
composer dumpautoload
php artisan db:seed --class="UserAvatarsSeeder"
php artisan db:seed --class="ScreenshotsSeeder"
```