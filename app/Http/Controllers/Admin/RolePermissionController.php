<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RolePermission;
use Illuminate\Validation\Rule;

class RolePermissionController extends Controller
{
    public function index($id)
    {
        $role =  Role::where("id", $id)->first();
        $permissions = Permission::all();

        $set_menu = [
            'active_menu' => "dashboard/roles",
            'active_path' => "dashboard/role-permissions/{id}",
            'active_parent' => "Tools"
        ];
        return view('admin.role-permission.index', compact('role', 'permissions', 'set_menu'));
    }

    public function updatePermissions(Request $request)
    {

        $permissions = $request->input('permissions');

        $role =  Role::where("id", $request->role_id)->first();

        RolePermission::where('role_id', $request->role_id)->delete();
        
        if (!empty($permissions)) {

            foreach($permissions as $key => $value)
            {
                $role->addPermission($key);

            } 

        }

        return redirect()->intended('/dashboard/role-permissions/'. $request->role_id)->with('notification_message', "Role's permissions have been updated."); 
        
    }
}