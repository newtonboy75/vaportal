@extends('layouts.timedly.app')

@section('pageTitle')
    Profile
@endsection

@section('content')
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Profile of {{$user->fullname}}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                            <div class="box box-info">
                                    <div class="box-body">

                                        
<div class="panel-body">

        <div class="col-md-12">
        
            <div class="row">

                <div class="box box-info- mx-auto" style="width: 100% !important; margin-left: auto; margin-right: auto;">
                    <div class="box-body">
                    <a href="/dashboard/va" class="float-right"><small><i class="fas fa-arrow-left"></i> Back</small></a>
                        <div class="row">
                            <div class="col-sm-2 p-5">
                                <div class="form-group text-center">
                                    <img id='user-avatar-profile' src="{{$avatar}}" class="img-circle img-responsive center-block" alt="User Image" width="200" height="200"><br>
                                    <p style="font-size: 18px;"><strong>{{$user->fullname}} 
                                    @if(isset($user->name_suffix))
                                        @if($user->name_suffix != 'N/A')
                                            $user->name_suffix 
                                        @endif
                                     @endif
                                    </strong></p>
                                    <p>{{$user->timezone}}</p>
                                    <p>&nbsp;</p>
                                    <p><a style="width: 90%;" class="btn btn-primary btn-sm" href="{{$user->facebook ?? '#'}}">Facebook Profile</a></p>
                                    <p><a style="background-color: #3ec4ff; color: #fff; width: 90%;" class="btn btn-secondary btn-sm" href="skype:{{$user->skype_id}}?chat">Skype</a></p>
                                </div>
                            </div>
                            
                            <div class="col-sm-10">
                                <div style="padding-left: 30px; border-left: 1px solid #f0f0f0;">
                                <p>
                                <small><strong>EMAIL</strong></small><br>
                                    {{$user->email}}
                                </p>
                                <p>   
                                    <small><strong>PERSONAL EMAIL</strong></small><br>
                                    {{$user->personal_email ?? 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>ADDRESS</strong></small><br>
                                    {{$user->address1 ?? 'N/A'}}
                                </p>
                                
                                
                                <p>   
                                    <small><strong>LOCATION</strong></small><br>
                                    {{$user->city ? $user->city.' Philippines' : 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>ZIPCODE</strong></small><br>
                                    {{$user->zip ?? 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>MOBILE #</strong></small><br>
                                    {{$user->mobile_number ?? 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>ALTERNATE CONTACT #</strong></small><br>
                                    {{$user->alternate_contact_number ?? 'N/A'}}
                                </p>
                                
                                <hr>
                                <p>   
                                    <small><strong>BIRTHDAY</strong></small><br>
                                    {{$user->birthday ?? 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>GENDER</strong></small><br>
                                    {{ucwords($user->gender) ?? 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>CIVIL STATUS</strong></small><br>
                                    {{ ucwords($user->civil_status) ?? 'N/A'}}
                                </p>
                                
                                <hr>
                                

                                
                                
                                
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>

            </div>

                                    </div>
                                </div>
                    </div>
                </div>
            </div>


        </div>
@endsection
