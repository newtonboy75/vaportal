<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
use App\Models\Project;
use Illuminate\Validation\Rule;
use App\Models\ActivityLog;
use App\Models\ClientTask;
use App\Models\ManagerClient;

class ProjectsController extends Controller
{
    public function index()
    {
        $auth_user = \Auth::user();

         if ($auth_user->is('client') ) {
            return va_view('admin.projects.client-index');
         } 
         return view('admin.projects.index');

    }

    public function create()
    {
        $auth_user = \Auth::user();

         if ($auth_user->is('client') ) {
            return va_view('admin.projects.client-create_edit');
         } 

        return view('admin.projects.create_edit');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'max:1024',
        ]);

        $auth_user = \Auth::user();

        $project = Project::create([
            'user_id' => $auth_user->id,
            'name' => $request->name,
            'description' => $request->description
        ]);

        return redirect('/dashboard/projects/edit/'.$project->id)->with('notification_message', 'Project has been created.'); 
    }    

    public function show($id)
    {
        $q = Project::find($id);
        $html = view('admin.projects.show', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }    

    public function edit($id)
    {
        $q = Project::find($id);
        if (empty($q)) {
            return redirect('/dashboard');            
        }

        $auth_user = \Auth::user();

        if ($auth_user->is('client') ) {
           return va_view('admin.projects.client-create_edit', compact('q'));
        } 
        return view('admin.projects.create_edit', compact('q'));
    }

    public function update(Request $request)
    {
        $q = Project::find($request->id);

        $request->validate([
            'name' => 'required|max:255',
            'description' => 'max:1024',
        ]);

        $auth_user = \Auth::user();
        
        if (!empty($q)) {

            $q->name = $request->name;
            $q->description = $request->description;
            $q->save();

            return redirect('/dashboard/projects/edit/'.$q->id)->with('notification_message', 'Project has been updated.'); 
        } else {
            return redirect('/dashboard');            
        }

    } 

    public function delete($id)
    {
        $q = Project::find($id);
        if (!empty($q)) {
            //delete the project
            $q->delete();
            //update project_id to 0
            DB::table('client_tasks')->where('project_id',$id)
            ->update([ 'project_id' => 0]);
            $response['status'] = "ok";
        } else {
            $response['status'] = "error";

        }
        return json_encode($response);

    }

    public function datatables()
    {
        $auth_user = \Auth::user();

        //if ($auth_user->is('client') || $auth_user->is('managers')) {
        if ($auth_user->is('client')) {
            $projects = Project::where('user_id', $auth_user->id)
            ->select(['id','name', 'description']);
            
        }elseif($auth_user->is('manager')) {
            $clients = ManagerClient::where('user_id', $auth_user->id)->pluck('client_id');
            $projects = Project::whereIn('user_id', $clients)
            ->select(['id','name', 'description']);

        } else {
            $projects = Project::select(['id','name', 'description']);
        }


        return DataTables::of($projects)
        ->editColumn('name', function($project){
            $total_count = ClientTask::where('project_id', $project->id)->count();
            $done_count = ClientTask::where('project_id', $project->id)->where('status','done')->count();

            $percent = 0;
            if ($total_count!=0) {
               $percent = ($done_count / $total_count) * 100;
            }
            return '<div class="project-name-list">'.$project->name.'</div><div class="progress-group" style="width:90%">
            <span class="progress-number"><b>'.$done_count.'</b>/'.$total_count.'</span>
            <div class="progress">
            <div class="progress-bar progress-bar-aqua" style="width: '.$percent.'%"></div>
            </div>
            </div>';
        })
        ->addColumn('actions', function($project){

            $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-sm btn-danger button-delete" data-id="'.$project->id.'"><i class="fa fa-trash"></i></a>';
            $edit_btn = '<a class="btn btn-info btn-sm" href="/dashboard/projects/edit/'.$project->id.'"><i class="fa fa-edit"></i></a>';    

            $show_btn = '<a class="btn btn-primary btn-sm" href="/dashboard/taskboard/'.$project->id.'"><i class="fa fa-eye"></i></a>';    

            return '<div class="btn-toolbar">'. $edit_btn . $show_btn . $delete_btn .'</div>';
        })->rawColumns(['actions','name'])
        ->make(true);        

    }
}