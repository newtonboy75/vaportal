<?php

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {	

        Route::get('/dashboard/clients', [
            'middleware' => 'check.permission:list_clients',
            'uses' => 'Admin\ClientController@index'
        ]);

        Route::get('/dashboard/clients/datatables', [
            'middleware' => 'check.permission:list_clients',
            'uses' => 'Admin\ClientController@datatables'
        ]);

        Route::get('/dashboard/clients/show/{id}', [
            'middleware' => 'check.permission:list_clients',
            'uses' => 'Admin\ClientController@show'
        ])->where('id', '[0-9]+'); 

        Route::get('/dashboard/clients/create', [
            'middleware' => 'check.permission:add_clients',
            'uses' => 'Admin\ClientController@create'
        ]);        

        Route::post('/dashboard/clients/create', [
            'middleware' => 'check.permission:add_clients',
            'uses' => 'Admin\ClientController@store'
        ]);  

        Route::post('/dashboard/clients/update', [
            'middleware' => 'check.permission:edit_clients',
            'uses' => 'Admin\ClientController@update'
        ]);

        Route::get('/dashboard/clients/edit/{id}', [
            'middleware' => 'check.permission:edit_clients',
            'uses' => 'Admin\ClientController@edit',
            'selected_nav_path' => 'dashboard/clients/edit',
            'selected_parent_path' => 'dashboard/clients'
        ])->where('id', '[0-9]+');         

        Route::get('/dashboard/clients/delete/{id}', [
            'middleware' => 'check.permission:delete_clients',
            'uses' => 'Admin\ClientController@delete'
        ])->where('id', '[0-9]+');         

        Route::get('/dashboard/client-va-rates/{id}', [
        //        'middleware' => 'check.permission:edit_clients',
                'uses' => 'Admin\ClientController@client_va_rate',
                'selected_nav_path' => 'dashboard/clients/edit',
                'selected_parent_path' => 'dashboard/clients'
        ]);

        Route::get('/dashboard/client-va-rates/{id}/available-vas', [
            //'middleware' => 'check.permission:edit_clients',
            'uses' => 'Admin\ClientController@client_va_rate_available_va'
        ]);

        Route::get('/dashboard/client-va-rates/{id}/datatables', [
        //        'middleware' => 'check.permission:edit_clients',
                'uses' => 'Admin\ClientController@client_va_rate_datatables'
        ]);

        Route::get('/dashboard/client-va-rate-update/{id}', [
        //    'middleware' => 'check.permission:edit_clients',
            'uses' => 'Admin\ClientController@client_va_rate_update'
        ]);

        Route::get('/dashboard/client-va-rate-add/{id}', [
        //        'middleware' => 'check.permission:edit_clients',
                'uses' => 'Admin\ClientController@client_va_rate_add'
        ]);

});	