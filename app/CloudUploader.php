<?php

namespace App;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class CloudUploader
{
    protected $image;
    protected $handler;
    protected $safedir;
    protected $uid;
    protected $user;
    protected $screenshotsDir;
    protected $avatarsDir;
    
    function __construct($user)
    {
        $this->handler = Storage::disk('s3');
        $this->safedir = rtrim(config('filesystems.disks.s3.upload_dir'), '/');
        $this->uid = $user->id;

        $uploadDir = storage_path("app/uploads");

        $this->screenshotsDir = $uploadDir . '/screenshots';
        if (!file_exists($this->screenshotsDir)) {
            File::makeDirectory($this->screenshotsDir, 0755, true, true);
        }
        $this->avatarsDir = $uploadDir . '/avatars';
    }

    private function _makeScreenshotUploadDir()
    {
        $path = $this->screenshotsDir . '/' . $this->uid;
        if (!file_exists($path)) {
            File::makeDirectory($path, 0755, true, true);
        }
        return $path;
    }

    public function getScreenshotUploadDir()
    {
        return $this->_makeScreenshotUploadDir();
    }

    public function generateScreenshotName()
    {
        return time() . rand();
    }

    private function _makeAvatarUploadDir()
    {
        if (!file_exists($this->avatarsDir)) {
            File::makeDirectory($this->avatarsDir, 0755, true, true);
        }
        return $this->avatarsDir;
    }

    public function getAvatarUploadDir(){
        return $this->_makeAvatarUploadDir();
    }

    public function getStorage()
    {
        return 's3';
    }

    public function makeThumbNail($filepath, $thumnail_key)
    {
        $workingDir = $this->getScreenshotUploadDir();
        $filePath2 = $workingDir . "/" . $thumnail_key;
        $thumb = Image::make($filepath);

        $thumb->fit(290, 290)->save($filePath2);
        $thumb->destroy();
        return $filePath2;
    }

    public function uploadScreenshot($filepath, $screenshot)
    {
        $path_parts = pathinfo($filepath);

        $screenshot_key = $path_parts['filename'] . '.' . $path_parts['extension'];

        $thumnail_key = 'thumb_' .  $screenshot_key;

        $filePath2 = $this->makeThumbNail($filepath, $thumnail_key);

        $cloudPath = 'screenshots/' . $this->uid . '/' . $screenshot_key;
        \App\Jobs\CloudUploadScreenshot::dispatch([
            'type' => 'screenshot',
            'cloudPath' => $cloudPath,
            'user' => $this->user,
            'filePath' => $filepath,
            'screenshot' => $screenshot,
            'safeDir' => $this->safedir
        ]);

        $cloudPath2 = 'screenshots/' . $this->uid . '/' . $thumnail_key;
        \App\Jobs\CloudUploadScreenshot::dispatch([
            'type' => 'thumb',
            'cloudPath' => $cloudPath2,
            'user' => $this->user,
            'filePath' => $filePath2,
            'screenshot' => $screenshot,
            'safeDir' => $this->safedir
        ]);

        //File::deleteDirectory($workingDir);

        return true;
    }

    public function uploadAvatar($filepath, $ext)
    {
        $path_parts = pathinfo($filepath);

        $fullFileName = $path_parts['filename'] . '.' . $path_parts['extension'];

        $cloudPath = 'avatars/' . $fullFileName;

        $res = $this->handler->put($this->safedir . '/' . $cloudPath, file_get_contents($filepath));

        //if (!$res) {
        //    throw new \Exception('Could not upload');
        //}
        File::delete($filepath);
        return $this->handler->url($this->safedir . '/' . $cloudPath);
    }
}

interface ImageInterface
{
    public function resize($w, $h);
    public function fit($w, $h);
    public function save($path);
}
