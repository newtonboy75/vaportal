<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class ScreenshotsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $baseUrl = rtrim(Storage::disk('s3')->url('/'), '/');
        $cloudUrl = $baseUrl . '/' . rtrim(config('filesystems.disks.s3.upload_dir'), '/') . '/screenshots';

        \App\Models\Screenshot::whereNull('thumbnail_url')->where('created_at', '>', '2021-06-01')->chunk(200, function ($screenshots) use ($cloudUrl) {
            foreach ($screenshots as $screenshot) {
                if (empty($screenshot->filename)) continue;
                if (substr($screenshot->filename, 0, 5) == 'https') continue;
                echo "processing $screenshot->id " . PHP_EOL;
                $finalCloudPath = $cloudUrl . '/' . $screenshot->user_id . '/' . $screenshot->filename;
                $finalCloudThumbnailPath = $cloudUrl . '/' . $screenshot->user_id . '/thumb_' . $screenshot->filename;
                $finalCloudThumbnailPath = str_replace('//thumb_', '/thumb_', $finalCloudThumbnailPath);
                $screenshot->filename = $finalCloudPath;
                $screenshot->thumbnail_url = $finalCloudThumbnailPath;
                $screenshot->path = '';
                $screenshot->storage = 's3';
                $screenshot->save();
            }
        });
    }
}

/*
fixing issue with double slash

\App\Models\Screenshot::whereNotNull('thumbnail_url')->where('created_at', '>', '2021-09-01')->where('thumbnail_url' ,'like', '%//thumb_%')->chunk(500, function ($screenshots) {
    foreach ($screenshots as $screenshot) {
        $s = str_replace('//thumb_', '/thumb_', $screenshot->thumbnail_url);
        //echo $s . PHP_EOL;
        $screenshot->thumbnail_url = $s;
        $screenshot->save();
    }
});

*/
