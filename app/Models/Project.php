<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

    protected $table = "projects";
    public $timestamps = false;

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'name', 'description', 'created_at'];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

    public function getPercentDone() 
    {
        $total_count = ClientTask::where('project_id', $this->id)->count();
        $done_count = ClientTask::where('project_id', $this->id)->where('status','done')->count();

        $percent = 0;
        if ($total_count!=0) {
        $percent = ($done_count / $total_count) * 100;
        }

        return $percent;
    }
}