@extends('layouts.timedly.app')

@php
$requri = explode('/', $_SERVER['REQUEST_URI']);

$start_time = isset($user_attendance->start_time) ? $user_attendance->start_time : '12:00AM';
$end_time = isset($user_attendance->end_time) ? $user_attendance->end_time : '11:59PM';

@endphp

@section('pageTitle')
    -Screenshots of {{$user->first_name}} {{$user->last_name}}
@endsection
@push('view-styles')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
@endpush

@push('view-styles')
#prev-slide{
        position: absolute;
        top: 30% !important;
        left: 0;
        cursor: pointer;
        margin-left:30px;
        margin-top: -40px !important;
 }

#next-slide{
        position: absolute;
        top: 30% !important;
        right: 0;
        cursor: pointer;
        margin-right: 30px;
        margin-top: -40px !important;
}
@endpush

@section('pageScripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="//momentjs.com/downloads/moment-timezone-with-data.js"></script>
<script>
var _table;

function fetchData(target_date, start_time, end_time) {
    $('#va-screenshots-table').DataTable().destroy();

    _table = $('#va-screenshots-table').DataTable({
        keys: !0,
        language: {
            paginate: {
                previous: "<i class='fas fa-angle-left'>",
                next: "<i class='fas fa-angle-right'>"
            }
        },
        processing: true,
        serverSide: true,
        searching: false,
        ajax: {
            url:'/dashboard/screenshots/{{$user->id}}/datatables',
            data: {
                target_date : target_date,
                start_time: start_time,
                end_time: end_time,
                type : 'GET'
            }
        },
        "columns": [
                { "data": "path" },
                { "data": "time_taken" },
                { "data": "mouse_activity" },
                { "data": "keyboard_activity" },
                { "data": "created_at", visible: false },
                { "data": "actions", width: "150px", orderable: false, searchable: false},
        ],
        "order": [[ 4, "desc" ]],
        'drawCallback': function(settings) {
            if ($('.va-screenshots-table tr').length==2 && $('.va-screenshots-table tr').find('td').hasClass('dataTables_empty')) {
                $('.va-screenshots-table tr').css("height", "auto");
                $('.va-screenshots-table tr').css("width", "100%");
                $('.va-screenshots-table tr').css("margin", "0");
            } else {
                $('.va-screenshots-table tr').css("height", "220px");
                $('.va-screenshots-table tr').css("width", "220px");
                $('.va-screenshots-table tr').css("margin", "5px");
            }
            $('.va-screenshots-table thead tr').css("height", "auto");
        }
    });

}


$(function() {

  var client_tz = '{{ $tz }}';
  var manila    = moment.tz("Asia/Manila");
  var client_side = manila.clone().tz(client_tz);
  var startime_load = '{{$start_time}}';
  var endtime_load = '{{$end_time}}';

    fetchData($('#target_date').val(), startime_load, endtime_load);

    $('#datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    }).on('changeDate', function(e) {
        start_time = $("#start_time").val();
        end_time = $("#end_time").val();
        target_date = $("#target_date").val();
        fetchData(target_date, start_time, end_time);
    });

    $('.clockpicker').clockpicker({
        placement: 'bottom',
        align: 'left',
        donetext: 'Set',
        twelvehour:true,
        afterDone: function(){
          target_date = $("#target_date").val();
          start_time = $("#start_time").val();
          end_time = $("#end_time").val();

          fetchData(target_date, start_time, end_time);
        }
    });



    $("#mouse-sort").click(function(){
        _sort = $(this).data('sort');
        _table
            .order( [ 2, _sort ] )
            .draw();
        if (_sort=='asc') {
            $(this).data('sort', 'desc');
        } else {
            $(this).data('sort', 'asc');
        }
    });

    $("#keyboard-sort").click(function(){
        _sort = $(this).data('sort');
        _table
            .order( [ 3, _sort ] )
            .draw();
        if (_sort=='asc') {
            $(this).data('sort', 'desc');
        } else {
            $(this).data('sort', 'asc');
        }
    });
});

var temp_delete_id = 0;

//this shows the preview modal
$(document).on("click", ".button-preview", function(){
    var id = $(this).attr('data-id');
    $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
    $.ajax({
        url: "/dashboard/screenshots/preview/"+id,
        dataType: "json"
    }).done(function(data) {
        $('.modal-body .content').html(data.html);
        $('.modal-body .content').append('<div id="next-slide"><a href="#" class="prev-next" rel="next" id="get-next"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-right-circle-fill" viewBox="0 0 16 16"><path style="width: 30px !important;" d="M8 0a8 8 0 1 1 0 16A8 8 0 0 1 8 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z"></path></svg></a></div>');
        $('.modal-body .content').append('<div id="prev-slide"><a href="#" class="prev-next" rel="prev" id="get-prev"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16"><path style="width: 30px !important;" d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"></path></svg></div>');
        $('#modal-preview').modal('show');
        $('#view-ss').hide();
    });
});

//this shows the modal
$(document).on("click", ".button-delete", function(){
    var id = $(this).attr('data-id');
    temp_delete_id = id;
    $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');
    $.ajax({
        url: "/dashboard/screenshots/show/"+id,
        dataType: "json"
    }).done(function(data) {
        $('.modal-body .content').html(data.html);
        //$('.modal-body .content').append('<div id="next-slide"><a href="#" class="prev-next" rel="next" id="get-next"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-right-circle-fill" viewBox="0 0 16 16"><path style="width: 30px !important;" d="M8 0a8 8 0 1 1 0 16A8 8 0 0 1 8 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z"></path></svg></a></div>');
        //$('.modal-body .content').append('<div id="prev-slide"><a href="#" class="prev-next" rel="prev" id="get-prev"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16"><path style="width: 30px !important;" d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"></path></svg></div>');
        $('#modal-warning').modal('show');
    });
});

//this does the actual delete
$("#button-delete").click(function(){
    $(this).html('<i class="fa fa-spin fa-refresh"></i> Delete');
    $.ajax({
        url: "/dashboard/screenshots/delete/"+temp_delete_id,
        dataType: "json"
    }).done(function(data) {
        $('#modal-warning').modal('hide');
        $("#button-delete").html('Delete');
        _table.ajax.reload( null, false );
    });
});

$(document).on("click", ".prev-next", function(){

           var imgsrc = $('.modal-body .content').find('div .img-responsive').attr('src').split('/');
           var position = $(this).attr('rel');
            $('.modal-body .content').html("");
            var new_target = '';
            var new_name = '';
            var new_date = '';
            //alert(imgsrc[3]);
            //console.log(imgsrc[3]);
            $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');

            //$('#sortable').find('img[src$="/test1.jpg"]')
            var target_img = $("img[src$='"+imgsrc[3]+"']");
            if(position == 'next'){
                new_target = target_img.closest('tr').next().find('img');
            }else{
                new_target = target_img.closest('tr').prev().find('img');
            }

            new_name = new_target.parent('td').next('td');
            new_date = new_name.next('td');

            var new_img = new_target.attr('src').replace('thumb_', '');
            $('.modal-body .content').html('<div><img width="838" height="475" class="img-responsive img-thumbnail" src="'+new_img+'"></div><br><strong>'+new_name.text()+'</strong><br><small>'+new_date.text()+'</small></div>');



            //$.get( "/dashboard/getSliders", { id: imgsrc[3], position: position })
            //.done(function( data ) {
                //console.log(data);
               // $('.modal-body .content').html(data.html);
                $('.modal-body .content').append('<div id="next-slide"><a href="#" class="prev-next" rel="next" id="get-next"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-right-circle-fill" viewBox="0 0 16 16"><path style="width: 30px !important;" d="M8 0a8 8 0 1 1 0 16A8 8 0 0 1 8 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z"></path></svg></a></div>');
                $('.modal-body .content').append('<div id="prev-slide"><a href="#" class="prev-next" rel="prev" id="get-prev"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16"><path style="width: 30px !important;" d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"></path></svg></div>');
           // });

        });

</script>
@endsection

@section('content')
@include('includes.timedly.preview-modal')

    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7" style="display: none;">
                        <h6 class="h2 text-white d-inline-block mb-0">Screenshots of {{$user->first_name}} {{$user->last_name}}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Select Date</label>
                                <div class="input-group date" id="datepicker">
                                    <input type="text" id='target_date' name='target_date' class="form-control input-lg" value="{{ last($requri) }}">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Sort by Activity</label>
                                <div class="btn-toolbar">
                                    <button style='height:54px; width:120px; margin-bottom: 10px;' class='btn btn-info btn-sort' id='mouse-sort' data-sort='asc'>Sort by <img src='/images/custom/mouse.png' style='width:32px'/></button>
                                    <button style='height:54px; width:120px'class='btn btn-info btn-sort' id='keyboard-sort' data-sort='asc'>Sort by <img src='/images/custom/keyboard.png' style='width:32px'/></button>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Start Time</label>




                                <div class="input-group clockpicker">
                                  <!--
                                  <input id="start_date_time" type="datetime-local" name="start_date_time" value="2021-09-30T00:00">

                                  -->

                                  <input id="start_time" name="start_time" type="text" class="form-control input-lg" value="{{date('h:iA', strtotime($start_time))}}">

                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>End Time</label>
                                <div class="input-group clockpicker">
                                    <input id="end_time" name="end_time" type="text" class="form-control input-lg" value="{{date('h:iA', strtotime($end_time))}}">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="table-responsive">
                                <table class="va-screenshots-table table table-hover dataTable" id="va-screenshots-table" role='grid' width='100%'>
                                    <thead>
                                        <tr>
                                            <th>Thumb</th>
                                            <th>Time Taken</th>
                                            <th>Mouse Activity</th>
                                            <th>Keyboard Activity</th>
                                            <th>Created</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
@endsection
