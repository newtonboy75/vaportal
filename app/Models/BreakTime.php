<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BreakTime extends Model {

    protected $table = "breaks";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'user_id',
        'attendance_id', 
        'break_start',
        'break_stop'
    ];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }
}