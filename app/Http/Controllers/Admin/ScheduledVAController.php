<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
use DateTime;

class ScheduledVAController extends Controller
{
    public function index()
    {
        return view('admin.scheduled-va.index');
    }

}