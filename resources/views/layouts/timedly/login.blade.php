{{-- Head --}}
@include('includes.timedly.header')

{{-- Main Content --}}
<body class="bg-default">
    <div class="main-content" id="panel">

        <div class="header bg-gradient-orange py-7 py-lg-8">
            <div class="container">
                <div class="header-body text-center">
                    <div class="row justify-content-center">
                        <!--
                        <div class="col-lg-5 col-md-6">
                            <a href="#" class="active" id="login-form-link">
                                <img src="{{ asset('images/timedly/brand/blue.png') }}" class="img-responsive">
                            </a>
                        </div>
                        -->
                    </div>
                </div>
            </div>

        </div>

        @yield('content')

    </div>
    <script src="{{asset('js/timedly.js')}}"></script>
 </body>    
</html>