<?php
//TODO: set permissions
Route::group(array(
    'middleware' => ['web', 'admin.auth'],
    ), function() {	

        Route::get('/dashboard/summary-reports', [
            'uses' => 'Admin\SummaryReportsController@index'
        ]);

        Route::get('/dashboard/summary-reports/generate', [
            'uses' => 'Admin\SummaryReportsController@generateSummaryReports'
        ]);

        Route::get('/dashboard/summary-reports/generate/pdf', [
            'uses' => 'Admin\SummaryReportsController@generatePDF'
        ]);
});