@extends('layouts.timedly.login')

@section('pageTitle')
    VA Time Tracker | Activate Account
@endsection

@section('pageScripts')
<script>
    var updated = false;
    $(document).ready(function () {
        @php
            if(session()->has('error_msg')):
                $message = session()->get('error_msg');
                $type = 'error';
                $title = 'Something went wrong.';
        @endphp

        swal({
            title: '{{$title}}',
            html: '{!!$message!!}',
            type: '{{$type}}',
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary'
        });

        @php
            endif;
        @endphp
    });
</script>
@endsection

@section('content')
    <!-- Page content -->
    <div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-7">
                <div class="card bg-secondary shadow border-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        @if( isset($status) && $status == 'is-success' )
                        <div class="text-center text-muted mb-4">
                            <p>
                                {{ $result_message }}
                            </p>
                            <small><a href="{{ url('auth/login') }}" class="btn btn-success">Go to Login</a></small>
                        </div>
                        @else
                            <form method="GET" action="{{url('/auth/verify')}}">
                                {{ csrf_field() }}
                                <div class="form-group mb-3">
                                    <input type="text" name="verify_token" id="verify_token" tabindex="1" class="form-control" placeholder="Activation Code" value="{{old('verify_token')}}" required="required">
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary my-4">Activate</button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
