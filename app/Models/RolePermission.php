<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model {

    protected $table = "permission_role";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['id', 'permission_id', 'role_id'];

    public function permission()
    {
        return $this->belongsTo(Permission::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }    
}