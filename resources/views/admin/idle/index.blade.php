@extends('layouts.adminlte-master')
@section('page-title', "Current Idle")

@section('content')

<section class="content">

    <div class="row">

        <!-- Current Idle -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Idle</h3> 
                     <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                </div>
                <div class="box-body">
                    <div>
                        <table class="table table-bordered table-hover" role='grid' width='100%'>
                            <thead>
                                <tr>
                                    <th width="40%">Name</th>
                                    <th width="40%">Idle Start</th>
                                    <th width="20%">Idle Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($vas_idle))
                                    @foreach ($vas_idle as $va)
                                        <tr>
                                            <td>{{$va->user->full_name}}</td>
                                            <td>{{ $va->idle_start_formatted }}</td>
                                            <td>{{ $va->idle_time }}</td>
                                        </tr>
                                    @endforeach
                                @else 
                                    <tr>
                                        <td colspan="5">No items to display</td>
                                    </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div><!-- box-body -->
            </div>
        </div>

    </div>

</section>

@endsection

@push('view-scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@endpush