<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class CloudUploadScreenshot implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $cloudPath, $filePath, $safeDir, $screenshot, $type, $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->cloudPath = $data['cloudPath'];
        $this->filePath = $data['filePath'];
        $this->safeDir = $data['safeDir'];
        $this->screenshot = $data['screenshot'];
        $this->type = $data['type'];
        $this->user = $data['user'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!file_exists($this->filePath)) {
            $this->fail();
            return -1;
        }
        $handler = Storage::disk('s3');
        $res = $handler->put($this->safeDir . '/' . $this->cloudPath, file_get_contents($this->filePath));

        if (!$res) {
        //    throw new \Exception('Could not upload');
            $this->fail();
            return -1;
        }
        $url = $handler->url($this->safeDir . '/' . $this->cloudPath);

        if($this->type == 'screenshot'){
            $this->screenshot->filename = $url;
            $this->screenshot->save();
        } else {
            $this->screenshot->thumbnail_url = $url;
            $this->screenshot->save();
        }
        unlink($this->filePath);
        return 0;
    }
}
