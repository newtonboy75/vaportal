<span class="text-warning">***Approves/declines all in one operation</span>
<div class='content'>
<dl class='dl-horizontal'>

    @foreach($q->all as $all)
    
    <dt>Shift</dt><dd>{{str_replace('_', '  ', $all->attendance->shift)}}</dd>
    <dt>Client</dt><dd>{{$all->client->fullname}}</dd>
    <dt>Total Time</dt><dd>{{$all->total_time}}</dd>
    <dt>Total Task Time</dt><dd>{{$all->total_task_time}}</dd>
    <dt>Total Idle Time</dt><dd>{{$all->total_idletime}}</dd>
    <dt>Total Break Time</dt><dd>{{$all->total_breaktime}}</dd>
    <dt>Billable Hours</dt><dd>{{$all->billable_hours}}</dd>
    @if (isset($all->attendance))
    <dt>Total Late Time</dt><dd>{{$all->attendance->late_time}}</dd>
    <dt>Late Reason</dt><dd>{{$all->attendance->late_reason}}</dd>
    @else
    <dt>Late Reason</dt><dd></dd>
    @endif
    <br>
    @endforeach
</dl>
</div>