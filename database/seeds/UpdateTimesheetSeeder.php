<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Timesheet;
use App\Models\Attendance;

class UpdateTimesheetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->command->info('Started updating timesheets for Schedule Date.');

        $timesheets = Timesheet::all();

        foreach ($timesheets as $timesheet) {
            if ($timesheet->attendance_id!=0) {
                $attendance = Attendance::select('id','schedule_date')
                ->find($timesheet->attendance_id);
                    $timesheet->schedule_date = $attendance->schedule_date;
                    $timesheet->save();
            }
        }

        $this->command->info('Done updating timesheets for Schedule Date.');

    }
}