@extends(Auth::user()->roles[0]['slug'] == 'manager' ? 'layouts.adminlte-master' : 'layouts.adminlte-master2')
@section('page-title', "Timesheets")

@section('content')
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <div class="float-left"><h3 class='box-title'>Weekly Team Report</h3></div>
                    <div class="pull-right"><a class="btn btn-sm btn-warning" title="Export current table to excel" href="#" id="export-to-excel"><small><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-save" viewBox="0 0 16 16"><path d="M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v7.293l2.646-2.647a.5.5 0 0 1 .708.708l-3.5 3.5a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L7.5 9.293V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z"/></svg> &nbsp;Export Records</small></a></div>
                </div>
                <div class='box-body'>
                
                    <div class='row'>
                        <div class='col-md-6'>
                        
                            <label>Select Week <small class="text-warning"> (click and press delete to reset)</small></label>
                            <div class="input-group date" id="datepicker">
                            <input type="text" id='target_date' name='target_date' class="form-control input-lg" value="{{ $firstdate }} - {{ $lastdate }}">
                            <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                            </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <label>Teams</label>
                            <select class="form-control input-lg" id="managers" name="managers">
                                @foreach($all_managers as $manager) 
                                	<option id="{{$manager->lead_user_id}}" {{$selected = $manager->lead_user_id == '16' ? 'selected' : ''}}>{{ucwords($manager->name)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class='row'><hr></div>

                    
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover table-striped table-compact dataTable sortable" id="ts_table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name <span class="pull-right" style="cursor: pointer;"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-sort-alpha-down" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M10.082 5.629 9.664 7H8.598l1.789-5.332h1.234L13.402 7h-1.12l-.419-1.371h-1.781zm1.57-.785L11 2.687h-.047l-.652 2.157h1.351z"/><path d="M12.96 14H9.028v-.691l2.579-3.72v-.054H9.098v-.867h3.785v.691l-2.567 3.72v.054h2.645V14zM4.5 2.5a.5.5 0 0 0-1 0v9.793l-1.146-1.147a.5.5 0 0 0-.708.708l2 1.999.007.007a.497.497 0 0 0 .7-.006l2-2a.5.5 0 0 0-.707-.708L4.5 12.293V2.5z"/></svg></span></th>
                                        
                                        <th>Client</th>
                                        <th>Position</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Day</th>
                                        <th>Contract Hours</th>
                                        <th>Actual Hours</th>
                                    </tr>
                                </thead>
                                <tbody id="databody"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Modal -->
<div class="modal fade" id="infoModalPosition" tabindex="-1" role="dialog" aria-labelledby="infoModalPositionLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="infoModalPositionLabel">Edit User Info</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div id="user_name"></div><br>
      
      <input type="hidden" id="tr_id" value="">
        <input type="hidden" id="sched_stat_id" value="">
        <input type="hidden" id="attendance_id" value="">
        <div id="edit_user_position">
        <strong><small>Position</small></strong><br>
            <select id="user-position" class="form-control">
                <option id="EVA">EVA</option>
                <option id="GVA">GVA</option>
                <option id="ISA">ISA</option>
                <option id="OP">OP</option>
                <option id="MKT">MKT</option>
                <option id="DEV">DEV</option>
            </select>
        </div>
        <br>
        <div id="edit_user_employment">
        <strong><small>Employment Status</small></strong><br>
            <select id="user-employment-status" class="form-control">
                <option id="" value="FT">FT (Fulltime)</option>
                <option id="" value="PT">PT (Part-time)</option>
            </select>
        </div>
        <br>
        <p><span class="text-success" id="error"></stpan>&nbsp;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-info">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<link rel="stylesheet" href="/js/autocomplete/easy-autocomplete.min.css">

<style>
.datepicker tr:hover {
background-color: #808080;
}

.ts-user-total{
    text-align: right;
    font-weight: 600;
}

.to_edit_position, .to_edit_status, .to_edit_amount{
    cursor: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16"><path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/></svg>') 25 25, auto !important;
}

.ts-user-id{
    font-weight: 500;
}

.ts-row{
    background: #eceff1 !important;
    margin-bottom: 20px !important;
}

.gray-row{
    background: #f0f0f0;
}

.spare-tr{
    background: #fff !important;
    border: 0px !important;
}


</style>
@endpush

@push('view-scripts')
<script src="https://code.jquery.com/ui/1.8.0/jquery-ui.min.js" integrity="sha256-i1lZLWfq3HA69s3Vuo0Hf5+UhdAftkBVVWFDNfib6Zs=" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="{{ asset('js/inplace-editing-table-simpletablecelleditor/SimpleTableCellEditor.js') }}"></script>
<script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>

<script>
var _table;

$(function() {
    var value = "{{date('Y-m-d', strtotime(now()))}}";
    var firstDate ="{{ $firstdate }}";
    var lastDate = "{{ $lastdate }}";
    
    var dateList = getDaysBetweenDates(firstDate, lastDate);
    $("#target_date").val(dateList.join(','));
    //console.log(dateList);
    
    fetchData($("#target_date").val(), '16');
    $('table.sortable th').click();
    $('table.sortable th').click();
    $('table.sortable th').click();

    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: false,
        forceParse: false,
        weekStart: 1,
        multidate: true,
        multidateSeparator: ",",
    }).on('hide', function(e) {
        target_date = $("#target_date").val();
        var target_day =new Date(e.date).getDate();
        var pickedMonth = new Date(e.date).getMonth() + 1;
        var pickedYear = new Date(e.date).getFullYear();
	
    	const fdate = moment(new Date(firstDate)).format("YYYY-MM-DD");
    	const ldate = moment(new Date(lastDate)).format("YYYY-MM-DD");
    	
        //$("#target_date").val(fdate+' - '+ldate);
        manager = $('#managers').find(":selected").attr('id');

        fetchData($("#target_date").val(), manager);
        $('table.sortable th').click();
        $('table.sortable th').click();
    });
    
    $('#managers').change(function(){
    	    	
        manager = $('#managers').find(":selected").attr('id');
        fetchData($("#target_date").val(), manager);
        $('table.sortable th').click();
        $('table.sortable th').click();
    });
    
    $('table.sortable th').click(function(){
        var table = $(this).parents('table').eq(0);
        var column_index = get_column_index(this);
        var rows = table.find('tbody tr').toArray().sort(comparer(column_index));
        this.asc = !this.asc;
        if (!this.asc){rows = rows.reverse()};
        for (var i = 0; i < rows.length; i++){table.append(rows[i])};
    });
    
    const editor =new SimpleTableCellEditor("ts_table");
    
    
    //editable table
    //editor.SetEditableClass("to_edit_status");
    //editor.SetEditableClass("to_edit_position");
    editor.SetEditableClass("to_edit_amount", { validation: $.isNumeric });
    
    $("#ts_table").on("cell:edited", function (event) {
        
        var newVal = '';
        var target_id = '';
               
        if(event.element.getAttribute('rel') == 'to_edit_amount'){
            newVal = event.newValue;
            target_id = event.element.getAttribute('data');
            //alert(event.element.getAttribute('data'));
            changeTsAmount(target_id, newVal);
        }
        //console.log(`'${event.oldValue}' changed to '${event.newValue}'`);
         manager = $('#managers').find(":selected").attr('id');
         setTimeout(
        function() 
        {
            fetchData($("#target_date").val(), manager);
        }, 2000);

    });
    
    var tr_id = '';
    var user_name = '';
    var job_status = '';
    var job_position = '';
    var sched_status = 'null';
    var attendance_id = '';
    var saved_success = '';
    
    $(document).on('click', '.to_edit_position', function(){
        //alert($(this).attr('id'));
        tr_id = $(this).parent('tr').find('.ts-user-id').text();
        user_name = $(this).parent('tr').find('.ts-user-name').text();
        job_status = $(this).parent('tr').find('.to_edit_status').text();
        job_position = $(this).parent('tr').find('.to_edit_position').text();
        sched_status = $(this).parent('tr').find('.to_edit_status').attr('data');
        attendance_id = $(this).parent('tr').find('#attendance_id').attr('data');
        //$('#edit_user_employment').hide();
        //$('#edit_user_position').show();
        $('#infoModalPosition').modal('show');
    });
    
    $(document).on('click', '.to_edit_status', function(){
        //alert($(this).attr('id'));
        tr_id = $(this).parent('tr').find('.ts-user-id').text();
        user_name = $(this).parent('tr').find('.ts-user-name').text();
        job_status = $(this).parent('tr').find('.to_edit_status').text();
        job_position = $(this).parent('tr').find('.to_edit_position').text();
        sched_status = $(this).parent('tr').find('.to_edit_status').attr('data');
        attendance_id = $(this).parent('tr').find('#attendance_id').attr('data');
        //$('#edit_user_employment').show();
        //$('#edit_user_position').hide();
        $('#infoModalPosition').modal('show');
    });
    
    $('#infoModalPosition').on('show.bs.modal', function (e) {
        saved_success = '';
        $('.modal-body #error').text('');
        $('.modal-body #user-position').val(job_position).change();
        $('.modal-body #user-employment-status').val(job_status).change();
        $('.modal-body #user_name').text(user_name);
        $('.modal-body #tr_id').val(tr_id);
        $('.modal-body #sched_stat_id').val(sched_status);
        $('.modal-body #attendance_id').val(attendance_id);
    });
    
    $('#save-info').click(function(){
        //alert('test');
        var user_position = $('.modal-body #user-position').val();
        var user_emp_status = $('.modal-body #user-employment-status').val();
        var user_id = $('.modal-body #tr_id').val();
        var id_attendance = $('.modal-body #attendance_id').val();
        
        $.get( "/dashboard/teams/get_weekly_save_info", { user_id:  user_id, user_position:  user_position, user_emp_status: user_emp_status, sched_status: sched_status, attendance_id:  id_attendance})
        .done(function( data ) {
        console.log( data );
        //$('#databody').html('');
        //$('#databody').html(data);
        if(data == '1'){
            $('.modal-body #error').text('User info saved');
            saved_success = data;
            //$('#infoModalPosition').modal('hide');
        }else{
            $('.modal-body #error').text('an error occured while saving. please contact Newton.');
        }
        });
    });
    
    $('#infoModalPosition').on('hide.bs.modal', function (e) {
        if(saved_success == '1'){
        target_date = $("#target_date").val();
        
        manager = $('#managers').find(":selected").attr('id');

        fetchData(target_date, manager);
        }
    });
    
    $('#export-to-excel').click(function(){
        var filename = $('#managers').find(":selected").text();
        var curdate =  $("#target_date").val();
        
        let table = document.getElementsByTagName("table");
        TableToExcel.convert(table[0], { // html code may contain multiple tables so here we are refering to 1st table tag
           name: filename + '_' + curdate.replace(/\s+/g, "") + '.xlsx', // fileName you could use any name
           sheet: {
              name: 'Sheet 1' // sheetName
           }
        });
  
    });
    
});

function changeTsAmount(id, amount){
$.get( "/dashboard/teams/get_weekly_edit", { id:  id, amount:  amount })
    .done(function( data ) {
        console.log( data );
    });
}

//function fetchData(firstDate, lastDate, manager) {
function fetchData(allDate, manager) {
$('#databody').html('');
$('#databody').html('<div style="padding: 10px;">please wait...</div>');
    $.get( "/dashboard/teams/get_weekly", { all_dates:  allDate, manager: manager })
    .done(function( data ) {
    console.log( data );
    $('#databody').html('');
    $('#databody').html(data);
    });
}

function comparer(index) {
    return function(a, b) {
        var valA = getCellValue(a, index), valB = getCellValue(b, index);
        return isNumber(valA) && isNumber(valB) ? valA - valB : valA.localeCompare(valB);
    }
}
function getCellValue(row, index){ return $(row).children('td').eq(index).html() };

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
@verbatim
function exportTableToExcel(tableId, filename) {
    let dataType = 'application/vnd.ms-excel';
    let extension = '.xls';

    let base64 = function(s) {
        return window.btoa(unescape(encodeURIComponent(s)))
    };

    let template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
    let render = function(template, content) {
        return template.replace(/{(\w+)}/g, function(m, p) { return content[p]; });
    };

    let tableElement = document.getElementById(tableId);

    let tableExcel = render(template, {
        worksheet: filename,
        table: tableElement.innerHTML
    });

    filename = filename + extension;

    if (navigator.msSaveOrOpenBlob)
    {
        let blob = new Blob(
            [ '\ufeff', tableExcel ],
            { type: dataType }
        );

        navigator.msSaveOrOpenBlob(blob, filename);
    } else {
        let downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);

        downloadLink.href = 'data:' + dataType + ';base64,' + base64(tableExcel);

        downloadLink.download = filename;

        downloadLink.click();
    }
}
@endverbatim
function get_column_index(element) {
    var clickedEl = $(element);
    var myCol = clickedEl.closest("th").index();
    var myRow = clickedEl.closest("tr").index();
    var rowspans = $("th[rowspan]");
    rowspans.each(function () {
        var rs = $(this);
        var rsIndex = rs.closest("tr").index();
        var rsQuantity = parseInt(rs.attr("rowspan"));
        if (myRow > rsIndex && myRow <= rsIndex + rsQuantity - 1) {
            myCol++;
        }
    });
    // alert('Row: ' + myRow + ', Column: ' + myCol);
    return myCol;
};

function getDaysBetweenDates(startDate, stopDate) {
    var dateArray = [];
    var currentDate = moment(startDate);
    var stopDate = moment(stopDate);
    while (currentDate <= stopDate) {
        dateArray.push( moment(currentDate).format('YYYY-MM-DD') )
        currentDate = moment(currentDate).add(1, 'days');
    }
    return dateArray;
}

$('#target_date').click(function(){
        $(this).focus();    
        $(this).select();    
 });
</script>

@endpush