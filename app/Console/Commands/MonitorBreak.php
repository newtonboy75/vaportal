<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Attendance;
use App\Models\User;
use App\Http\Controllers\API\UserAttendanceController;
use DateTime;

class MonitorBreak extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'client:break';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Periodically check if the VA is beyond allowed breaktime.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $dt = new DateTime();

        $md = $dt->format('Y-m-d H:i:s');

        $monitor_breaks = \DB::table('monitor_breaks')->whereRaw('`due_breaktime` < "'.$md.'"')->get();

        if (!empty($monitor_breaks)) {

            foreach($monitor_breaks as $monitor) {
                
                $user = User::find($monitor->user_id);

                //insert to notifications table
                $notif['content'] = $user->full_name . " has exceeded breaktime.";
                $notif['user_id'] = $user->id;
                $notif['type'] = "break-exceed";
                $notif['created_at'] = date("Y-m-d H:i:s");
                $notif['updated_at'] = date("Y-m-d H:i:s");
                \DB::table('notifications')->insert($notif);

                \DB::table('monitor_breaks')->where('id',$monitor->id)->delete();
            }
        }
    }

}