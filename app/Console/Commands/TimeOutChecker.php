<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Attendance;
use App\Models\User;
use App\Models\UserPing;
use App\Http\Controllers\API\UserAttendanceController;
use DateTime;

class TimeOutChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'client:timeout';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Periodically check if the VA is beyond the schedule date time.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $dt = new DateTime();
        $dt->modify('-5 minutes');
        $md = $dt->format('Y-m-d H:i:s');

        $user_pings = UserPing::whereRaw('`updated_at` < "'.$md.'"')
        ->pluck('user_id')->all();

        $attendances = Attendance::where('status', Attendance::STATUS_IN)
        ->whereIn('user_id', $user_pings)
        ->get();
        
        $now = time();
        echo "\r\nAttendance Count:" . count($attendances) . "\r\n";

        foreach($attendances as $attendance) {
            $user = User::find($attendance->user_id);
            echo "\r\ndoTimeout user:" . $user->id . "\r\n";
            UserAttendanceController::doTimeout($attendance);
        }

        /*foreach($attendances as $attendance) {
            $sched_end = '';
            if(strtotime($attendance->start_time) < strtotime($attendance->end_time)) { // normal
                $sched_end = $attendance->date_in . ' ' . $attendance->end_time;
            } else { // end time is next day
                $sched_end = date('Y-m-d', strtotime($attendance->date_in . ' +1 day')) . ' ' . $attendance->end_time;
            }

            if($now > strtotime($sched_end)) { // force time out
                // check if socket_id exists
                $user = User::find($attendance->user_id);
                if(is_null($user->socket_id)) {
                    echo "\r\ndoTimeout user:" . $user->id . "\r\n";
                    UserAttendanceController::doTimeout($attendance);
                } else {
                    // do ping request
                    echo "\r\nPing user:" . $user->id . "\r\n";
                    $user->broadcast()->ping();
                }
            }
        }

        $users = User::where("is_online",1)->get();
        foreach ($users as $user) {
            $att = Attendance::where("user_id", $user->id)
            ->where('status', 'in')->get();
            if (empty($att)) {
                $user->is_online = 0;
                $user->access_token = null;
                $user->socket_id = null;
                $user->save();
                echo "\r\nis_online=0 set to user:" . $user->id . "\r\n";
            }
        }*/
    }
}