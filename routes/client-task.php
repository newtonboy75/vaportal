<?php

//TODO: update middleware for permissions

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {	

        Route::get('/dashboard/client-tasks', [
            'uses' => 'Admin\ClientTaskController@index'
        ]);

        Route::get('/dashboard/client-tasks/archive', [
            'uses' => 'Admin\ClientTaskController@indexArchive'
        ]);

        Route::get('/dashboard/client-tasks/deleted', [
            'uses' => 'Admin\ClientTaskController@indexDeleted'
        ]);

        Route::get('/dashboard/client-tasks/datatables', [
            'uses' => 'Admin\ClientTaskController@datatables'
        ]);

        Route::get('/dashboard/client-tasks/archive-datatables', [
            'uses' => 'Admin\ClientTaskController@archivedDatatables'
        ]);

        Route::get('/dashboard/client-tasks/deleted-datatables', [
            'uses' => 'Admin\ClientTaskController@deletedDatatables'
        ]);

        Route::get('/dashboard/client-tasks/show/{id}', [
            'uses' => 'Admin\ClientTaskController@show'
        ])->where('id', '[0-9]+'); 

        Route::get('/dashboard/client-tasks/view/{id}', [
            'uses' => 'Admin\ClientTaskController@viewTask'
        ])->where('id', '[0-9]+'); 

        Route::post('/dashboard/client-tasks/notes', [
            'uses' => 'Admin\ClientTaskController@saveNotes'
        ]);  


        Route::get('/dashboard/client-tasks/create', [
            'uses' => 'Admin\ClientTaskController@create'
        ]);

        Route::post('/dashboard/client-tasks/create', [
            'uses' => 'Admin\ClientTaskController@store'
        ]);  

        Route::post('/dashboard/client-tasks/update', [
            'uses' => 'Admin\ClientTaskController@update'
        ]);

        Route::get('/dashboard/client-tasks/edit/{id}', [
            'uses' => 'Admin\ClientTaskController@edit',
            'selected_nav_path' => 'dashboard/client-tasks/edit',
            'selected_parent_path' => 'dashboard/client-tasks'
        ])->where('id', '[0-9]+');

        Route::get('/dashboard/client-tasks/delete/{id}', [
            'uses' => 'Admin\ClientTaskController@delete'
        ])->where('id', '[0-9]+');

        Route::get('/dashboard/client-tasks/delete-permanent/{id}', [
            'uses' => 'Admin\ClientTaskController@deletePermanent'
        ])->where('id', '[0-9]+');

});