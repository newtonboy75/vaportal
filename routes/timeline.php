<?php

Route::group(array(
    'middleware' => ['web', 'admin.auth'],
    ), function() {

        Route::get('/dashboard/timeline', [
            'uses' => 'Admin\TimelineController@index'
        ]);
        Route::get('/dashboard/timeline/datatables', [
            'uses' => 'Admin\TimelineController@datatables'
        ]);
});