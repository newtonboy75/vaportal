<?php

namespace App;

use App\Models\User;
use App\Events\Broadcast;

class Broadcaster
{
    protected $user;

    function __construct(User $user)
    {
        $this->user = $user;
    }

    public function screenshot()
    {
        event(new Broadcast('user', 'screenshot', $this->user->id));
    }

    public function processList($screenshot = 0)
    {
        event(new Broadcast('user', 'process-list', $this->user->id, [
            'screenshot_id' => $screenshot
        ]));
    }

    public function browserHistory($minutes = 60, $screenshot = 0)
    {
        event(new Broadcast('user', 'browser-history', $this->user->id, [
            'minutes' => $minutes,
            'screenshot_id' => $screenshot
        ]));
    }

    public function privateMessage($message = '', $all = false)
    {
        $channel = ($all) ? 'users' : 'user';
        $uid = ($all) ? null : $this->user->id;
        event(new Broadcast($channel, 'message-alert', $uid, [
            'message' => $message
        ]));
    }

    public function ping()
    {
        event(new Broadcast('user', 'ping', $this->user->id));
    }
}
