<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\Setting;
use Illuminate\Validation\Rule;

class SettingsController extends Controller
{
    public function index()
    {
        $auth_user = \Auth::user();
        if ($auth_user->is('administrator')) {
            $settings = Setting::select(['id','name','value','slug'])->get();
            return view('admin.settings.update', compact('settings'));
        } else {
            return view('admin.settings.index');
        }
    }

    //this is for administrators only
    public function save(Request $request)
    {
        $input = $request->all();
        foreach ($input as $key => $value) {
            if ($key != "_token") {
                if ($value != "") {
                    Setting::where('slug',$key)->update(['value' => $value]);

                }
            }
        }
        return redirect()->intended('/dashboard/settings')->with('notification_message', 'Update successful'); 
    }

    public function create()
    {
        return view('admin.settings.create_edit');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'slug' => 'required|max:255',
            'value' => 'required|max:255',
        ]);

        Setting::create([
            'name' => $request->name,
            'slug' => $request->slug,
            'value' => $request->value
        ]);

        return redirect('/dashboard/settings');
    } 

    public function edit($id)
    {

        $q = Setting::where('id', $id)->first();

        if (empty($q)) {
            return redirect('/dashboard');
        }
        return view('admin.settings.create_edit', compact('q'));
    }

    public function update(Request $request) 
    {
        $q = Setting::where('id', $request->id)->first();

        $request->validate([
            'name' => 'required|max:255',
            'slug' => 'required|max:255',
            'value' => 'required|max:255',
        ]);

        if (!empty($q)) {
            $q->value = $request->value;
            $q->name = $request->name;
            $q->slug = $request->slug;
            $q->save();
        }

        return redirect()->intended('/dashboard/settings/edit/'. $q->id)->with('notification_message', 'Update successful'); 

    }  

    public function show($id)
    {
        $q = Setting::find($id);
        $html = view('admin.settings.show', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }

    public function delete($id)
    {
        $q = Setting::where('id', $id)->first();
        
        if (!empty($q)) {
            $q->delete();
        }        
        $response['status'] = "ok";
        return json_encode($response);
    }

    public function datatables()
    {
        $user = \Auth::user();
        $settings = Setting::select(['id','name','value']);

        return DataTables::of($settings)

            ->addColumn('actions', function($setting){
                $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$setting->id.'"><i class="fa fa-trash"></i></a>';
                $edit_btn = '<a class="btn btn-info" href="/dashboard/settings/edit/'.$setting->id.'"><i class="fa fa-edit"></i></a>';
                return '<div class="btn-toolbar">'. $edit_btn .  $delete_btn .'</div>';
            
        })->rawColumns(['actions'])
        ->make(true);

    }

}