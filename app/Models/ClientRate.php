<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientRate extends Model
{
    protected $table = "client_rates";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'va_id', 'rate_per_hour'];

    public function addLog($array) {
    	ClientRateLog::create($array);
    }

    public function getDefaultRate() {
        $rate = $this->where('user_id', $this->user_id)->where('va_id', 0)->first();
        return $rate->rate_per_hour;
    }

    public function getRatePerHourFormattedAttribute() {
    	if(isset($this->rate_per_hour)) {
        	return number_format( $this->rate_per_hour, 2, '.', '' );
        } else {
        	return number_format( 0, 2, '.', '' );;
        }
    }

    public function getClientRate($va_id) {
        return $this->where('user_id', $this->user_id)->where('va_id', $va_id)->first();
    }

    public function client() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function va() {
        return $this->hasOne('App\Models\User', 'id', 'va_id');
    }
}
