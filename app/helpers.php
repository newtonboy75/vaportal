<?php

/*
|--------------------------------------------------------------------------
| VA_VIEW
|--------------------------------------------------------------------------
|
| Will return the view of the current theme that is set in config.app.theme
|
*/

if (! function_exists('va_view')) {
    function va_view($path, $a = []) {
        $kaboom = explode('.', $path);

        // Template is in a sub folder
        if( count($kaboom) > 2 ) {
            $main = $kaboom[0] . '.' . $kaboom[1];
            $sub = '';
            array_splice($kaboom, 0, 2);
            
            if( count($kaboom) > 0 ) {
                $sub = '.' . implode('.', $kaboom);
            }
        } else {
            $main = $kaboom[0];
            $sub = '';
            array_splice($kaboom, 0, 1);
            
            if( count($kaboom) > 0 ) {
                $sub = '.' . implode('.', $kaboom);
            }
        }

        $new_path = $main . '.' . config('app.theme') . $sub;
        return view($new_path, $a);
    }
}