<?php

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {	

        Route::get('/dashboard/roles', [
            'middleware' => 'check.permission:list_roles',
            'uses' => 'Admin\RoleController@index'
        ]);

        Route::get('/dashboard/roles/datatables', [
            'middleware' => 'check.permission:list_roles',
            'uses' => 'Admin\RoleController@datatables'
        ]);

        Route::get('/dashboard/roles/show/{id}', [
            'middleware' => 'check.permission:list_roles',
            'uses' => 'Admin\RoleController@show'
        ])->where('id', '[0-9]+'); 

        /*  Route::get('/dashboard/roles/create', [
            'middleware' => 'check.permission:add_roles',
            'uses' => 'Admin\RoleController@create'
        ]);        

       Route::post('/dashboard/roles/create', [
            'middleware' => 'check.permission:add_roles',
            'uses' => 'Admin\RoleController@store'
        ]);  

        Route::post('/dashboard/roles/update', [
            'middleware' => 'check.permission:edit_roles',
            'uses' => 'Admin\RoleController@update'
        ]);

        Route::get('/dashboard/roles/edit/{id}', [
            'middleware' => 'check.permission:edit_roles',
            'uses' => 'Admin\RoleController@edit',
            'selected_parent_path' => 'dashboard/roles',
            'selected_nav_path' => 'dashboard/roles/edit',
        ])->where('id', '[0-9]+');         

        Route::get('/dashboard/roles/delete/{id}', [
            'middleware' => 'check.permission:delete_roles',
            'uses' => 'Admin\RoleController@delete'
        ])->where('id', '[0-9]+'); */

        Route::get('/dashboard/role-permissions/{id}', [
            'uses' => 'Admin\RolePermissionController@index'
        ])->where('id', '[0-9]+');        

        Route::post('/dashboard/role-permissions/{id}/update', [
            'uses' => 'Admin\RolePermissionController@updatePermissions'
        ]);

});	