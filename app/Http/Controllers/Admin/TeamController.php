<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
use App\Models\Team;
use App\Models\TeamUser;
use App\Models\UserRole;
use App\Models\ManagerClient;
use App\Models\Timesheet;
use App\Models\Schedule;
use App\Models\Attendance;
use Illuminate\Validation\Rule;
use App\Models\ActivityLog;
use App\Models\ScheduleStatus;

class TeamController extends Controller
{

    private $auser;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->auser = \Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        return view('admin.team.index');
    }

    public function create()
    {
        //$available_va = Team::get_available_va();
        $available_manager = Team::getAvailableManager();
        return view('admin.team.create_edit', compact('available_manager'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:100',
            'lead_user_id' => 'required',
        ]);

        $team = Team::create([
            'name' => $request->name,
            'lead_user_id' => $request->lead_user_id,
        ]);

        $team->addUser( $request->lead_user_id );

        ActivityLog::addLog("User [{$this->auser->first_name} {$this->auser->last_name}] created a new team [{$request->name}].");

        return redirect('/dashboard/teams/edit/' . $team->id)->with('notification_message', 'Team has been created.'); 
    } 

    public function edit( $id )
    {
        $q = Team::find( $id );
        if ( empty( $q ) ) {
            return redirect( '/dashboard' );            
        }

        $available_manager = Team::getAvailableManager($q->lead_user_id);
        return view('admin.team.create_edit', compact('q', 'available_manager'));        
    }

    public function update(Request $request) 
    {
        $q = Team::find($request->id);

        $request->validate([
            'name' => 'required|max:100',
            'lead_user_id' => 'required',
        ]);

        if (!empty($q)) {
            $q->name = $request->name;
            $q->lead_user_id = $request->lead_user_id;
            $q->save();

            ActivityLog::addLog("User [{$this->auser->first_name} {$this->auser->last_name}] updated team [{$request->name}].");
        }

        return redirect()->intended('/dashboard/teams/edit/'. $q->id)->with('notification_message', 'Team has been updated.'); 

    }  

    public function show($id)
    {
        $q = Team::find($id);
        $html = view('admin.team.show', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }

    public function delete($id)
    {
        $q = Team::find($id);
        if (!empty($q)) {
            ActivityLog::addLog("User [{$this->auser->first_name} {$this->auser->last_name}] deleted team [{$q->name}].");
            $q->delete();
        }        
        $response['status'] = "ok";
        return json_encode($response);
    }

    public function datatables()
    {

    	$teams = Team::select(DB::raw("teams.id, name, CONCAT( first_name, ' ', last_name ) AS lead_user"))
                    ->join('users', 'teams.lead_user_id', 'users.id');

        return DataTables::of($teams)
            ->addColumn('team_count', function($team){
                $team_user_count = DB::table('team_user')->where('team_id', $team->id)->count();
                return $team_user_count;
            })
            ->addColumn('client_count', function($team){
                $team_info = DB::table('teams')->where('id', $team->id)->first();
                if (!empty($team_info)) {
                    $client_count = DB::table('manager_client')->where('user_id', $team_info->lead_user_id)->count();
                } else {
                    $client_count = 0;
                }
                return $client_count;
            })
            ->addColumn('actions', function($team){
                $team_info_btn = '<a data-toggle="modal" title="Show Team Members" data-target="#team-members" class="btn btn-success button-team-members" data-id="'.$team->id.'"><i class="fa fa-info"></i></a>';
                $team_add_btn = '<a data-toggle="modal" title="Add Team Member" data-target="#team-add-member" class="btn btn-info button-team-add-members-modal" data-id="'.$team->id.'"><i class="fa fa-plus"></i></a>'; 
                $delete_btn = '<a data-toggle="modal" title="Delete Team" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$team->id.'"><i class="fa fa-trash"></i></a>';
                $edit_btn = '<a class="btn btn-info" title="Edit Team" href="/dashboard/teams/edit/'.$team->id.'"><i class="fa fa-edit"></i></a>';    

                return '<div class="btn-toolbar">'. $team_info_btn . $team_add_btn . $edit_btn .  $delete_btn .'</div>';

        })->rawColumns(['actions'])
        ->make(true);        

    }


    function show_team_members($id) {
        $q = Team::find($id);

        $html = view('admin.team.team-members', compact('q'))->render();
        $response['team_name'] = $q->name;
        $response['html'] = $html;
        return json_encode($response);
    }

    function show_available_va($id) {
        $q = Team::get_available_va($id);
        $html = view('admin.team.team-members-add', compact('q', 'id'))->render();
        $response['va_count'] = count($q);
        $response['html'] = $html;
        return json_encode($response);
    }

    function team_member_add(Request $request) {
        $q = Team::find($request->team_id);
        $q->addUser( $request->va_id );
        $response['html'] = 'Team member has been added';
        return json_encode($response); 
    }

    function team_member_remove(Request $request) {
        $q = Team::find($request->team_id);
        $q->removeUser( $request->va_id );
        $response['html'] = 'Team member has been removed';
        return json_encode($response); 
    }
    
    public function team_summary(){
        
        $all_managers = Team::select('id', 'name', 'lead_user_id')->get();
        
        $curdate = date('d');
        
        if($curdate >= 11 && $curdate <= 25){
            $firstdate = date('Y-m-11');
            $lastdate = date('Y-m-25');
        }elseif($curdate >= 1 && $curdate <= 10){
            $firstdate = date('Y-m-26', strtotime('-1 months'));
            $lastdate = date('Y-m-10');
        }elseif($curdate >= 26 && $curdate <= 31){
            $firstdate = date('Y-m-26');
            $lastdate = date('Y-m-10', strtotime('+1 months'));
        }
        
        return view('admin.team.weekly_index', compact('all_managers', 'firstdate', 'lastdate'));
    }
    
    
        function getWeekly() {

        $html = '';
        $id = request()->manager;
        $all_dates = explode(',', request()->all_dates);
        $attendances = '';
        
        $team = Team::where('lead_user_id', $id)->first();
        $vas = TeamUser::where('team_id', $team->id)->pluck('user_id');
        $users = User::whereIn('id', $vas)->orderBy('first_name')->with('teams')->get();
        
        foreach($users as $u){
            $my_team_id = '';
            foreach($u->teams as $tm){
                if($tm->lead_user_id == $id){
                    $my_team_id = $tm->lead_user_id;
                }
            }
            
            $attendances = Attendance::where('user_id', $u->id)->whereIn('schedule_date',  $all_dates)->pluck('id');          
            
            $ts = Timesheet::whereIn('attendance_id', $attendances)->whereIn('schedule_date',  $all_dates)->orderBy('client_id', 'asc')->orderBy('schedule_date', 'asc')->get();
            $ts_ = Timesheet::whereIn('attendance_id', $attendances)->whereIn('schedule_date',  $all_dates)->orderBy('client_id', 'asc')->groupBy('client_id')->orderBy('schedule_date', 'asc')->pluck('client_id');
            
            
            $has_record = false;
            $billable_hours_sum = 0;
            $i = 0;
            $row_class = '';
            $sked_stat = 'FT';
            $sched_id = null;
            $user_pos = 'VA';
            $myteam = '';
            $myclients = [];
            
            foreach($ts_ as $s){
                $current_managers = ManagerClient::where('user_id', $my_team_id)->where('client_id', $s)->orderBy('id')->first();
                if(isset($current_managers)){
                        if($current_managers->user_id == $id){
                            $myclients[] = $current_managers->client_id;
                        }
                }
            }
            
            if(count($myclients) == 1){
                foreach($ts as $t){
                
                $current_managers = ManagerClient::where('user_id', $my_team_id)->where('client_id', $t->client_id)->orderBy('id')->first();
                $sched = Schedule::where('day_of_week', $t->attendance->day_of_week)->where('start_time', $t->attendance->start_time)->where('end_time', $t->attendance->end_time)->where('user_id', $t->user->id)->where('client_id', $t->client_id)->first();
                     
                if(isset($sched->id)){
                    $schedule_status = ScheduleStatus::where('schedule_id', $sched->id)->first();
                    $sked_stat = isset($schedule_status->status) ? $schedule_status->status : '*FT';
                    $sched_id = isset($schedule_status->status)  ? $sched->id : null;
                    $user_pos = isset($schedule_status->position)  ? $schedule_status->position : '*VA';
                }
                
                $billable_hours = substr(number_format($t->billable_hours, 2), 0, -1);
                
                if(isset($current_managers)){
                        if($current_managers->user_id == $id){
                            
                        $html .= '<tr id="'.$t->id.'"><td class="ts-user-id">'.$t->user->id.'</td><td title="click to edit" class="ts-user-name" id="'.$t->user->id.'">'.$t->user->first_name.' ' .$t->user->last_name.'</td><td>'.$t->client->first_name.' '.$t->client->last_name.'</td><td  class="to_edit_position" rel="'.$sched_id.'">'.$user_pos.'</td><td class="to_edit_status" data="'.$sched_id.'" rel="'.$t->user->id.'">'.$sked_stat.'</td><td id="attendance_id" data="'.$t->attendance->id.'">'.date('d-M-Y', strtotime($t->schedule_date)).'</td><td>'.date('D', strtotime($t->attendance->day_of_week)).'</td><td>'.round($t->attendance->hours_needed).'</td><td title="click to edit" class="to_edit_amount ts-user-total" rel="to_edit_amount" data="'.$t->id.'">'.(floor($billable_hours * 2) / 2).'</td></tr>';
                        
                        $has_record = true;
                        $billable_hours_sum += (floor($billable_hours * 2) / 2);
                                                    
                        }
                } 
                 
            }

            if($has_record){
                $html .= '<tr class="ts-row"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td id="'.$u->id.'" class="ts-user-total">Total:&nbsp;&nbsp;'.$billable_hours_sum.'</td></tr>';
                $billable_hours_sum = 0;
            }
            
            }else{
                foreach($myclients as $s){
                    foreach($ts as $t){
                
                $current_managers = ManagerClient::where('user_id', $my_team_id)->where('client_id', $t->client_id)->orderBy('id')->first();
                $sched = Schedule::where('day_of_week', $t->attendance->day_of_week)->where('start_time', $t->attendance->start_time)->where('end_time', $t->attendance->end_time)->where('user_id', $t->user->id)->where('client_id', $t->client_id)->first();
                     
                if(isset($sched->id)){
                    $schedule_status = ScheduleStatus::where('schedule_id', $sched->id)->first();
                    $sked_stat = isset($schedule_status->status) ? $schedule_status->status : '*FT';
                    $sched_id = isset($schedule_status->status)  ? $sched->id : null;
                    $user_pos = isset($schedule_status->position)  ? $schedule_status->position : '*VA';
                }
                
                $billable_hours = substr(number_format($t->billable_hours, 2), 0, -1);
                
                if(isset($current_managers)){
                        if($current_managers->user_id == $id && $t->client_id == $s){
                            
                        $html .= '<tr id="'.$t->id.'"><td class="ts-user-id">'.count($myclients).$t->user->id.'</td><td title="click to edit" class="ts-user-name" id="'.$t->user->id.'">'.$t->user->first_name.' ' .$t->user->last_name.'</td><td>'.$t->client->first_name.' '.$t->client->last_name.'</td><td  class="to_edit_position" rel="'.$sched_id.'">'.$user_pos.'</td><td class="to_edit_status" data="'.$sched_id.'" rel="'.$t->user->id.'">'.$sked_stat.'</td><td id="attendance_id" data="'.$t->attendance->id.'">'.date('d-M-Y', strtotime($t->schedule_date)).'</td><td>'.date('D', strtotime($t->attendance->day_of_week)).'</td><td>'.round($t->attendance->hours_needed).'</td><td title="click to edit" class="to_edit_amount ts-user-total" rel="to_edit_amount" data="'.$t->id.'">'.(floor($billable_hours * 2) / 2).'</td></tr>';
                        
                        $has_record = false;
                        $billable_hours_sum += (floor($billable_hours * 2) / 2);
                                                    
                        }
                } 
                 
            }
        
                    if($has_record == false){
                        $html .= '<tr class="ts-row"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td id="'.$u->id.'" class="ts-user-total">Total:&nbsp;&nbsp;'.$billable_hours_sum.'</td></tr>';
                        $billable_hours_sum = 0;
                    }
                }
            }
        }
        
        return $html;
    }
        
    public function getWeeklyEditTs(){
        $timesheet = Timesheet::find(request()->id);
        $timesheet->billable_hours = request()->amount;
        $timesheet->save();
        
        if($timesheet){
            return $timesheet->billable_hours;
        }
    }
    
    public function get_weekly_save_info(){

        $attendance = Attendance::where('id', request()->attendance_id)->first();
        
        if(!$attendance){
            return 0;
        }
        
        $sched = Schedule::where('day_of_week', $attendance->day_of_week)->where('start_time', $attendance->start_time)->where('end_time', $attendance->end_time)->where('user_id', $attendance->user_id)->where('client_id', $attendance->client_id)->first();
        $user_status = ScheduleStatus::updateOrCreate(['schedule_id' => $sched->id], ['user_id' => request()->user_id, 'status' => request()->user_emp_status, 'position' => request()->user_position]);
        
        if(!$user_status){
            return 0;
        }
        
        return 1;
    }    


}