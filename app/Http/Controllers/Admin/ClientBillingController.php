<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\ClientBilling;
use App\Models\User;
use Illuminate\Validation\Rule;
use App\Models\ActivityLog;

class ClientBillingController extends Controller
{
    public function index()
    {
        $auth_user = \Auth::user();
        if ($auth_user->is('client')) {
            return va_view('admin.client-billing.client-index');
        }

        return view('admin.client-billing.index');
    }


    public function datatables()
    {

        $auth_user = \Auth::user();
        if ($auth_user->is('client')) {
            $client_billings = ClientBilling::where('client_id', $auth_user->id)
            ->select(['id','invoice_number', 'client_id', 'date_generated','billing_period', 'total_amount_due', 'status', 'pdf_path']);

            return DataTables::of($client_billings)
            ->editColumn('id', function ($cb) {
                return $cb->invoice_number;
            }) 
            ->removeColumn('client_id')
            ->addColumn('actions', function($cb){
                $download_btn = '<a title="Download Invoice" target="_blank" class="btn btn-success" href="'.$cb->pdf_path.'"><i class="fa fa-download"></i></a>';
                //$info_btn = '<a class="btn btn-info" href="/dashboard/invoice/info/'.$cb->id.'"><i class="fa fa-info"></i></a>';
                $info_btn = "";
                return '<div class="btn-toolbar">'. $info_btn . $download_btn . '</div>';
            })->rawColumns(['actions'])
            ->make(true); 
        }


        $client_billings = ClientBilling::select(['id','invoice_number','client_id', 'billing_period', 'total_amount_due', 'status', 'email_status', 'pdf_path']);

        return DataTables::of($client_billings)
            ->addColumn('check', function($cb) {
                return $cb->id;
            })
            ->editColumn('id', function ($cb) {
                return $cb->invoice_number;
            })
            ->editColumn('client_id', function($cb) {
                return $cb->client->first_name . " " . $cb->client->last_name;
            })
            ->addColumn('actions', function($cb){
                $download_btn = '<a title="Download Invoice" target="_blank" class="btn btn-success" href="'.$cb->pdf_path.'"><i class="fa fa-download"></i></a>';
                $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$cb->id.'"><i class="fa fa-trash"></i></a>';
                $edit_btn = '<a class="btn btn-info" href="/dashboard/invoice/edit/'.$cb->id.'"><i class="fa fa-edit"></i></a>';
                return '<div class="btn-toolbar">'. $edit_btn .  $delete_btn . $download_btn . '</div>';
        })->rawColumns(['actions'])
        ->make(true); 
    }

    public function show($id)
    {
        $q = ClientBilling::find($id);
        $html = view('admin.client-billing.show', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }

    public function delete($id)
    {
        $auth_user = \Auth::user();
        $q = ClientBilling::where('id', $id)->first();
        
        if (!empty($q)) {
            ActivityLog::addLog("User [{$auth_user->first_name} {$auth_user->last_name}] deleted the invoice number[{$q->invoice_number}] for [{$q->client->first_name} {$q->client->last_name}].");
            $q->delete();

            //delete pdf
            unlink(public_path() . $q->pdf_path);
        }
        $response['status'] = "ok";
        return json_encode($response);
    }

    public function edit($id)
    {
        $q = ClientBilling::where('id', $id)->first();

        if (empty($q)) {
            return redirect('/dashboard');
        }
        return view('admin.client-billing.edit', compact('q'));
    }

    public function update(Request $request) 
    {

        $q = ClientBilling::where('id', $request->id)->first();

        $request->validate([
            'status' => 'required|max:16',
        ]);

        if (!empty($q)) {
            $q->status = $request->status;
            $q->save();
        }

        return redirect()->intended('/dashboard/invoice/edit/'. $q->id)->with('notification_message', 'Invoice has been updated.'); 

    } 

    public function deleteMultiple(Request $request)
    {
        $multiple_delete_id = $request->multiple_delete_id;

        $auth_user = \Auth::user();

        foreach ($multiple_delete_id as $id) {
            $q = ClientBilling::where('id', $id)->first();
            
            if (!empty($q)) {
                ActivityLog::addLog("User [{$auth_user->first_name} {$auth_user->last_name}] deleted the invoice number[{$q->invoice_number}] for [{$q->client->first_name} {$q->client->last_name}].");
                $q->delete();

                //delete pdf
                //unlink(public_path() . $q->pdf_path);
            }
        }

        $response['status'] = "ok";
        return json_encode($response);
    }

}