<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model {

    protected $table = "role_user";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'role_id'];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }    
}