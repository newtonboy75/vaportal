<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Screenshot;
use Illuminate\Support\Facades\Storage;

class ScreenshotCleanup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'screenshot:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleanup today\'s screenshot';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date  = \Carbon\Carbon::now()->subDays(2);

        collect(Storage::disk('uploads')->listContents('screenshots', true))->each(function ($file) use ($date) {
            if ($file['type'] == 'file' && $file['timestamp'] < $date->getTimestamp()) {
                Storage::disk('uploads')->delete($file['path']);
                //echo 'Deleting file ' . $file['path'] . PHP_EOL;
            }
        });

        Screenshot::withoutGlobalScope('uploaded')->where('created_at', '<=', $date->toDateTimeString())->whereNull('filename')->delete();

    }
}
