<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ScheduleStatus extends Model {

    protected $table = "schedule_status";
    
    protected $fillable = [
        'schedule_id', 
        'status', 
        'position',
        'user_id',
    ];


   

}
