<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientBilling extends Model
{
    protected $table = "client_billing";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 
        'date_generated',
        'billing_period',
        'invoice_number',
        'pre_payment',
        'taxes_due',
        'total_billable_hours',
        'total_amount_due',
        'status',
        'email_status'
    ];

    public function client()
    {
        return $this->belongsTo("App\Models\User", "client_id");
    } 
}
