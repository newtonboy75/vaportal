<div class="col-md-4">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">VA Summary</h3> 
		</div>
		<div class="box-body">
			<div class="form-group">
				<label>Display VA Hours by</label>
				<select class="form-control" id="dashboard-va-summary-duration">
					<option value="weekly">Weekly</option>
					<option value="monthly">Monthly</option>
				</select>
			</div>
			<div id="va-summary-table">
			</div>
		</div><!-- box-body -->
		<div class="box-footer">
		</div>
	</div>

</div>