<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use DateTime;
use DateTimeZone;

class Schedule extends Model {

    protected $table = "schedules";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'user_id', 
        'client_id', 
        'start_time', 
        'end_time', 
        'shift',
        'timezone',
        'day_of_week',
        'week',
        'year',
        'max_breaktime',
        'paid_break',
        'hours_needed',
        'user_start_time',
        'user_end_time',
        'user_timezone',
        'user_day_of_week',
        'user_day_of_week_end'
    ];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

    public function client()
    {
        return $this->belongsTo("App\Models\User", "client_id");
    }

    public function showShift()
    {
        $shift_config = config('vatimetracker.shift');
        foreach ($shift_config as $key => $value) {
            if ($key == $this->shift) {
                return $value;
                break;
            }
        }
    }

    public function startTimeAMPM()
    {
        return date("h:i A",strtotime($this->start_time));
    }

    public function endTimeAMPM()
    {
        return date("h:i A",strtotime($this->end_time));
    }
    
    public function showDetail($target_date, $user) 
    {
        $schedule = $this;

        $sched_timezone = "";
        $timezone_config = config('vatimetracker.timezone');
        foreach ($timezone_config as $key => $value) {
            if ($key == $this->timezone) {
                $sched_timezone = $value;
                break;
            }
        }
        
        $user_timezone = "";
        $timezone_config = config('vatimetracker.timezone');
        foreach ($timezone_config as $key => $value) {
            if ($key == $user->timezone) {
                $user_timezone = $value;
                break;
            }
        }

        $start_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date . " " . $schedule->start_time)), new DateTimeZone($sched_timezone));
        $end_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date . " " . $schedule->end_time)), new DateTimeZone($sched_timezone));
        
        $orig_schedule = $start_time->format("h:i A l") . " - " . $end_time->format("h:i A l") . ": " . $schedule->timezone;

        $start_time->setTimezone(new DateTimeZone($user_timezone));
        $end_time->setTimezone(new DateTimeZone($user_timezone));
        
        $updated_schedule = $start_time->format("h:i A l") . " - " . $end_time->format("h:i A l") . ": " . $user->timezone;
        
        return "<div>" . $orig_schedule . "</div><div>" . $updated_schedule . "</div>";
    }

    public function showVASchedule()
    {
        return "<div>" . date("h:i A",strtotime($this->user_start_time)) . " - " . date("h:i A",strtotime($this->user_end_time)) . "</div>";
    }

    // converts schedule date time to client date time
    public function getClientScheduleDate() {

        $user_timezone = "";
        $timezone_config = config('vatimetracker.timezone');
        foreach ($timezone_config as $key => $value) {
            if ($key == $this->user_timezone) {
                $user_timezone = $value;
                break;
            }
        }

        $client_timezone = "";
        $timezone_config = config('vatimetracker.timezone');
        foreach ($timezone_config as $key => $value) {
            if ($key == $this->timezone) {
                $client_timezone = $value;
                break;
            }
        }

        $date_now = date("Y-m-d");
        if ($this->user_day_of_week != date('l')) {
            $date_now = date('Y-m-d', strtotime('-1 day', strtotime( $date_now ) ) );
        }

        $timezone = new DateTime($date_now. " " .$this->user_start_time, new DateTimeZone($user_timezone));
        $timezone->setTimezone(new DateTimeZone($client_timezone));
        return $timezone->format("Y-m-d");
    }

    // public function getUserStartTimeAttribute()
    // {
    //     $target_date = new DateTime();
    //     $target_date->modify('next '. $this->day_of_week);

    //     $seconds = ($this->hours_needed * 3600);
    //     // we're given hours, so let's get those the easy way
    //     $hours = floor($this->hours_needed);
    //     // since we've "calculated" hours, let's remove them from the seconds variable
    //     $seconds -= $hours * 3600;
    //     // calculate minutes left
    //     $minutes = floor($seconds / 60);

    //     $start_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date->format("Y-m-d"). " ".$this->start_time)), new DateTimeZone($this->client->showTimezone()));
    //     $end_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date->format("Y-m-d"). " ".$this->start_time)), new DateTimeZone($this->client->showTimezone()));
    //     $end_time->modify("+".$hours." hours +".$minutes . " minutes");
    //     $end_time_value = $end_time->format("H:i:s");

    //     $start_time->setTimezone(new DateTimeZone($this->user->showTimezone()));
    //     $end_time->setTimezone(new DateTimeZone($this->user->showTimezone()));

    //     return $start_time->format("H:i:s");
    // }

    // public function getUserEndTimeAttribute()
    // {
    //     $target_date = new DateTime();
    //     $target_date->modify('next '. $this->day_of_week);

    //     $seconds = ($this->hours_needed * 3600);
    //     // we're given hours, so let's get those the easy way
    //     $hours = floor($this->hours_needed);
    //     // since we've "calculated" hours, let's remove them from the seconds variable
    //     $seconds -= $hours * 3600;
    //     // calculate minutes left
    //     $minutes = floor($seconds / 60);

    //     $start_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date->format("Y-m-d"). " ".$this->start_time)), new DateTimeZone($this->client->showTimezone()));
    //     $end_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date->format("Y-m-d"). " ".$this->start_time)), new DateTimeZone($this->client->showTimezone()));
    //     $end_time->modify("+".$hours." hours +".$minutes . " minutes");
    //     $end_time_value = $end_time->format("H:i:s");

    //     $start_time->setTimezone(new DateTimeZone($this->user->showTimezone()));
    //     $end_time->setTimezone(new DateTimeZone($this->user->showTimezone()));

    //     return $end_time->format("H:i:s");
    // }

    public function DST()
    {
        $target_date = new DateTime();
        $target_date->modify('next '. $this->day_of_week);

        $seconds = ($this->hours_needed * 3600);
        // we're given hours, so let's get those the easy way
        $hours = floor($this->hours_needed);
        // since we've "calculated" hours, let's remove them from the seconds variable
        $seconds -= $hours * 3600;
        // calculate minutes left
        $minutes = floor($seconds / 60);

        $start_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date->format("Y-m-d"). " ".$this->start_time)), new DateTimeZone($this->client->showTimezone()));
        $end_time = new DateTime(date("Y-m-d h:i A",strtotime($target_date->format("Y-m-d"). " ".$this->start_time)), new DateTimeZone($this->client->showTimezone()));
        $end_time->modify("+".$hours." hours +".$minutes . " minutes");
        $end_time_value = $end_time->format("H:i:s");

        $start_time->setTimezone(new DateTimeZone($this->user->showTimezone()));
        $end_time->setTimezone(new DateTimeZone($this->user->showTimezone()));

        $user_day_of_week_end = null;
        if ($start_time->format('l')!=$end_time->format('l')) {
            $user_day_of_week_end = $end_time->format('l');
        }

        $dst_update = [
            'user_start_time' => $start_time->format("H:i:s"),
            'user_end_time' => $end_time->format("H:i:s"),
            'user_day_of_week' => $start_time->format('l'),
            'user_day_of_week_end' => $user_day_of_week_end
        ];

        return $dst_update;
    }
}