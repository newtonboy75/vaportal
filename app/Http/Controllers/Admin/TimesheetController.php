<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\Timesheet;
use App\Models\User;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Models\ActivityLog;
use App\Models\Attendance;

class TimesheetController extends Controller
{

    private $auser;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->auser = \Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        return view('admin.timesheet.index');
    }

    public function create()
    {
        $clients = User::HasRole('client')
        ->orderBy('first_name')
        ->get();

        $vas = User::HasRole('va')
        ->orderBy('first_name')
        ->get();
        return view('admin.timesheet.create', compact('clients', 'vas'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'target_date' => 'required',
            'schedule_date' => 'required',
            'client_id' => 'required',
            'user_id' => 'required',
            'billable_hours' => 'required',
            'status' => 'required'
        ]);

        $ts = Timesheet::create([
            'target_date' => $request->target_date,
            'schedule_date' => $request->schedule_date,
            'client_id' => $request->client_id,
            'user_id' => $request->user_id,
            'billable_hours' => $request->billable_hours,
            'attendance_id' => 0,
            'total_time' => "00:00:00",
            'total_task_time' => "00:00:00",
            'total_idletime' => "00:00:00",
            'total_breaktime' => "00:00:00",
            'status' => $request->status
        ]);

        $va = User::find($request->user_id);

        ActivityLog::addLog("User [{$this->auser->fullname}] created a new timesheet for [{$va->fullname}].");

        return redirect('/dashboard/timesheets/edit/' . $ts->id)->with('notification_message', 'Timesheet has been created.'); 
    } 
    
    //old edit
    public function edit($id)
    {
        $set_menu = [
            'active_menu' => "dashboard/timesheets",
            'active_path' => "dashboard/timesheets/edit/{id}",
            'active_parent' => "Tools"
        ];

        $q = Timesheet::find($id);
        
        if (empty($q)) {
            return redirect('/dashboard');
        }
        return view('admin.timesheet.edit', compact('q','set_menu'));
    }
    
    //newton
    public function edit_($id)
    {
        $set_menu = [
            'active_menu' => "dashboard/timesheets",
            'active_path' => "dashboard/timesheets/edit/{id}",
            'active_parent' => "Tools"
        ];
        
        //get attendance id
        $q1 = Timesheet::whereId($id)->first();
        $target_attendance = Attendance::whereId($q1->attendance_id)->first();
        $att = $target_attendance->getAllShifts($q1->user_id, $q1->client_id, $q1->schedule_date);
        
        //$tsheet = Timesheet::whereIn('attendance_id', $att)->sum('billable_hours');
        
        $tsheet = $q1->getTotalBillableHours($att);
        $timesheet_shifts = $q1->getAllTimesheetsByShifts($att);     
        
        $a = [
            'id' => $q1->id,
            'billable_hours' => $tsheet,
            'status' => $q1->status,
            'attendance_id' => $q1->attendance_id,
            'attendance_ids' =>$timesheet_shifts
        ];
        
        $q = (object)$a;

        if (empty($q)) {
            return redirect('/dashboard');
        }
        return view('admin.timesheet.edit', compact('q','set_menu'));
    }


    public function update(Request $request)
    {
        $q = Timesheet::find($request->id);

        $request->validate([
            'billable_hours' => 'required|numeric',
            'status' => 'required'
        ]);

        if (!empty($q)) {
            if (isset($request->target_date)) $q->target_date = $request->target_date;
            if (isset($request->schedule_date)) $q->schedule_date = $request->schedule_date;
            $q->billable_hours = $request->billable_hours;
            $q->status = $request->status;
            $q->save();
        }

        return redirect()->intended('/dashboard/timesheets/edit/'. $q->id)->with('notification_message', 'Timesheet has been updated.'); 

    }
    
         //newton
     public function update_(Request $request)
    {        
        $attendance_ids = preg_replace("/[^0-9,]/", "", $request->attendance_ids);
        $attendances = explode(',', $attendance_ids);
                
        foreach($attendances as $attendance_id){

         $q = Timesheet::find($attendance_id);

        $request->validate([
            'billable_hours' => 'required|numeric',
            'status' => 'required'
        ]);

        if (!empty($q)) {
            if (isset($request->target_date)) $q->target_date = $request->target_date;
            if (isset($request->schedule_date)) $q->schedule_date = $request->schedule_date;
            //$q->billable_hours = $request->billable_hours;
            $q->status = $request->status;
            $q->save();
        }
        }
        

        return redirect()->intended('/dashboard/timesheets/edit/'. $attendances[0])->with('notification_message', 'Timesheet has been updated.'); 

    }

    public function datatables(Request $request)
    {
        $user = \Auth::user();

        $firstDate = $request->firstDate;
        $lastDate = $request->lastDate;

        $status = "pending";
        if (isset($request->status)) {
            $status = $request->status;
        }

        $timesheet = Timesheet::where('status', $status)
        ->distinct('user_id')
        ->groupBy('user_id')
        ->whereBetween('target_date',[$firstDate, $lastDate])
        ->select('id','target_date','user_id', 'status', DB::raw("SUM(billable_hours) as total_billable_hours"));

        return DataTables::of($timesheet)
            ->removeColumn('status')
            ->editColumn('user_id', function($ts){
                return $ts->user->first_name . " " . $ts->user->last_name;
            })
            ->addColumn('actions', function($ts) use ($firstDate, $lastDate) {
                //$delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$ts->id.'"><i class="fa fa-trash"></i></a>';
                $info_btn = '<a class="btn btn-info" href="/dashboard/timesheets/info/'.$ts->user_id.'?firstDate='.$firstDate.'&lastDate='.$lastDate.'"><i class="fa fa-info"></i></a>';
                return '<div class="btn-toolbar">'. $info_btn .'</div>';
            
        })->rawColumns(['actions'])
        ->make(true);

    }

    public function info(Request $request, $user_id)
    {   
        $firstDate = null;
        $lastDate = null;
        if ($request->firstDate!='' && $request->lastDate!='') {
            $firstDate = $request->firstDate;
            $lastDate = $request->lastDate;
        }

        $set_menu = [
            'active_menu' => "dashboard/timesheets",
            'active_path' => "dashboard/timesheets/info/{id}",
            'active_parent' => "Tools"
        ];

        $user = User::find($user_id);
        return view('admin.timesheet.info', compact('user','set_menu','firstDate', 'lastDate'));
    }
    
    //old dt
    public function info_datatables(Request $request, $id)
    {
        $user = \Auth::user();

        $firstDate = $request->firstDate;
        $lastDate = $request->lastDate;

        $timesheet = Timesheet::where('user_id', $id)
        ->whereBetween('target_date',[$firstDate, $lastDate])
        ->select('id','target_date','client_id', 'total_time', 'billable_hours', 'status');

        return DataTables::of($timesheet)
            ->removeColumn('id')
            ->editColumn('client_id', function($ts){
                return $ts->client->first_name . " " . $ts->client->last_name;
            })
            ->editColumn('status', function($ts){
                return ucwords($ts->status);
            })
            ->addColumn('actions', function($ts){
                $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$ts->id.'"><i class="fa fa-trash"></i></a>';
                $status_btn = '<a data-toggle="modal" title="Approve or Decline" data-target="#modal-status" class="btn btn-success button-status" data-id="'.$ts->id.'"><i class="fa fa-check-square-o"></i></a>';
                $edit_btn = '<a class="btn btn-info" title="Edit Timesheet" href="/dashboard/timesheets/edit/'.$ts->id.'"><i class="fa fa-edit"></i></a>';
                return '<div class="btn-toolbar">'. $edit_btn  . $status_btn . $delete_btn . '</div>';
            
        })->rawColumns(['actions'])
        ->make(true);

    }
    
    //newton
    public function info_datatables_(Request $request, $id)
    {
        $user = \Auth::user();

        $firstDate = $request->firstDate;
        $lastDate = $request->lastDate;
        
        $attendance = Attendance::where('user_id', $id)
        ->whereBetween('schedule_date',[$firstDate, $lastDate])
        //->where('shift', '1st_shift')
        ->groupBy('day_of_week', 'client_id')
        ->orderBy('id', 'asc');

        return DataTables::of($attendance)
            ->removeColumn('id')
            ->removeColumn('user_id')
            ->addColumn('target_date', function($ts){
                return $ts->schedule_date;
            })
            ->editColumn('client_id', function($ts){
                return $ts->client->first_name . " " . $ts->client->last_name;
            })
            ->editColumn('billable_hours', function($ts){
                $att = Attendance::where('user_id', $ts->user_id)->where('client_id', $ts->client_id)->whereDate('schedule_date', $ts->schedule_date)->where('day_of_week', $ts->day_of_week)->orderBy('id')->pluck('id');
                $tsheet = Timesheet::whereIn('attendance_id', $att)->sum('billable_hours');
                return $tsheet;
            })
            ->editColumn('total_time', function($ts){
                $hours_ = 0;
                $tsheet = [];
                $att = Attendance::where('user_id', $ts->user_id)->where('client_id', $ts->client_id)->whereDate('schedule_date', $ts->schedule_date)->where('day_of_week', $ts->day_of_week)->orderBy('id')->pluck('id');
                $tsheets = Timesheet::whereIn('attendance_id', $att)->get();
                $secs = 0;
                $remark = '';
                
                foreach($tsheets as $ts){
                    $tsheet[] = $ts->total_task_time;
                    $remark .= '<br>'.str_replace('_', ' ', $ts->attendance->shift).': '.$ts->total_task_time;
                }
                
                return '<strong>'.$this->sumTotalTime($tsheet).'</strong>'.$remark;
            })
            ->editColumn('status', function($ts){
                $att = Attendance::where('user_id', $ts->user_id)->where('client_id', $ts->client_id)->whereDate('schedule_date', $ts->schedule_date)->where('day_of_week', $ts->day_of_week)->orderBy('id')->pluck('id');
                $tsheet = Timesheet::whereIn('attendance_id', $att)->select('status')->first();
                return ucwords($tsheet->status);
            })
            ->addColumn('actions', function($ts){
                
                $tsheet = Timesheet::where('attendance_id', $ts->id)->first();
                
                
                $delete_btn = '<a data-toggle="modal" data-target="#modal-danger" class="btn btn-danger button-delete" data-id="'.$tsheet->id.'"><i class="fa fa-trash"></i></a>';
                $status_btn = '<a data-toggle="modal" title="Approve or Decline" data-target="#modal-status" class="btn btn-success button-status" data-id="'.$tsheet->id.'"><i class="fa fa-check-square-o"></i></a>';
                $edit_btn = '<a class="btn btn-info" title="Edit Timesheet" href="/dashboard/timesheets/edit/'.$tsheet->id.'"><i class="fa fa-edit"></i></a>';
                return '<div class="btn-toolbar">'. $edit_btn  . $status_btn . $delete_btn . '</div>';
                
                //return $att;
            
        })->rawColumns(['actions', 'total_time'])
        ->make(true);

    }
    
    function sumTotalTime($times) {
      $seconds = 0;
      foreach ($times as $time)
      {
        list($hour,$minute,$second) = explode(':', $time);
        $seconds += $hour*3600;
        $seconds += $minute*60;
        $seconds += $second;
      }
      $hours = floor($seconds/3600);
      $seconds -= $hours*3600;
      $minutes  = floor($seconds/60);
      $seconds -= $minutes*60;
      if($seconds < 9)
      {
      $seconds = "0".$seconds;
      }
      if($minutes < 9)
      {
      $minutes = "0".$minutes;
      }
        if($hours < 9)
      {
      $hours = "0".$hours;
      }
      return "{$hours}:{$minutes}:{$seconds}";
    }

    public function show($id) {
        $q = Timesheet::find($id);
        $html = view('admin.timesheet.show', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }
    
    //newton
    public function shows_($id) {
        
        $q = Timesheet::find($id);
        $q->all = $q->getAllShifts($q->user_id, $q->client_id, $q->schedule_date);
        
        $html = view('admin.timesheet.show_x', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }

    public function delete($id)
    {
        $q = Timesheet::find($id);
        if (!empty($q)) {
            $q->delete();
        }
        $response['status'] = "ok";
        return json_encode($response);
    }

    public function changeStatus(Request $request, $status) 
    {
        $q = Timesheet::find($request->id);
        if ($status == 'approve') {
            $q->status =  'approved';
            $q->save();
        }

        if ($status == 'decline') {
            $q->status =  'declined';
            $q->save();
        }

        $response['status'] = $status;
        return json_encode($response);
    }
}