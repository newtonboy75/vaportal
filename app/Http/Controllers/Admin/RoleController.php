<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\Role;
use Illuminate\Validation\Rule;

class RoleController extends Controller
{
    public function index()
    {
        return view('admin.role.index');
    }


    public function create()
    {
        return view('admin.role.create_edit');
    }

    public function store(Request $request)
    {
        $request->validate([
            'slug' => 'required|max:100',
            'name' => 'required|max:100',
            'description' => 'required|max:100',
        ]);

        $role = Role::create([
            'name' => $request->name,
            'slug' => $request->slug,
            'description' => $request->description,
        ]);

        return redirect('/dashboard/roles');
    }

    public function edit($id)
    {
        $q = Role::find($id);
        if (empty($q)) {
            return redirect('/dashboard');            
        }
        return view('admin.role.create_edit', compact('q'));        
    }    

    public function update(Request $request) 
    {
        $q = Role::find($request->id);

        $request->validate([
            'name' => 'required|max:100',
            'slug' => 'required|max:100',
            'description' => 'required|max:100'
        ]);

        if (!empty($q)) {
            $q->name = $request->name;
            $q->slug = $request->slug;
            $q->description = $request->description;
            $q->save();
        }

        return redirect()->intended('/dashboard/roles/edit/'. $q->id)->with('notification_message', 'Role has been updated.'); 

    } 

    public function show($id)
    {
        $q = Role::find($id);
        $html = view('admin.role.show', compact('q'))->render();
        $response['html'] = $html;
        return json_encode($response);
    }

    public function delete($id)
    {
        $q = Role::find($id);
        if (!empty($q)) {
            $q->delete();
        }        
        $response['status'] = "ok";
        return json_encode($response);
    }


    public function datatables()
    {

        $roles = Role::select(['id','name', 'slug','description']);

        return DataTables::of($roles)
            ->addColumn('actions', function($role){
   
                $key_btn = '<a class="btn btn-primary" href="/dashboard/role-permissions/'.$role->id.'"><i class="fa fa-key"></i></a>';

                if ($role->id==1) {
                    return '';  
                } else {
                    return '<div class="btn-toolbar">'. $key_btn . '</div>';
                }

        })->rawColumns(['actions'])
        ->make(true);        

    }

}