<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamUser extends Model {

    protected $table = "team_user";

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['id', 'team_id', 'user_id'];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }
}