@extends('layouts.adminlte-master')
@section('page-title', "Messages")

@section('content')
@include('admin.delete-modal')
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Messages</h3>
                </div>

                <div class='box-body'>
                    <div class='row'>
                        <div class="col-sm-5" sytle="height: 90vh">
                            <table class="table table-hover dataTable" id="messages-table" role='grid' style="width:100%; height: 100%"></table>
                        </div>

                        <div class="col-sm-7">
                          <div id="read-message-section">
                            <div id="read-message-solo">loading... please wait...</div>
                            <div id="msg-reply-box">
                              <form id="reply-form" method="post" action="/dashboard/messages/save">
                              {{ csrf_field() }}
                              <input type="hidden" name="to_id" id="to" value="">
                              <input type="hidden" name="from_id" id="from" value="">
                              <input type="hidden" name="reply_to" id="reply_to" value="">
                              <textarea class="form-control" name="message" id="txt-message-box" placeholder="reply..."></textarea><p>&nbsp;</p>
                              <input type="submit" value="Submit">
                            </form>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<style>
#messages-table tr{
  cursor: pointer;
}

#messages-table tr .active{
  background: #f0f0f0;
  border: 1px solid blue;
}



#read-message-section{
  width: 90%;
  height: 100%;
  display: inline-block;
  border-left: 2px solid #f0f0f0;
  padding-left: 30px;
  padding-right: 30px;
  font-size: 14px !important;
}

#txt-message-box{
  width: 100%;
  height: 200px;
}

.mbox{
  display: inline-block;
  //margin-bottom: 30px !important;
}

#read-message-solo{
  height:400px;
  max-height: 400px;
  overflow: auto;
  padding:20px;
}

#msg-reply-box{
  width: 100%;
  display: inline-block;
  margin-top: 40px;
  padding: 20px;
  border-top: 1px solid #e3e3e3;
}
</style>
@endpush
@push('view-scripts')

<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>


<script>
var _table;
$(function() {
    _table = $('#messages-table').DataTable({
        processing: true,
        serverSide: true,
        "responsive": true,
        "rowId": "id",
        "initComplete": function(){

    setTimeout(function(){ $("table tr:first-child").click(); }, 3000);
  },
        ajax: '/dashboard/messages/datatables',
        "columns": [
                { "data": "id" },
                { "data": "from_id"},
                { "data": "message"},
                { "data": "updated_at"},
                //{ "data": "actions", width: "150px", orderable: false, searchable: false},
        ],
        "columnDefs": [ {
          "targets": 2,
          "data": "message",
          "render": function ( data, type, row, meta ) {
            var msg = ''
            var reply = '';
            if(data['reply'] != null){
              if(data['reply'].length >= 1){
                msg = '<div id="single-message" style="font-weight: 500; color: #000; max-width: 300px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">' + data['orig'] + '</div><br><div style="font-weight: 500"></div>';
                $('#single-message').parent('tr').addClass('font-weight-bold');

              }else{
                msg = '<div style="font-weight: 500; color: gray; max-width: 300px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"">' + data['orig'] + '</div>';
              }
            }else{
              msg = '<div id="single-message"  style="font-weight: 500; color: #000; max-width: 300px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">' + data['orig'] + '</div><br><div style="font-weight: 500"></div>';
            }

            return msg;
          }
        } ]
    });


});

//actions here
$(document).on('click', 'tr', function(){
  var id = $(this).attr('id');
  //alert(id);
  $('#messages-table tr').removeClass('active');
  $(this).addClass('active');

  $.ajax({
      url: "/dashboard/messages/info/"+id,
      method: "GET",
  }).done(function(data) {
      //$('#modal-warning').modal('hide');
      //$("#button-delete").html('Delete');
      //_table.ajax.reload( null, false );
      console.log($('#msg-infos').attr('rel'));

      $('#reply_to').val(id);


      //$('#msg-reply-box').find('#from').val(misc_infos[1]);


      $('#read-message-solo').html('');
      $('#read-message-solo').html(data);

      setTimeout(function(){

        var misc_infos = $('#msg-infos').attr('rel').split('|');

        $('#msg-reply-box').find('input#to').val(misc_infos[0]);
        $('#msg-reply-box').find('input#from').val(misc_infos[1]);

      }, 3000);



  });


});

$(document).on('click', '#send-reply', function(){
  alert('hello');
});



</script>

@endpush
