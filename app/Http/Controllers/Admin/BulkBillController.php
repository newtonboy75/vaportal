<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\User;
use App\Models\ClientBilling;
use App\Models\ActivityLog;
use App\Models\Timesheet;
use App\Models\Schedule;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use PDF;

class BulkBillController extends Controller
{
    public function index()
    {
        $users = User::HasRole('client')
        ->orderBy('first_name')
        ->get();
        return view('admin.bulkbill.index', compact('users'));
    }

    public function bulkGenerate(Request $request)
    {
        $invoice_number = $request->invoice_number;

        foreach ($request->client_ids as $client_id) {

            $timesheet = Timesheet::groupBy(['target_date', 'user_id'])
            ->where('client_id', $client_id)
            ->where('status','approved')
            //->whereBetween('target_date', [$request->start_date, $request->end_date])
            ->whereHas('attendance', function($query) use ($request) {
                $query->whereBetween('schedule_date', [$request->start_date, $request->end_date]);
            })
            ->orderBy('u.first_name')
            ->orderBy('target_date')
            ->selectRaw('*,u.first_name, sum(billable_hours) as total_billable_hours')
            ->join('users as u', 'u.id','=','user_id')
            ->get();

            $advance_payments = $request->advance_payments;
            $tax = $request->tax;
            $client = User::find($client_id);
            $start_date = $request->start_date;
            $end_date = $request->end_date;
            $setup_fee = $request->setup_fee;
            $credits = $request->credits;
            $credit_text = $request->credit_text;

            $overall_total = 0;
            foreach ($timesheet as $ts) {
                $rate = $ts->client->getRateBilling($ts->user_id);
                $total = $ts->total_billable_hours * $rate;
                $overall_total = $overall_total + $total;
            }
    
            $overall_total = $overall_total + $setup_fee;
    
            $total_tax = $overall_total * ($tax/100);
            $amount_due = ( $overall_total + $total_tax ) - $advance_payments - $credits;
    
            $status = 'unpaid';
            if ($amount_due<0) $status = 'advance';
    
            $cb = ClientBilling::create([
                'client_id' => $client_id,
                'date_generated' => date("Y-m-d",time()),
                'billing_period' => $start_date . ' - ' . $end_date,
                'invoice_number' => $invoice_number,
                'pre_payment' => $advance_payments,
                'taxes_due' => $total_tax,
                'total_billable_hours' => $overall_total,
                'total_amount_due' => $amount_due,
                'status' => $status,
                'email_status' => "unsent"
            ]);
    
            $pdf = PDF::loadView("admin.bills.pdf", compact(
                'timesheet', 
                'advance_payments', 
                'tax', 
                'client', 
                'start_date', 
                'end_date', 
                'invoice_number', 
                'setup_fee',
                'credits',
                'credit_text'
            ));
    
            $path = public_path() . "/invoices/" .$client_id;
    
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
    
            $pdf_filename = strtolower("invoice-" . $client->first_name . "-" . $client->last_name . "-" . bin2hex(random_bytes(10)) . "-" . date("Y-m-d") . ".pdf");
            
            $cb->pdf_path = "/invoices/" .$client_id . "/" . $pdf_filename;
            $cb->save();

            $inumber1 = substr($invoice_number, 0, 8);
            $inumber2 = substr($invoice_number, 8, 5);
            $inumber2 = sprintf("%05d", $inumber2 + 1);

            $invoice_number = $inumber1 . $inumber2;

            $pdf->save($path .'/'. $pdf_filename);
        }

        $response['status'] = "ok";
        return json_encode($response);
    }
}