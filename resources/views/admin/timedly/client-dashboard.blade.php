@extends('layouts.timedly.app')

@section('pageTitle')
    Dashboard
@endsection
@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endpush

@section('pageScripts')
<style type="text/css">
.pagination{ margin-bottom: 20px !important; }
    #prev-slide{
        position: absolute;
        top: 40%;
        left: 0;
        cursor: pointer;
        margin-left:30px;
    }

    #next-slide{
        position: absolute;
        top: 40%;
        right: 0;
        cursor: pointer;
        margin-right: 30px;
    }

    .previc{
        width: 30px !important;
        height: 30px !important;
    }
</style>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="//momentjs.com/downloads/moment.min.js"></script>
    <script src="//momentjs.com/downloads/moment-timezone-with-data.js"></script>
    <script src="{{ asset('js/notify.min.js') }}"></script>
    <script>
    var screenshots_table;
    var online_table;
    var audio = document.getElementById("notif");
    var load_ss = true;

    //refresh page every minute
    window.setInterval(function(){
      //screenshots_table.ajax.reload();
        //if(load_ss == true){
        if (!$('#screenshot').is(':visible')) {
          loadScreenshots();
          online_table.ajax.reload();
          loadVaTaskLatest();
        }

    }, 50000);

    function loadOnlineVA() {
        online_table = $('#online-table').DataTable({
          processing: false,
          serverSide: true,
          language: {
          paginate: {
          previous: '<strong><span class="prev-icon">&lt;</span></strong>',
          next: '<strong><span class="next-icon">&gt;</span></strong>'
          }
          },
          ajax: '/dashboard/va/online_dashboard_datatables_client',
          "columns": [
            { "data": "date_in" },
            { "data": "time_in" },
            { "data": "user_id" },
            { "data": "status" },
          ]
        });
      }

      function loadVaTaskLatest(){
           $.ajax({
            url: "/dashboard/get-vatask-clients-all",
            dataType: "json"
        }).done(function(data) {
            //console.log(data);
            var html = '';
            $.each(data, function(index, value){

              var client_tz = '{{ Auth::user()->timezone }}';

              var timezones = [];
              timezones['EST'] = 'America/New_York';
              timezones['PST'] = 'America/Los_Angeles';
              timezones['CST'] = 'America/Chicago';
              timezones['MST'] = 'America/Denver';
              timezones['MST-Phoenix'] = 'America/Phoenix';
              timezones['MNL'] = ' Asia/Manila';

              var client_side = moment.tz("Asia/Manila");
              var user_side = client_side.clone().tz(timezones[client_tz]).format('YYYY-MM-DD');
              var cs_time_s = moment(value.time_start, 'h:mm:ss a').tz(timezones[client_tz]).format('hh:mm:ss A');
              var cs_time_e = moment(value.time_end, 'h:mm:ss a').tz(timezones[client_tz]).format('hh:mm:ss A');

              html += '<tr><td scope="row">'+user_side+'</td><td>'+value.user.first_name+' '+value.user.last_name+'</td><td>'+value.name+'</td><td>'+cs_time_s+'</td><td>'+cs_time_e+'</td></tr>';

            });

            $('.list-task').html(html);

        });
        }

    function loadLatestScreenshotCounts(){
         $.ajax({
            url: "/dashboard/get-sc-clients-all",
            dataType: "json"
        }).done(function(data) {
            $('#sc-client-cnt').html(data);
        });
        }

    function loadScreenshots() {
      $.ajax({
        url: "/get-all-sc-clients",
        dataType: "json"
      }).done(function(data) {
        $('.dashboard__tracker-activities .row').html('');
        var html = '';
        var dd_format = '';
        var dt_format = '';
        var db_date = '';
        var user = '';
        var client_tz = '{{ Auth::user()->timezone }}';
        var timezones = [];
        timezones['EST'] = 'America/New_York';
        timezones['PST'] = 'America/Los_Angeles';
        timezones['CST'] = 'America/Chicago';
        timezones['MST'] = 'America/Denver';
        timezones['MST-Phoenix'] = 'America/Phoenix';
        timezones['MNL'] = ' Asia/Manila';

        $.each(data, function(k, v){

          if(v.filename != undefined){
            const FORMAT_DATE = "MMM DD, YYYY";
            const FORMAT_TIME = "hh:mm:ss A";

            var dte = moment(v.created_at);
            dt_format = dte.tz(timezones[client_tz], false).format(FORMAT_TIME);
            dd_format = dte.tz(timezones[client_tz], false).format(FORMAT_DATE);

            user = v.user.first_name + ' ' + v.user.last_name;
            //Taken on: ' + dd_format + ' at ' + dt_format + ' ' + user + '

            html += '<div class="col-lg-6 col-12 thumb-ss" id="'+k+'" style="margin-bottom: 10px;"><img style="width: 200px !important; height: 160px !important; cursor: pointer;" src="' + v.thumbnail_url + '" data-id="' +v.id+ '" alt="" class="button-preview img-responsive img-thumbnail" data-toggle="modal" data-target="#screenshot" style="cursor:pointer;""><div class="tracker-summary"><div style="font-weight: 600;" class="user">'+ user +' </div><div style="text-transform: uppercase;"><small class="timestamp">'+dd_format + ' ' + dt_format +'</small></div></div></div><div class="modal fade" id="screenshot"><div class="modal-dialog modal-lg" role="dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div class="content"><i class="fa fa-spin fa-refresh"></i></div></div></div></div></div>';
          }
        });

        $('.dashboard__tracker-activities .row').html(html);

      });
    }

    (function ($) {
      function checkNotif() {
        $.ajax({
          url: "/dashboard/notifications/check_notif",
          dataType: "json",
        }).done(function(data) {

           // console.log(data)

          if (!jQuery.isEmptyObject(data) ) {
            for(var k in data) {
              if (data[k]['type']=='time-in' || data[k]['type']=='time-out')
              $.notify(data[k]['content'], {autoHideDelay:10000,className:'info'});

              if (data[k]['type']=='idle-start' || data[k]['type']=='idle-stop') {
                audio.play();
                $.notify(data[k]['content'], {autoHideDelay:10000,className:'error'});
              }

               if (data[k]['type']=='break-exceed') {
                audio.play();
                $.notify(data[k]['content'], {autoHideDelay:10000,className:'error'});
              }

              if (data[k]['type']=='break-start' || data[k]['type']=='break-stop') {
                audio.play();
                $.notify(data[k]['content'], {autoHideDelay:10000,className:'warn'});
              }
            }
          }
        });
      }

      $(document).ready(function() {
        var curpos = '';

        loadLatestScreenshotCounts();
        loadScreenshots();
        loadOnlineVA();
        loadVaTaskLatest();

        $('#screenshot').on('hide.bs.modal', function(){
        load_ss = true;
        //console.log('close');
        });

        //this shows the preview modal
        $(document).on("click", ".button-preview", function(){
        var id = $(this).attr('data-id');
        curpos = $(this).parent('div').attr('id');
        $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');

        load_ss = false;

        $.ajax({
            url: "/dashboard/screenshots/preview/"+id,
            dataType: "json"
        }).done(function(data) {
            $('.modal-body .content').html(data.html);
            $('.modal-body .content').append('<div id="next-slide"><a href="#" class="prev-next" rel="next" id="get-next"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-right-circle-fill" viewBox="0 0 16 16"><path style="width: 30px !important;" d="M8 0a8 8 0 1 1 0 16A8 8 0 0 1 8 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z"/></svg></a></div>');
            //$('.modal-body .content').append('<div id="prev-slide"><a href="#" class="prev-next" rel="prev" id="get-prev"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16"><path style="width: 30px !important;" d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"/></svg></div>');
        });


         $(document).on("click", ".prev-next", function(){

           var dimg = $('.modal-body .content').find('div .img-responsive').attr('src');
           var position = $(this).attr('rel');
           var pos_id = '';
            $('.modal-body .content').html("");


            $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');

            var new_pos = 0;
            var new_target = '';
            var new_name = '';
            var new_date = '';
            var new_img = '';

            $('.modal-body .content').html('<i class="fa fa-spin fa-refresh"></i>');

            pos_id = curpos;

            if(position == 'next'){
                if(new_pos <= 19){
                    new_pos = Number(pos_id) + 1;
                    curpos = Number(curpos) + 1;
                    new_target = $('#'+ new_pos).find('img');
                }

            }else{
                if(new_pos >= 0){
                    new_pos = Number(pos_id) - 1;
                    new_target = $('#'+ new_pos).find('img');
                    curpos = Number(curpos) - 1;
                }

            }

            new_name = $('#'+new_pos).find('.user').text();
            new_date = $('#'+new_pos).find('.timestamp').text();

            new_img = new_target.attr('src').replace('thumb_', '');

            var client_tz = '{{ Auth::user()->timezone }}';
            var timezones = [];
            timezones['EST'] = 'America/New_York';
            timezones['PST'] = 'America/Los_Angeles';
            timezones['CST'] = 'America/Chicago';
            timezones['MST'] = 'America/Denver';
            timezones['MST-Phoenix'] = 'America/Phoenix';
            timezones['MNL'] = ' Asia/Manila';

            var client_side = moment.tz("Asia/Manila");
            var user_side = client_side.clone().tz(timezones[client_tz]).format('YYYY-MM-DD');

            $('.modal-body .content').html('<div><img style="width: 742px; height: 417px;" class="img-responsive img-thumbnail" src="'+new_img+'"></div><br><strong>'+new_name+'</strong>&nbsp;&nbsp;<a href="'+new_img+'"><small>view all screenshots</small></a><br><small>'+new_date+'</small></div>');

            if(new_pos < 19){
                 $('.modal-body .content').append('<div id="next-slide"><a href="#" class="prev-next" rel="next" id="get-next"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-right-circle-fill" viewBox="0 0 16 16"><path style="width: 30px !important;" d="M8 0a8 8 0 1 1 0 16A8 8 0 0 1 8 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z"/></svg></a></div>');
            }

            if(new_pos > 0){
                 $('.modal-body .content').append('<div id="prev-slide"><a href="#" class="prev-next" rel="prev" id="get-prev"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16"><path style="width: 30px !important;" d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"/></svg></div>');
            }

        });

    });
});

    }) (jQuery);
    </script>

@endsection

@section('content')
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid" style="display: none;">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Dashboard</h6>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-4 col-md-6">
                        <div class="card card-stats">
                            <!-- Card body -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">My Virtual Assistants</h5>
                                        <span class="h2 font-weight-bold mb-0">{{$va_count}}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                            <i class="fas fa-users"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-sm">
                                    <a href="/dashboard/va">View all</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="card card-stats">
                            <!-- Card body -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Total Working Hours</h5>
                                        <span class="h2 font-weight-bold mb-0">{{$total_time}}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                            <i class="far fa-clock"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-sm">
                                    &nbsp;
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="card card-stats">
                            <!-- Card body -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-uppercase text-muted mb-0">Total Screenshots</h5>
                                        <span class="h2 font-weight-bold mb-0" id="sc-client-cnts">{{$screenshot_count}}</span>
                                    </div>
                                    <div class="col-auto">
                                        <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                            <i class="ni ni-tv-2"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="mt-3 mb-0 text-sm">
                                    <a href="/dashboard/screenshots">View all screenshots</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col-xl-8">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Latest Virtual Assistant Attendance</h3>
                            </div>
                            <div class="col text-right">
                                <a href="/dashboard/attendance" class="btn btn-sm btn-primary">See all</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <div class="table-responsive">
                            <table class="va-online-table table table-hover dataTable" id="online-table" role='grid'>
                            <thead>
                                <tr>
                                    <th>Date In</th>
                                    <th>Time In</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                        </table>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Virtual Assistants Logs</h3>
                            </div>
                            <div class="col text-right">
                                {{-- <a href="attendance.html" class="btn btn-sm btn-primary">See all</a> --}}
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush" id="va-task">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col" data-sort="date_start">Date</th>
                                    <th scope="col" data-sort="budget">VA</th>
                                    <th scope="col" data-sort="status">Task Name</th>
                                    <th scope="col" data-sort="status">Time Started</th>
                                    <th scope="col" data-sort="status">Time Ended</th>
                                </tr>
                                </thead>
                                <tbody class="list-task">
                                    @if (count($vatask_latest) >= 1)
                                        <tr>
                                            <td colspan="5">No items to display</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table><br>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4">
            <div class="card">
                    <!-- Card header -->

                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <!-- Title -->
                                <h5 class="h3 mb-0">Stats</h5>
                            </div>
                        </div>
                    </div>
                    <!-- Card body -->

                    <div class="card-body">
                        <!-- List group -->
                        <div class="row">
                            <div class="col-sm-4 text-center">
                            <h6>My Virtual Assistants</h6>
                                <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                            <i class="fas fa-users"></i>
                                </div>
                                <div><span class="h2 font-weight-bold mb-0">{{$va_count}}</span></div>
                            </div>

                            <div class="col-sm-4 text-center">
                            <h6>Total Working Hours</h6>
                                <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                            <i class="far fa-clock"></i>
                                        </div>
                                <div><span class="h2 font-weight-bold mb-0">{{$total_time}}</span></div>
                            </div>
                            <div class="col-sm-4 text-center">
                            <h6>Total Screenshots</h6>
                                <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                            <i class="ni ni-tv-2"></i>
                                        </div>
                                <div><span class="h2 font-weight-bold mb-0" id="sc-client-cnt">{{$screenshot_count}}</span></div>
                            </div>

                    </div>
                </div>
             </div>

                <div class="card">
                    <!-- Card header -->

                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <!-- Title -->
                                <h6 class="text-uppercase text-muted ls-1 mb-1">{{count($projects)}}/15 Projects</h6>
                                <h5 class="h3 mb-0">Progress track</h5>
                            </div>
                        </div>
                    </div>
                    <!-- Card body -->

                    <div class="card-body">
                        <!-- List group -->
                        <ul class="list-group list-group-flush list my--3">

                            @if (!empty($projects))

                                @foreach($projects as $p)

                                @php
                                    $percent = $p->getPercentDone();
                                @endphp

                                <li class="list-group-item px-0">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <h5><a href="task-board.html">{{$p->name}}</a></h5>
                                        <div class="progress progress-xs mb-0">
                                            <div class="progress-bar bg-gradient-info" role="progressbar" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percent}}%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <a href='/dashboard/taskboard/{{$p->id}}' class="btn btn-sm btn-primary">View</a>
                                    </div>
                                </div>
                                </li>

                                @endforeach

                            @endif

                        </ul>
                    </div>
                </div>

                <div class="card">
                    <!-- Card header -->
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <!-- Title -->
                                <h5 class="h3 mb-0">Tracker Activities</h5>
                            </div>
                        </div>
                    </div>
                    <!-- Card body -->
                    <div class="card-body dashboard__tracker-activities">
                        <div class="row">
                            @if (count($screenshot_latest))
                                @foreach ($screenshot_latest as $slist)
                                    <div class="col-lg-6 col-12 thumbs-ss">
                                        <img src="{{$slist->thumbnail_url}}" data-id='{{$slist->id}}' alt="" class="button-preview img-responsive img-thumbnail" data-toggle="modal" data-target="#screenshot" style='cursor:pointer;'>
                                        <button type="button" data-id='{{$slist->id}}' class="btn button-preview btn-warning btn-sm btn-round pull-right" data-toggle="modal" data-target="#screenshot">View</button>
                                        <div class="tracker-summary">
                                        @php
                                            $created_at = $user->convertToUserTimezone($slist->created_at);
                                        @endphp
                                        <p>Taken on: {{$created_at->format('M d, Y')}} at {{$created_at->format('h:i:s A')}} <strong>{{$slist->user->first_name}} {{$slist->user->last_name}}</strong></p>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="screenshot">
                                        <div class="modal-dialog modal-lg" role="dialog">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="content">
                                                <i class="fa fa-spin fa-refresh"></i>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
