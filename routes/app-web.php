<?php

Route::group(array(
    'middleware' => ['web', 'admin.auth'],
    ), function() {	

        Route::get('/dashboard/app-web', [
            'uses' => 'Admin\AppWebController@index'
        ]);

        Route::get('/dashboard/app-web/datatables', [
            'uses' => 'Admin\AppWebController@datatables'
        ]);

        Route::get('/dashboard/app-web/preview/app/{id}', [
            'uses' => 'Admin\AppWebController@previewApp'
        ]);

        Route::get('/dashboard/app-web/preview/web/{id}', [
            'uses' => 'Admin\AppWebController@previewWeb'
        ]);
});	