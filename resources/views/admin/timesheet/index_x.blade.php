@extends('layouts.adminlte-master')
@section('page-title', "Timesheets")

@section('content')
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Timesheets</h3>
                    <span class="pull-right"><a href="/dashboard/timesheets/create" class="btn btn-primary"><i class="fa fa-plus"></i> Create Timesheet</a></span>
                </div>

                <div class='box-body'>
                    <div class='row'>
                        <div class='col-md-6'>
                            <label>Select Week</label>
                            <div class="input-group date" id="datepicker">
                            <input type="text" id='target_date' name='target_date' class="form-control input-lg" value="">
                            <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                            </div>
                            </div>

                        </div>
                        <div class='col-md-6'>
                            <label>Status</label>
                            <select class="form-control input-lg" id="status" name="status">
                                <option value="pending">Pending</option>
                                <option value="approved">Approved</option>
                                <option value="declined">Declined</option>
                            </select>
                        </div>
                    </div>
                    <div class='row'><hr></div>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <table class="table table-bordered table-hover dataTable" id="timesheet-table" role='grid' width='100%'>
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>VA</th>
                                        <th>Total Billable Hours</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('view-styles')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

<style>
.datepicker tr:hover {
background-color: #808080;
}
</style>
@endpush

@push('view-scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script>
var _table;

$(function() {

    var value = "{{date('Y-m-d', strtotime(now()))}}";
    var firstDate = moment(value).isoWeekday(1).day(1).format("YYYY-MM-DD");
    var lastDate =  moment(value).isoWeekday(1).day(7).format("YYYY-MM-DD");
    $("#target_date").val(firstDate + " - " + lastDate);

    fetchData(firstDate, lastDate, 'pending');

    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        forceParse: false,
        weekStart: 1
    }).on('changeDate', function(e) {
        target_date = $("#target_date").val();
        status = $('#status').find(":selected").val();
        
        value = e.date;
        firstDate = moment(value).isoWeekday(1).day(1).format("YYYY-MM-DD");
        lastDate =  moment(value).isoWeekday(1).day(7).format("YYYY-MM-DD");
        $("#target_date").val(firstDate + " - " + lastDate);

        fetchData(firstDate, lastDate, status);
    });

    $("#status").change(function(){
        status = $('#status').find(":selected").val();
        fetchData(firstDate, lastDate, status);
    });
});

function fetchData(firstDate, lastDate, status) {
    $('#timesheet-table').DataTable().destroy();

    _table = $('#timesheet-table').DataTable({
        processing: true,
        serverSide: false,
        ajax: { 
            url:'/dashboard/timesheets/datatables',
            data: {
                firstDate : firstDate,
                lastDate : lastDate,
                status: status,
                type : 'GET'
            }
        },
        "columns": [
                { "data": "target_date",  width: "30%" },
                { "data": "user_id",  width: "40%" },
                { "data": "total_billable_hours", width: "20%", orderable: false, searchable: false, width: "20%"},
                { "data": "actions", width: "10%", orderable: false, searchable: false},
        ]
    });
}

</script>

@endpush