<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Screenshot;
use Carbon\Carbon;

class ScreenshotPathChanger extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'screenshot:modify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Modify yesterday\'s screenshot path from local to s3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $date_start = '2021-03-01';
      //$today = date('Y-m-d h:i:s');
      $date_end = '2021-05-30';
      //$screenshots = Screenshot::whereBetween('created_at', [$date_start." 00:00:00",$date_end." 08:59:59"])->update(['storage' => 's3']);
      //exec('sudo find /var/www/html/virtudesk-portal-dashboard/public/screenshots/ -name "*.jpg" -type f -mtime +5 -exec rm -f {} \;');
      //sudo find /var/www/html/virtudesk-portal-dashboard/public/screenshots/ -name "*.jpg" -type f -mtime +5 -exec rm -f {} \;'
      
      //exec('aws s3 mv /var/www/html/virtudesk-portal-dashboard/public/screenshots/90/thumb_16107729941828959900.jpg  s3://virtudesk-crm/screenshots/90/thumb_16107729941828959900.jpg');
    }
}