@extends('layouts.adminlte-master')
@section('page-title', "Profile")

@section('content')
<div class="panel panel-default">

    <div class="panel-heading">
        <i class="fa fa-user-circle-o fa-minus"></i> Profile of {{$user->fullname}}
    </div>

    <div class="panel-body">

        <div class="col-md-12">

            <div class="row">

                <div class="box box-info- mx-auto" style="width: 70% !important; margin-left: auto; margin-right: auto;">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-2 p-5">
                                <div class="form-group text-center">
                                    <img id='user-avatar-profile' src="{{$avatar}}" class="img-circle img-responsive center-block" alt="User Image"><br>
                                    <p style="font-size: 18px;"><strong>{{$user->fullname}} 
                                     @if(isset($user->name_suffix))
                                        @if($user->name_suffix != 'N/A')
                                            $user->name_suffix 
                                        @endif
                                     @endif
                                    </strong></p>
                                    <p>{{$user->timezone}}</p>
                                    <p>&nbsp;</p>
                                    <p><a style="width: 90%;" class="btn btn-primary btn-sm" target="_blank" href="{{$user->facebook ?? '#'}}">Facebook Profile</a></p>
                                    <p><a style="background-color: #3ec4ff; color: #fff; width: 90%;" class="btn btn-secondary btn-sm" href="skype:{{$user->skype_id}}?chat">Skype</a></p>
                                </div>
                            </div>
                            
                            <div class="col-sm-10">
                                <div style="padding-left: 30px; border-left: 1px solid #f0f0f0;">
                                <p>
                                <small><strong>FULL NAME</strong></small><br>
                                    {{$user->fullname}} {{$user->name_suffix ?? ''}}
                                </p>
                                
                                <p>
                                <small><strong>EMAIL</strong></small><br>
                                    {{$user->email}}
                                </p>
                                <p>   
                                    <small><strong>PERSONAL EMAIL</strong></small><br>
                                    {{$user->personal_email ?? 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>ADDRESS</strong></small><br>
                                    {{$user->address1 ?? 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>LOCATION</strong></small><br>
                                    {{ucwords($user->location) ?? 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>ZIPCODE</strong></small><br>
                                    {{$user->zip ?? 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>MOBILE NUMBER</strong></small><br>
                                    {{$user->mobile_number ?? 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>ALTERNATE CONTACT NUMBER</strong></small><br>
                                    {{$user->aleternate_contact_number ?? 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>LANDLINE NUMBER</strong></small><br>
                                    {{$user->landline ?? 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>FACEBOOK ACCOUNT</strong></small><br>
                                    {{$user->facebook ?? 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>SKYPE</strong></small><br>
                                    {{$user->skype_id ?? 'N/A'}}
                                </p>
                                
                                <hr>
                                <p>   
                                    <small><strong>DATE OF BIRTH</strong></small><br>
                                    {{$user->birthdate ? date('F j, Y',  strtotime($user->birthdate)) : 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>GENDER</strong></small><br>
                                    {{ucwords($user->gender) ?? 'N/A'}}
                                </p>
                                
                                <p>   
                                    <small><strong>CIVIL STATUS</strong></small><br>
                                    {{ ucwords($user->civil_status) ?? 'N/A'}}
                                </p>
                                
                                <hr>
                                
                                <p> 
                                    <small><strong>CONTACT PERSON (in case of emergency)</strong></small><br>
                                    {{ ucwords($user->contact_person_name) ?? 'N/A'}}
                                </p>
                                
                                <p> 
                                    <small><strong>CONTACT #</strong></small><br>
                                    {{ ucwords($user->contact_person_number) ?? 'N/A'}}
                                </p>
                                
                                <p> 
                                    <small><strong>RELATIONSHIP</strong></small><br>
                                    {{ ucwords($user->contact_person_relationship) ?? 'N/A'}}
                                </p>
                                
                                
                                
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>

            </div>

<!--
            <div class="row">

                <div class="box box-info">

                    <div class="box-body">

                        <div class="form-group">
                            <label>Total Hours Rendered</label>
                            <div>{{$user->profile->total_hours_rendered}}</div>
                        </div>

                        <div class="form-group">
                            <label>Total Lates</label>
                            <div>{{$user->profile->total_lates}}</div>
                        </div>

                        <div class="form-group">
                            <label>Total Overbreaks</label>
                            <div>{{$user->profile->total_overbreaks}}</div>
                        </div>

                        <div class="form-group">
                            <label>Total Idles</label>
                            <div>{{$user->profile->total_idle}}</div>
                        </div>

                    </div>

                </div>

            </div>
-->

        </div>

    </div>

</div>
@endsection