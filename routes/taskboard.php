<?php

//TODO: update middleware for permissions

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {	

        Route::get('/dashboard/taskboard', [
            'uses' => 'Admin\TaskboardController@index'
        ]);

        Route::get('/dashboard/taskboard/change-status', [
            'uses' => 'Admin\TaskboardController@changeStatus'
        ]);

        Route::get('/dashboard/taskboard/{id}', [
            'uses' => 'Admin\TaskboardController@index',
            'selected_parent_path' => 'dashboard/client-tasks',
            'selected_nav_path' => 'dashboard/taskboard',
        ])->where('id', '[0-9]+');   
});