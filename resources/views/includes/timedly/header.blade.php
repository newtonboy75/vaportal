<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Virtual Assistant Management Platform">
    <meta name="author" content="Creative Tim">
    <title>@yield('pageTitle') - timedly</title>
    <!-- Favicon -->
    <link rel="icon" href="{{ asset('images/timedly/brand/favicon.png') }}" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/timedly/main.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/timedly/argon.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/timedly/custom.css') }}" type="text/css">
    
    <!-- Google Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,400;0,500;0,600;0,700;1,400&display=swap" rel="stylesheet">
    
    <style>
    .bg-gradient-primary{
        margin-top: -250px;
        background-color: rgba(0, 0, 0, 0.35) !important;
        background-image: linear-gradient(225deg, rgba(0, 0, 0, 0.86) 0%, rgba(234, 110, 48, 0.8) 100%), url(https://myvirtudesk.ph/wp-content/uploads/2021/05/Team-3-1.jpg) !important;
        background-position: center center;
    background-repeat: no-repeat;
    border-width: 0px;
    border-color: rgb(226, 226, 226);
    border-style: solid;
    background-size: cover;
    height: calc(75vh - 0px);
    }
    
    .separator-skew{
        display:none !important;
    }
    
    .bg-default {
    /* background-color: #172b4d !important; */
    background: url('https://myvirtudesk.ph/wp-content/uploads/2021/04/4907156.jpg');
    }

    .bg-gradient-info_ {
        background: linear-gradient(87deg, #525f7f 0, #32325d 100%) !important;
    }
    
    .bg-gradient-info {background: linear-gradient(87deg, #fb6340 0, #ff8339eb 100%) !important;}
    body, table, p{
        font-family: 'Rubik', sans-serif !important;
        font-size: 15px !important;
    }
    
    #prev-slide{
        position: absolute;
        top: 50%;
        left: 0;
        cursor: pointer;
        margin-left:30px;
    }
    
    #next-slide{
        position: absolute;
        top: 50%;
        right: 0;
        cursor: pointer;
        margin-right: 30px;
    }
    </style>
    
    <script src="{{ asset('js/jquery.min.js') }}"></script>

{{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>--}}

    {{-- @if($currentPage == 'allReport') --}}
        <!-- select date range -->
        <script>
            $(document).ready(function(){
                $('input[id="customRadioInline4"]').click(function(){
                    $(".date-range").show();
                });
                $('input[id="customRadioInline1"]').click(function(){
                    $(".date-range").hide();
                });
                $('').click(function(){
                    $(".date-range").hide();
                });
                $('input[id="customRadioInline3"]').click(function(){
                    $(".date-range").hide();
                });
            });
        </script>
    {{-- @endif --}}
</head>
