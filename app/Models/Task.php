<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {

    protected $table = "tasks";
    public $timestamps = false;

    /**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'description'];

}