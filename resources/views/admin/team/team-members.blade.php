<div class='content'>
    <table class="table table-hover" id="members-table">
        <thead> 
            <tr> 
                <th>id</th>
                <th>VA Name</th>
                <th>Action</th>      
            </tr>
        </thead>
        <tbody>
        	@if($q->users()) 
		   		@foreach($q->users as $user) 
		   		
			   	<tr data-user-id="{{ $user->id }}">
	        		<th scope="row">{{ $user->id }}</th>
	        		<td>{{ $user->first_name }} {{ $user->last_name }}</td>
	        		@if( $q->lead_user_id != $user->id )
	        		<td>&nbsp;<a class="btn btn-danger button-remove-member" href="javascript:" data-team-id="{{ $q->id }}" data-user-id="{{ $user->id }}" data-name="{{ $user->first_name }} {{ $user->last_name }}" data-team="{{ $q->name }}" title="Remove member"><i class="fa fa-fw fa-trash"></i></a></td>
	        		@else 
	        		<td>Leader</td>
	        		@endif
	        	</tr>
		   		@endforeach
		   	@else 
			   	<tr>
	        		<td colspan="3">No Team Members</td>
	        	</tr>
		   	@endif
        	
        </tbody>
    </table>
   
</div>