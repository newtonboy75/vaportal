<?php

//TODO: update middleware for permissions

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {	

        Route::get('/dashboard/invoice', [
            'uses' => 'Admin\ClientBillingController@index'
        ]);

        Route::get('/dashboard/client-billing/datatables', [
            'uses' => 'Admin\ClientBillingController@datatables'
        ]);

        Route::get('/dashboard/client-billing/show/{id}', [
            'uses' => 'Admin\ClientBillingController@show'
        ])->where('id', '[0-9]+');

        Route::get('/dashboard/client-billing/delete/{id}', [
            'uses' => 'Admin\ClientBillingController@delete'
        ])->where('id', '[0-9]+');

        Route::get('/dashboard/client-billing/delete-multiple', [
            'uses' => 'Admin\ClientBillingController@deleteMultiple'
        ]);

        Route::post('/dashboard/invoice/update', [
            'uses' => 'Admin\ClientBillingController@update'
        ]);

        Route::get('/dashboard/invoice/edit/{id}', [
            'uses' => 'Admin\ClientBillingController@edit',
            'selected_nav_path' => 'dashboard/invoice/edit',
            'selected_parent_path' => 'dashboard/invoice'
        ])->where('id', '[0-9]+');

});