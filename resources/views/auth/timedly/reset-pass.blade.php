@extends('layouts.timedly.login')

@section('pageTitle')
    VA Time Tracker | Create new password
@endsection

@section('pageScripts')
<script>
    var updated = false;
    $(document).ready(function () {
        @php
            if($errors->any()):
                $message = implode('<br/>', $errors->all());
                $type = 'error';
                $title = 'Something went wrong.';
        @endphp

        swal({
            title: '{{$title}}',
            html: '{!!$message!!}',
            type: '{{$type}}',
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary'
        });

        @php
            endif;
        @endphp
    });
</script>
@endsection

@section('content')
    <!-- Page content -->
    <div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-7">
                <div class="card bg-secondary shadow border-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        @if( isset($status) && $status == 'is-success' )
                        <div class="text-center text-muted mb-4">
                            <p>
                                {{ $result_message }}
                            </p>
                            <small><a href="{{ url('auth/login') }}" class="btn btn-success">Go to Login</a></small>
                        </div>
                        @else
                            <div class="text-center text-muted mb-4">
                                <small>Create your new password</small>
                            </div>
                            <form method="POST" action="{{url('/auth/reset-pass')}}">
                                {{ csrf_field() }}
                                @if (isset($pr))
                                    <input type="hidden" value="{{$pr->token}}" name="token" />
                                @endif
                                <div class="form-group mb-3">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Password" name="password" type="password" required="required">
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Password Confirmation" name="confirm_password" type="password" required="required">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary my-4">Set New Password</button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
