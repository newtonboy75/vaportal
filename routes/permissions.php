<?php

Route::group(array(
	'middleware' => ['web', 'admin.auth'],
	), function() {	

        Route::get('/dashboard/permissions', [
            'middleware' => 'check.permission:list_permission',
            'uses' => 'Admin\PermissionController@index'
        ]);

        Route::get('/dashboard/permissions/datatables', [
            'middleware' => 'check.permission:list_permission',
            'uses' => 'Admin\PermissionController@datatables'
        ]);

        Route::get('/dashboard/permissions/show/{id}', [
            'middleware' => 'check.permission:list_permission',
            'uses' => 'Admin\PermissionController@show'
        ])->where('id', '[0-9]+'); 

        /*Route::get('/dashboard/permissions/create', [
            'middleware' => 'check.permission:add_permission',
            'uses' => 'Admin\PermissionController@create'
        ]);        

        Route::post('/dashboard/permissions/create', [
            'middleware' => 'check.permission:add_permission',
            'uses' => 'Admin\PermissionController@store'
        ]);  

        Route::post('/dashboard/permissions/update', [
            'middleware' => 'check.permission:edit_permission',
            'uses' => 'Admin\PermissionController@update'
        ]);

        Route::get('/dashboard/permissions/edit/{id}', [
            'middleware' => 'check.permission:edit_permission',
            'uses' => 'Admin\PermissionController@edit',
            'selected_parent_path' => 'dashboard/permissions',
            'selected_nav_path' => 'dashboard/permissions/edit',
        ])->where('id', '[0-9]+');         

        Route::get('/dashboard/permissions/delete/{id}', [
            'middleware' => 'check.permission:delete_permission',
            'uses' => 'Admin\PermissionController@delete'
        ])->where('id', '[0-9]+');*/

});	