<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class UserAvatarsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $s3 = Storage::disk('s3');
        $cloudPath = rtrim(config('filesystems.disks.s3.upload_dir'), '/') . '/avatars';

        \App\Models\User::chunk(100, function ($users) use ($s3, $cloudPath) {
            foreach ($users as $user) {
                if (empty($user->avatar)) continue;
                if (substr($user->avatar, 0, 5) == 'https') continue;
                $filePath = public_path('uploads/avatars/' . $user->avatar);
                if (!file_exists($filePath)) continue;
                $finalCloudPath = $cloudPath . '/' . $user->avatar;
                $res = $s3->put($finalCloudPath, file_get_contents($filePath));
                if (!$res) {
                    throw new \Exception('Could not upload');
                }
                $fileUrl =  $s3->url($finalCloudPath);
                $user->avatar = $fileUrl;
                $user->save();
            }
        });
    }
}
