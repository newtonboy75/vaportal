<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimeInOut extends Model {

	protected $table = "time_in_out";

	/**
     * Fillable property.
     *
     * @var array
     */
    protected $fillable = ['id', 'attendance_id', 'user_id', 'time_in', 'time_end'];

    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

    public function attendance()
    {
        return $this->belongsTo("App\Models\Attendance", "attendance_id");
    }

}