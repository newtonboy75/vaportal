<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\BrowserHistory;
use App\Models\ProcessList;
use App\Models\Screenshot;

class AppWebController extends Controller
{
    public function index()
    {
        return view('admin.appweb.index');
    }

    public function datatables(Request $request)
    {
        $target_date = date("Y-m-d", strtotime(now()));
        if (isset($request->target_date)) {
            $target_date = $request->target_date;
        }

            $ss = Screenshot::whereDate('created_at', $target_date)
            ->select('id', 'user_id', 'process_list_id', 'browser_history_id', 'created_at');
        
            return DataTables::of($ss)
            ->removeColumn('id')
            ->removeColumn('created_at')
            ->editColumn('process_list_id', function($s){
                return '<a title="View Process List" data-type="app" data-toggle="modal" data-target="#modal-preview" class="btn btn-info button-preview" data-id="'.$s->process_list_id.'"><i class="fa fa-eye"></i></a>';
            })
            ->editColumn('browser_history_id', function($s){
                return '<a title="View Brower History" data-type="web" data-toggle="modal" data-target="#modal-preview" class="btn btn-info button-preview" data-id="'.$s->browser_history_id.'"><i class="fa fa-eye"></i></a>';
            })
            ->editColumn('user_id', function($s){
                return $s->user->first_name . " " . $s->user->last_name;
            })
            ->addColumn('date', function($s){
                return date("Y m d", strtotime($s->created_at));
            })
            ->addColumn('time', function($s){
                return date("H:i:sA", strtotime($s->created_at));
            })
            ->rawColumns(['process_list_id','browser_history_id'])
            ->make(true);  
        



    }

    public function previewApp($id) 
    {
        $q = Screenshot::where("id", $id)->first();
        $html = "";
        foreach ($q->processList->list() as $ps) {
            $html .= "<div>" . $ps . "</div>";
        }
        $response['html'] = $html;
        $response['title'] = "Process List";
        return json_encode($response);
    }

    public function previewWeb($id) 
    {
        $q = Screenshot::where("id", $id)->first();
        $html = "";
        foreach ($q->browserHistory->list() as $bh) {
            $html .= "<div>" . $bh->url . "</div>";
        }
        $response['html'] = $html;
        $response['title'] = "Browser History List";
        return json_encode($response);
    }
}