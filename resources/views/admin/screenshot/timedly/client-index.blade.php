@extends('layouts.timedly.app')

@section('pageTitle')
    Screenshots
@endsection

@section('pageScripts')
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="//momentjs.com/downloads/moment-timezone-with-data.js"></script>

<script>
    var _table;
    var temp_delete_id = 0;

    function fetchData(target_date) {
        $('#screenshots-table').DataTable().destroy();

        $('#screenshots-table').DataTable({
            keys: !0,
            language: {
                paginate: {
                    previous: "<i class='fas fa-angle-left'>",
                    next: "<i class='fas fa-angle-right'>"
                }
            },
            "autoWidth": false,
            "bAutoWidth": false,
            processing: true,
            serverSide: true,
            ajax: {
                url:'/dashboard/screenshots/datatables',
                data: {
                    target_date : target_date,
                    type : 'GET'
                }
            },
            "columns": [
                    { "data": "first_name", "width": "20%" },
                    { "data": "last_name", "width": "20%" },
                    { "data": "screenshots", "width": "25%" },
                    { "data": "actions", "width": "150px", "orderable": false, searchable: "false"},
            ]
        });
    }

    $(document).ready(function() {

        var client_tz = '{{ $tz }}';
        var manila    = moment.tz("Asia/Manila");
        var client_side = manila.clone().tz(client_tz);

        $('#target_date').val(client_side.format("YYYY-MM-DD"));

        fetchData(client_side.format("YYYY-MM-DD"));

        //console.log('Manila', manila.format("MMMM Do YYYY, h:mm:ss a"));
        //console.log(client_tz, client_side.format("MMMM Do YYYY, h:mm:ss a"));

        $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        }).on('changeDate', function(e) {
            //target_date = $("#target_date").val();

            var target_date = $("#target_date").val();
            fetchData(target_date);
        });
    })
</script>
@endsection

@section('content')
    <div class="header bg-gradient-info pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">

                        <div class='row'>
                            <div class="col-lg-6 col-7">
                                <label>Select Date</label>
                                <div class="input-group date" id="datepicker">
                                <input type="text" id='target_date' name='target_date' class="form-control input-lg" value="{{date('Y-m-d', strtotime(now()))}}">
                                <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Page content -->

    <div class="container-fluid mt--6">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <!-- Card header -->
                    <div class="table-responsive py-4">
                        <table class="table table-flush" id="screenshots-table">
                            <thead class="thead-light">
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Screenshots</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>


        </div>
@endsection
