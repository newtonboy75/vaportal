<div class="col-md-4">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Welcome to VirtuDesk</h3> 
		</div>
		<div class="box-body">
			<ul class="list-group list-group-unbordered">
				<li class="list-group-item">
					<h4><i class="fa fa-clock-o" aria-hidden="true"></i> Today</h4>
					<div class="">
						{{ date("D M d, Y G:i a") }} {{ date_default_timezone_get() }}
					</div>
				</li>
				<li class="list-group-item">
					<h4><i class="fa fa-cog" aria-hidden="true"></i> Site Info Settings</h4>
					<div>
						<div>
							<label>Email:</label> {{ config( 'vatimetracker.siteinfo.email' ) }}
						</div>
						<div>
							<label>Contact:</label> {{ config( 'vatimetracker.siteinfo.phone' ) }}
						</div>
						<div class="text-right font-italic">
							<a href="#" class="font-italic">Click for more info</a>
						</div>
					</div>
				</li>
				<li class="list-group-item">
					<h4><i class="fa fa-money" aria-hidden="true"></i> Billing</h4>
					<div claass="">
						Create, update, and generate invoices and send to Client Dashboard.
					</div>
					<div class="text-right font-italic">
						<a href="/dashboard/billing">Click for more info</a>
					</div>
				</li>
				<li class="list-group-item">
					<h4><i class="fa fa-calendar" aria-hidden="true"></i> Schedules</h4>
					<div claass="">
						Manage Virtual Assistants Schedules.
					</div>
					<div class="text-right font-italic">
						<a href="/dashboard/schedules">Click for more info</a>
					</div>
				</li>
				<li class="list-group-item">
					<h4><i class="fa fa-image" aria-hidden="true"></i> Screenshots</h4>
					<div claass="">
						Manage Screenshots that has been taken by the tracker.
					</div>
					<div class="text-right font-italic">
						<a href="/dashboard/screenshots">Click for more info</a>
					</div>
				</li>
			</ul>
		</div><!-- box-body -->
		<div class="box-footer">
			<h4>Tracker</h4>
			<div>
				<a href="{{ config( 'vatimetracker.tracker_info.download_url' ) }}" class="btn btn-primary"><i class="fa fa-external-link" aria-hidden="true"></i> Installer</a>
				<a href="{{ config( 'vatimetracker.tracker_info.download_url' ) }}"class="btn btn-primary"><i class="fa fa-external-link" aria-hidden="true"></i> Guides</a>
			</div>
		</div> 
	</div>

</div>